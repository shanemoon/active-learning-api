from Simulator import *
from utils import save_pickle
import time

__author__ = "Shane Moon"

"""
	Description : Batch run the experiments.
	              Right now it is a bunch of copies and pastes.
	              Ideally, it needs some automation mechanism, which I won't worry about now.

	< Parameters > 
	'learner_type'    : type of <Learner> that defines the AL algorithm
	                    0 - <RandomLearner>
	                    1 - <MaxUtilityLearner>
	'learner_params'  : `params` passed to a <Learner> instance.
	                    'utility_params' : {
	                        'unlabeld_only' : (Boolean) to indicate if we are only considering utilities of unlabeled examples
	                        'active_features' : <List> of handles to feature methods (defined below)
	                        'active_features_params' : <List> of feature params in the same order as in 'active_features' {
	                            'density_est_type' : for utility functions that require density estimation
	                              0 - Kernel Density Estimation
	                              1 - K-means Reverse Count
	                        }
	                        'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
	                        'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
	                        'dynamic_params' : {
	                            # Params related to dynamic proactive learning (DPAL)
	                            'weight_update_method' : e.g. utility_functions.dpal_update
	                            'main_percentage' : percentage of # samples for the "main' model
	                            'M' : # of clusters for "extra" models
	                            'G' : # of samples in each 'extra' model cluster
	                            'extra_model_group_method'
	                              0 - K-means clustering
	                              1 - K-means clustering + 10-NN of centroids 
	                            'extra_model_group_space'
	                              0 - Original concept space
	                              1 - Utility metrics space
	                        }
	                        'utility_sample' (for <MaxUtilityLearner>)
	                          0 - Sort the utility tuples by their utility value, and select the best `n_queries' candidates
	                          1 - Weighted sample of tuples by their normalized utility value
	                        'annotator_selection' (for <MaxUtilityLearner>)
	                          0 - Choose an annotator for each instance with the best proactive_features[0] value
	                          1 - Choose an annotator for each instance with the best overall value
	                          2 - Allow duplicate annotators
	                    }
	'model_type'      : type of <Model> that defines the classifier
	                    0 - <ScikitModel>
	'model_params'    : `params` passed to a <Model> instance.
	                    'clf_type' : (e.g.) 'LR', 'SVM', 'NB' etc.
	'd_annotators'    : a dictionary for ('name' : [ cost, type, params ] )
	'dataset_name'    : name of the dataset
	'budget'          : total budget assigned for a single fold task
	'percent_labeled' : percentage of initially labeled instances
	'n_queries'       : # of queries that will be made at each iteration
    'n_folds'         : # of folds that will be performed for cross validation
"""


def run(dataset_name, exp_codes, override_names=[]):
	"""
		< Exp Params Code >
		0. DPAL: 0, PAL: 1, Non-PAL: 2
		1 ~ 6. Binary indication of criterion inclusion (entropy, density, unknownness, centroid, random, p_ans)
		7. Linear: 0, Logistic L1: 1, Logistic L2: 2, RBF: 3
		8. Log scale
		9. M
		10. G
		11. main_percentage
		12. annotator_noise_rate
	""" 

	# List of parameters
	list_exp_params  = []
	names_exp_params = []

	# Experiment Configurations
	graph_output_filename_folder = './../results/graphs'
	pickle_output_filename_folder = './../results/pickles'
	output_filename_suffix = '_test'
	graph_output_filename_extension = 'eps'
	pickle_output_filename_extension = 'pkl'

	# Determine output filename
	graph_output_filename = '%s/%s_%s%s.%s' % (graph_output_filename_folder, dataset_name, 
		'_'.join( [ ''.join([ str(exp_num) for exp_num in exp_code]) for exp_code in exp_codes] ), 
		output_filename_suffix, graph_output_filename_extension)
	pickle_output_filename = '%s/%s_%s%s.%s' % (pickle_output_filename_folder, dataset_name, 
		'_'.join( [ ''.join([ str(exp_num) for exp_num in exp_code]) for exp_code in exp_codes] ), 
		output_filename_suffix, pickle_output_filename_extension)

	# Determine if we will override legend names
	do_override_names = True if override_names != [] and len(override_names) == len(exp_codes) else False

	# Preset Experiments
	if dataset_name == 'synth3':
		total_budget = 300
		init_percent_labeled = 0.005
		n_queries_per_batch = 5
		exp_model_params = { 'clf_type' : 'SVC' }
		n_update_every_recurring = 20
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 3
		deafult_annotator_noise_rate = 0.4
		p_ans_sigma = 0.4
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 200
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = []
		stop_conditions_params = []
		downsize = -1

	elif dataset_name == 'synth1':
		total_budget = 500
		init_percent_labeled = 0.005
		n_queries_per_batch = 4
		exp_model_params = { 'clf_type' : 'SVC' }
		n_update_every_recurring = 30
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 4
		deafult_annotator_noise_rate = 0.4
		p_ans_sigma = 0.3
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 200
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = []
		stop_conditions_params = []
		downsize = -1
	

	elif dataset_name == 'iris':
		total_budget = 100
		init_percent_labeled = 0.02
		n_queries_per_batch = 5
		exp_model_params = { 'clf_type' : 'SVC' }
		n_update_every_recurring = 20
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 4
		deafult_annotator_noise_rate = 0.4
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 100
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = []
		stop_conditions_params = []
		downsize = -1
	

	elif dataset_name == '20newsgroups_norm':
		total_budget = 3000
		init_percent_labeled = 0.01
		n_queries_per_batch = 20
		exp_model_params = { 'clf_type' : 'NB' }
		n_update_every_recurring = 300
		density_est_type = 1
		unknownness_est_type = 1
		threshold_accuracy = 0.55
		n_domain_experts = 4
		deafult_annotator_noise_rate = 0.4
		p_ans_sigma = 0.5
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.1
		n_goal_reach_samples = 4000
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = []
		stop_conditions_params = []
		downsize = 10000

	elif dataset_name == '4newsgroups_mini':
		#total_budget = 1600 # noise = 0.5
		#total_budget = 1000  # noiseless
		total_budget = 750
		init_percent_labeled = 0.003
		n_queries_per_batch = 8
		exp_model_params = { 'clf_type' : 'NB' }
		#n_update_every_recurring = 200 # noise = 0.5
		n_update_every_recurring = 150 # noiseless
		density_est_type = 1
		unknownness_est_type = 1
		#threshold_accuracy = 0.25 # noise = 0.5
		threshold_accuracy = 0.7 # noiseless
		n_domain_experts = 4
		deafult_annotator_noise_rate = 0.50
		p_ans_sigma = 0.6
		p_ans_sigma_start = 0.6
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 800
		exp_n_repeat_iterations = 1
		exp_n_folds = 5	
		stop_conditions = [ utility_functions.dpal_stop_test_accuracy ]
		#stop_conditions_params = [ {'threshold_accuracy' : 0.83} ] # noise = 0.5
		stop_conditions_params = [ {'threshold_accuracy' : 0.885} ]  # noiseless
		downsize = -1

	elif dataset_name == 'new_mal_pca3_4':
		total_budget = 1000
		init_percent_labeled = 0.01
		n_queries_per_batch = 15
		exp_model_params = { 'clf_type' : 'SVC' }
		n_update_every_recurring = 300
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 4
		deafult_annotator_noise_rate = 0.4
		p_ans_sigma = 0.2	
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 100
		exp_n_repeat_iterations = 1
		exp_n_folds = 5	
		stop_conditions = []
		stop_conditions_params = []
		downsize = -1
	

	elif dataset_name == 'new_mal_4_norm':
		total_budget = 2400
		init_percent_labeled = 0.01
		n_queries_per_batch = 30
		exp_model_params = { 'clf_type' : 'LR' }
		n_update_every_recurring = 300
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 4
		deafult_annotator_noise_rate = 0.4
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 100
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = []
		stop_conditions_params = []
		downsize = -1

	elif dataset_name == 'new_mal_3':
		total_budget = 200
		init_percent_labeled = 0.001
		n_queries_per_batch = 1
		exp_model_params = { 'clf_type' : 'SVC' }
		n_update_every_recurring = 300
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 1
		deafult_annotator_noise_rate = 0.0
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 100
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = []
		stop_conditions_params = []
		downsize = -1

	elif dataset_name == 'new_mal_pca3_3':
		total_budget = 200
		init_percent_labeled = 0.001
		n_queries_per_batch = 1
		exp_model_params = { 'clf_type' : 'SVC' }
		n_update_every_recurring = 300
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 1
		deafult_annotator_noise_rate = 0.0
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.5
		p_ans_sigma_goal = 0.05
		n_goal_reach_samples = 100
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = []
		stop_conditions_params = []
		downsize = -1

	elif dataset_name == 'satimage':
		total_budget = 1500
		init_percent_labeled = 0.01
		n_queries_per_batch = 20
		exp_model_params = { 'clf_type' : 'LR' }
		n_update_every_recurring = 200
		density_est_type = 1
		unknownness_est_type = 1
		threshold_accuracy = 0.735
		n_domain_experts = 3
		deafult_annotator_noise_rate = 0.2
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.2
		p_ans_sigma_goal = 0.02
		n_goal_reach_samples = 100
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = [ utility_functions.dpal_stop_test_accuracy ]
		stop_conditions_params = [ {'threshold_accuracy' : 0.785} ]
		downsize = -1

	elif dataset_name == 'mnist':
		total_budget = 1500
		init_percent_labeled = 0.005
		n_queries_per_batch = 15
		exp_model_params = { 'clf_type' : 'SGD', 'clf_params' : {'loss' : 'log'} }
		n_update_every_recurring = 200
		density_est_type = 1
		unknownness_est_type = 1
		threshold_accuracy = 0.7
		n_domain_experts = 5
		deafult_annotator_noise_rate = 0.2
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.2
		p_ans_sigma_goal = 0.02
		n_goal_reach_samples = 400
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = [ utility_functions.dpal_stop_test_accuracy ]
		stop_conditions_params = [ {'threshold_accuracy' : 0.82} ]
		downsize = 7000

	elif dataset_name == 'covertype':
		total_budget = 500
		init_percent_labeled = 0.005
		n_queries_per_batch = 5
		exp_model_params = { 'LR' }
		n_update_every_recurring = 80
		density_est_type = 1
		unknownness_est_type = 1
		threshold_accuracy = 0.4
		n_domain_experts = 5
		deafult_annotator_noise_rate = 0.2
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.2
		p_ans_sigma_goal = 0.02
		n_goal_reach_samples = 400
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = [ utility_functions.dpal_stop_test_accuracy ]
		stop_conditions_params = [ {'threshold_accuracy' : 0.70} ]
		downsize = 7000

	elif dataset_name == 'mal':
		total_budget = 300
		init_percent_labeled = 0.05
		n_queries_per_batch = 5
		exp_model_params = { 'LR' }
		n_update_every_recurring = 50
		density_est_type = 1
		unknownness_est_type = 1
		threshold_accuracy = 0.4
		n_domain_experts = 3
		deafult_annotator_noise_rate = 0.2
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.2
		p_ans_sigma_goal = 0.02
		n_goal_reach_samples = 400
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = [ utility_functions.dpal_stop_test_accuracy ]
		stop_conditions_params = [ {'threshold_accuracy' : 0.70} ]
		downsize = 1000

	elif dataset_name == '3class_from_alldata_nz':
		total_budget = 500
		init_percent_labeled = 0.02
		n_queries_per_batch = 5
		exp_model_params = { 'SVC' }
		n_update_every_recurring = 50
		density_est_type = 1
		unknownness_est_type = 1
		threshold_accuracy = 0.4
		n_domain_experts = 3
		deafult_annotator_noise_rate = 0.2
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.2
		p_ans_sigma_goal = 0.02
		n_goal_reach_samples = 400
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = [ utility_functions.dpal_stop_test_accuracy ]
		stop_conditions_params = [ {'threshold_accuracy' : 0.70} ]
		downsize = -1

	elif dataset_name == '3class_from_alldata_nz_pca':
		total_budget = 500
		init_percent_labeled = 0.02
		n_queries_per_batch = 5
		exp_model_params = { 'SVC' }
		n_update_every_recurring = 50
		density_est_type = 1
		unknownness_est_type = 1
		threshold_accuracy = 0.4
		n_domain_experts = 3
		deafult_annotator_noise_rate = 0.2
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.2
		p_ans_sigma_goal = 0.02
		n_goal_reach_samples = 400
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = [ utility_functions.dpal_stop_test_accuracy ]
		stop_conditions_params = [ {'threshold_accuracy' : 0.70} ]
		downsize = -1

	elif dataset_name == 'alldata_nz2_pca':
		total_budget = 300
		init_percent_labeled = 0.002
		n_queries_per_batch = 1
		exp_model_params = { 'LR' }
		n_update_every_recurring = 30
		density_est_type = 0
		unknownness_est_type = 0
		threshold_accuracy = 0.55
		n_domain_experts = 1
		deafult_annotator_noise_rate = 0.0
		p_ans_sigma = 0.2
		p_ans_sigma_start = 0.2
		p_ans_sigma_goal = 0.02
		n_goal_reach_samples = 100
		exp_n_repeat_iterations = 1
		exp_n_folds = 5
		stop_conditions = [  ]
		stop_conditions_params = [  ]
		downsize = -1

	full_list_active_features = [utility_functions.entropy, utility_functions.density, utility_functions.unknownness, utility_functions.even_cluster, utility_functions.random_value]
	full_list_active_features_params = [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True} , {}, {} ]
	full_list_active_features_names = ['US', 'DENS', 'UNK', 'CENT', 'RAND']
	full_list_proactive_features = [ utility_functions.pro_p_answer ]
	full_list_proactive_features_params = [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ]
	full_list_proactive_features_names = ['P_ANS']


	for i in range(len(exp_codes)):
		exp_code = exp_codes[i]
		assert(len(exp_code) == 13)

		# 1. Decide what criteria will be used
		active_features = []
		active_features_params = []
		proactive_features = []
		proactive_features_params = []
		features_names = []

		if exp_code[1] == 1:
			# Entropy
			active_features.append( full_list_active_features[0] )
			active_features_params.append( full_list_active_features_params[0] )
			features_names.append( full_list_active_features_names[0] )
		if exp_code[2] == 1:
			# Density
			active_features.append( full_list_active_features[1] )
			active_features_params.append( full_list_active_features_params[1] )
			features_names.append( full_list_active_features_names[1] )
		if exp_code[3] == 1:
			# Unknownness
			active_features.append( full_list_active_features[2] )
			active_features_params.append( full_list_active_features_params[2] )
			features_names.append( full_list_active_features_names[2] )
		if exp_code[4] == 1:
			# Centroid
			active_features.append( full_list_active_features[3] )
			active_features_params.append( full_list_active_features_params[3] )
			features_names.append( full_list_active_features_names[3] )
		if exp_code[5] == 1:
			# Random
			active_features.append( full_list_active_features[4] )
			active_features_params.append( full_list_active_features_params[4] )
			features_names.append( full_list_active_features_names[4] )
		if exp_code[6] == 1:
			# Probability of answer
			proactive_features.append( full_list_proactive_features[0] )
			proactive_features_params.append( full_list_proactive_features_params[0] )
			features_names.append( full_list_proactive_features_names[0] )

		# 2. Is it DPAL, PAL, or AL?
		dpal_code = exp_code[0]

		# 3. Regresion method?
		regression_method_code = exp_code[7]
		if regression_method_code == 0:
			regression_method = 'linear'
		elif regression_method_code == 1:
			regression_method = 'lr1'
		elif regression_method_code == 2:
			regression_method = 'lr2'
		elif regression_method_code == 3:
			regression_method = 'svr_kernel'
		elif regression_method_code == 4:
			regression_method = 'linear_tensorflow'


		# 4. Do it in log scale?
		log_scale_code = exp_code[8]
		if log_scale_code == 0:
			dpal_b_log_weight = False
		else:
			dpal_b_log_weight = True

		# 5. M and G, main_percentage
		M, G, main_percentage = exp_code[9], exp_code[10], exp_code[11]

		# 6. Annotator
		exp_annotator_noise_rate = exp_code[12]
		if exp_annotator_noise_rate == -1:
			annotator_noise_rate = deafult_annotator_noise_rate
		else:
			annotator_noise_rate = exp_annotator_noise_rate

		# 7. Determine Name
		name_exp_params = ''
		if do_override_names and override_names[i] != '':
			name_exp_params = override_names[i]
		else:
			# If there is no override name provided,
			# we manually compose one.
			if dpal_code == 0:
				# DPAL
				name_exp_params += 'DPAL ('
			elif dpal_code == 1:
				# PAL
				name_exp_params += 'PAL ('
			else:
				# AL
				name_exp_params += '('
			# Criteria
			name_exp_params += '+'.join( features_names ) + ')'
			# Unique to dpal configs
			if dpal_code == 0:
				# Regression method
				if regression_method_code == 0:
					pass
				elif regression_method_code == 1:
					name_exp_params += ', LR1'
				elif regression_method_code == 2:
					name_exp_params += ', LR2'	
				elif regression_method_code == 3:
					name_exp_params += ', RBF'
				elif regression_method_code == 4:
					name_exp_params += ', LTF'					
				# Log scale or not
				if log_scale_code == 0:
					pass
				elif log_scale_code == 1:
					name_exp_params += ' (LOG)'
				# M, G, main_percentage, annotator_noise_rate
				name_exp_params += ' %d,%d,%.1f,%.1f' % (M, G, main_percentage, annotator_noise_rate)

		learner_type    = 1
		utility_params  = {
			'b_log_weight' : False,
			'active_features' : active_features,
			'active_features_params' : active_features_params,
			'proactive_features' : proactive_features,
			'proactive_features_params' : proactive_features_params,
			'utility_sample' : 0,
			'annotator_selection' : 0
		 }
		if dpal_code == 0:
			utility_params['dynamic_params'] = { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : main_percentage,
			                     'M' : M, 'G' : G, 'choose_M_method' : 0, 'extra_model_group_method' : 2,
			                     'extra_model_group_space' : 0, 'regression_method' : regression_method, 'b_log_weight' : dpal_b_log_weight,
			                     'start_conditions' : [utility_functions.dpal_update_test_accuracy],
			                     'start_conditions_params' : [ {'threshold_accuracy' : threshold_accuracy} ], 
			                     'update_conditions' : [utility_functions.dpal_update_every_recurring],
			                     'update_conditions_params' : [ {'n_update_every_recurring' : n_update_every_recurring } ],
			                     'stop_conditions' : stop_conditions,
			                     'stop_conditions_params' : stop_conditions_params }

		learner_params  = { 'utility_params' : utility_params }
		model_type       = 0
		model_params     = exp_model_params
		#d_annotators     = {}
		d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
		dataset_name     = dataset_name
		downsize         = downsize
		budget           = total_budget
		percent_labeled  = init_percent_labeled
		n_queries        = n_queries_per_batch
		n_folds          = exp_n_folds
		n_repeat_iterations = exp_n_repeat_iterations
		return_after_exp = { 'fold_error_rates', 'fold_costs' }

		exp_params = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
		                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
		                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

		list_exp_params.append( exp_params )
		names_exp_params.append( name_exp_params )



	# Make sure that we have all the necessary folders
	#make_folders()


	# Batch run the simulations
	result_dictionary = {}
	result_dictionary['legend_order'] = names_exp_params

	for i in range(len(list_exp_params)):
		exp_params = list_exp_params[i]
		legend = names_exp_params[i]
		print "==============================  Experiment %d, Code Name: %s  ==============================" % (i, legend)
		start_time = time.time()

		# Run the simulator
		exp_results = Simulator().run( exp_params )
		fold_error_rates = exp_results['fold_error_rates']
		fold_costs = exp_results['fold_costs']	

		# Add the result to result_dictionary
		result_dictionary[legend] = {'X':fold_costs, 'Y':fold_error_rates}
		elapsed_time = time.time() - start_time
		print "==============================  Experiment %d Done, Elapsed: %.2f  ==============================" % (i, elapsed_time)


	# Draw the graph
	Drawer().draw(graph_output_filename, result_dictionary, 'Cost', 'Error Rate', '', True)
	save_pickle(result_dictionary, pickle_output_filename)


if __name__ == '__main__':
	"""
		< Exp Params Code >
		0. DPAL: 0, PAL: 1, Non-PAL: 2
		1 ~ 6. Binary indication of criterion inclusion (entropy, density, unknownness, centroid, random, p_ans)
		7. Linear: 0, Logistic L1: 1, Logistic L2: 2, RBF: 3
		8. Log scale
		9. M
		10. G
		11. main_percentage
		12. annotator_noise_rate
	""" 
	exp_codes = [
		[0, 1, 0, 0, 0, 0, 0, 0, 0, 20, 20, 0.7, -1 ], # DPAL (US) Linear
	]
	dataset_name = '3class_from_alldata_nz_pca'
	run(dataset_name, exp_codes)