__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		An Annotator who always gives a ground-truth annotator.
"""

from BaseAnnotator import BaseAnnotator

class HumanAnnotator(BaseAnnotator):

	def __init__(self, name="", annotator_id=0, cost=1, params={}):
		# Ground Truth Annotator's id is fixed to be -1.
		if name == "":
			name = "Human Annotator ID %d" % annotator_id
		BaseAnnotator.__init__(self, name, annotator_id, cost, params)
