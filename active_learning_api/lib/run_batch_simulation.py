from Simulator import *
import time

__author__ = "Shane Moon"

"""
	Description : Batch run the experiments.
	              Right now it is a bunch of copies and pastes.
	              Ideally, it needs some automation mechanism, which I won't worry about now.

	< Parameters > 
	'learner_type'    : type of <Learner> that defines the AL algorithm
	                    0 - <RandomLearner>
	                    1 - <MaxUtilityLearner>
	'learner_params'  : `params` passed to a <Learner> instance.
	                    'utility_params' : {
	                        'unlabeld_only' : (Boolean) to indicate if we are only considering utilities of unlabeled examples
	                        'active_features' : <List> of handles to feature methods (defined below)
	                        'active_features_params' : <List> of feature params in the same order as in 'active_features' {
	                            'density_est_type' : for utility functions that require density estimation
	                              0 - Kernel Density Estimation
	                              1 - K-means Reverse Count
	                        }
	                        'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
	                        'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
	                        'dynamic_params' : {
	                            # Params related to dynamic proactive learning (DPAL)
	                            'weight_update_method' : e.g. utility_functions.dpal_update
	                            'main_percentage' : percentage of # samples for the "main' model
	                            'M' : # of clusters for "extra" models
	                            'extra_model_group_method'
	                              0 - K-means clustering
	                              1 - K-means clustering + 10-NN of centroids 
	                            'extra_model_group_space'
	                              0 - Original concept space
	                              1 - Utility metrics space
	                        }
	                        'utility_sample' (for <MaxUtilityLearner>)
	                          0 - Sort the utility tuples by their utility value, and select the best `n_queries' candidates
	                          1 - Weighted sample of tuples by their normalized utility value
	                        'annotator_selection' (for <MaxUtilityLearner>)
	                          0 - Choose an annotator for each instance with the best proactive_features[0] value
	                          1 - Choose an annotator for each instance with the best overall value
	                          2 - Allow duplicate annotators
	                    }
	'model_type'      : type of <Model> that defines the classifier
	                    0 - <ScikitModel>
	'model_params'    : `params` passed to a <Model> instance.
	                    'clf_type' : (e.g.) 'LR', 'SVM', 'NB' etc.
	'd_annotators'    : a dictionary for ('name' : [ cost, type, params ] )
	'dataset_name'    : name of the dataset
	'budget'          : total budget assigned for a single fold task
	'percent_labeled' : percentage of initially labeled instances
	'n_queries'       : # of queries that will be made at each iteration
    'n_folds'         : # of folds that will be performed for cross validation
"""


# List of parameters
list_exp_params  = []
names_exp_params = []

# Experiment Configurations
dataset_name = 'synth3'
output_filename_folder = './../results/graphs'
output_filename_suffix = '_test'
output_filename_extension = 'eps'

# Experiment List
# 1  : DPAL (US+DENS+UNK+P_ANS), RBF
# 2  : DPAL (US+DENS+UNK+P_ANS), RBF (LOG)
# 3  : DPAL (US+DENS+UNK+P_ANS), Linear
# 4  : DPAL (US+DENS+UNK+P_ANS), Linear (LOG) 
# 5  : PAL (US+DENS+UNK+P_ANS)
# 6  : PAL (US+P_ANS)
# 7  : US+DENS+UNK
# 8  : US
# 9  : DENS
# 10 : RAND

#exp_list = [3,5,6,7]
exp_list = [1,3,5,6]
output_filename = '%s/%s_%s%s.%s' % (output_filename_folder, dataset_name, 
	'_'.join( [str(exp_num) for exp_num in exp_list] ), 
	output_filename_suffix, output_filename_extension)

# Preset Experiments
if dataset_name == 'synth3':
	total_budget = 300
	init_percent_labeled = 0.005
	n_queries_per_batch = 5
	exp_model_params = { 'clf_type' : 'SVC' }
	n_update_every_recurring = 20
	density_est_type = 0
	unknownness_est_type = 0
	threshold_accuracy = 0.55
	n_domain_experts = 3
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.4
	p_ans_sigma_start = 0.5
	p_ans_sigma_goal = 0.05
	n_goal_reach_samples = 200
	exp_n_repeat_iterations = 1
	exp_n_folds = 5	

elif dataset_name == 'synth1':
	total_budget = 300
	init_percent_labeled = 0.005
	n_queries_per_batch = 5
	exp_model_params = { 'clf_type' : 'SVC' }
	n_update_every_recurring = 30
	density_est_type = 0
	unknownness_est_type = 0
	threshold_accuracy = 0.55
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.3
	p_ans_sigma_start = 0.5
	p_ans_sigma_goal = 0.05
	n_goal_reach_samples = 200
	exp_n_repeat_iterations = 1
	exp_n_folds = 5	

elif dataset_name == 'iris':
	total_budget = 100
	init_percent_labeled = 0.02
	n_queries_per_batch = 5
	exp_model_params = { 'clf_type' : 'SVC' }
	n_update_every_recurring = 20
	density_est_type = 0
	unknownness_est_type = 0
	threshold_accuracy = 0.55
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.2
	p_ans_sigma_start = 0.5
	p_ans_sigma_goal = 0.05
	n_goal_reach_samples = 100
	exp_n_repeat_iterations = 1
	exp_n_folds = 5	

elif dataset_name == '20newsgroups_norm':
	total_budget = 2500
	init_percent_labeled = 0.01
	n_queries_per_batch = 25
	exp_model_params = { 'clf_type' : 'NB' }
	n_update_every_recurring = 250
	density_est_type = 1
	unknownness_est_type = 1
	threshold_accuracy = 0.55
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.2
	p_ans_sigma_start = 0.5
	p_ans_sigma_goal = 0.05
	n_goal_reach_samples = 100
	exp_n_repeat_iterations = 1
	exp_n_folds = 5	

elif dataset_name == '4newsgroups_mini':
	total_budget = 1600
	init_percent_labeled = 0.005
	n_queries_per_batch = 16
	exp_model_params = { 'clf_type' : 'NB' }
	n_update_every_recurring = 200
	density_est_type = 1
	unknownness_est_type = 1
	threshold_accuracy = 0.5
	n_domain_experts = 4
	annotator_noise_rate = 0.6
	p_ans_sigma = 0.6
	p_ans_sigma_start = 0.6
	p_ans_sigma_goal = 0.05
	n_goal_reach_samples = 800
	exp_n_repeat_iterations = 1
	exp_n_folds = 5	

elif dataset_name == 'new_mal_pca3_4':
	total_budget = 1000
	init_percent_labeled = 0.01
	n_queries_per_batch = 15
	exp_model_params = { 'clf_type' : 'SVC' }
	n_update_every_recurring = 300
	density_est_type = 0
	unknownness_est_type = 0
	threshold_accuracy = 0.55
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.2	
	p_ans_sigma_start = 0.5
	p_ans_sigma_goal = 0.05
	n_goal_reach_samples = 100
	exp_n_repeat_iterations = 1
	exp_n_folds = 5	

elif dataset_name == 'new_mal_4_norm':
	total_budget = 2400
	init_percent_labeled = 0.01
	n_queries_per_batch = 30
	exp_model_params = { 'clf_type' : 'LR' }
	n_update_every_recurring = 300
	density_est_type = 0
	unknownness_est_type = 0
	threshold_accuracy = 0.55
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.2
	p_ans_sigma_start = 0.5
	p_ans_sigma_goal = 0.05
	n_goal_reach_samples = 100
	exp_n_repeat_iterations = 1
	exp_n_folds = 5


# Exp params 1
if 1 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True}  ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
		                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
		                     'extra_model_group_space' : 1, 'regression_method' : 'svr_kernel',
		                     'start_conditions' : [utility_functions.dpal_update_test_accuracy],
		                     'start_conditions_params' : [ {'threshold_accuracy' : threshold_accuracy} ], 
		                     'update_conditions' : [utility_functions.dpal_update_every_recurring],
		                     'update_conditions_params' : [ {'n_update_every_recurring' : n_update_every_recurring } ] },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_1 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_1 )
	names_exp_params.append( 'DPAL (US+DENS+UNK+P_ANS), RBF' )



# Exp params 2
if 2 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
		                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
		                     'extra_model_group_space' : 1, 'b_log_weight' : True, 'regression_method' : 'svr_kernel',
		                     'start_conditions' : [utility_functions.dpal_update_test_accuracy],
		                     'start_conditions_params' : [ {'threshold_accuracy' : threshold_accuracy} ], 
		                     'update_conditions' : [utility_functions.dpal_update_every_recurring],
		                     'update_conditions_params' : [ {'n_update_every_recurring' : n_update_every_recurring } ] },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_2 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_2 )
	names_exp_params.append( 'DPAL (US+DENS+UNK+P_ANS), RBF (LOG)' )



# Exp params 3
if 3 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
		                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
		                     'extra_model_group_space' : 1, 'regression_method' : 'linear',
		                     'start_conditions' : [utility_functions.dpal_update_test_accuracy],
		                     'start_conditions_params' : [ {'threshold_accuracy' : threshold_accuracy} ], 
		                     'update_conditions' : [utility_functions.dpal_update_every_recurring],
		                     'update_conditions_params' : [ {'n_update_every_recurring' : n_update_every_recurring } ] },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_3 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_3 )
	names_exp_params.append( 'DPAL (US+DENS+UNK+P_ANS), Linear' )



# Exp params 4
if 4 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
		                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
		                     'extra_model_group_space' : 1, 'b_log_weight' : True, 'regression_method' : 'linear',
		                     'start_conditions' : [utility_functions.dpal_update_test_accuracy],
		                     'start_conditions_params' : [ {'threshold_accuracy' : threshold_accuracy} ], 
		                     'update_conditions' : [utility_functions.dpal_update_every_recurring],
		                     'update_conditions_params' : [ {'n_update_every_recurring' : n_update_every_recurring } ] },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_4 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_4 )
	names_exp_params.append( 'DPAL (US+DENS+UNK+P_ANS), Linear (LOG)' )





# Exp params 5
if 5 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_5 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_5 )
	names_exp_params.append( 'PAL (US+DENS+UNK+P_ANS)' )




# Exp params 6
if 6 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy ],
		'active_features_params' : [ {'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_6 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_6 )
	names_exp_params.append( 'PAL (US+P_ANS)' )


# Exp params 600
if 600 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy ],
		'active_features_params' : [ {'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 0, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_600 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_600 )
	names_exp_params.append( 'PAL (US+P_ANS0)' )


# Exp params 601
if 601 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy ],
		'active_features_params' : [ {'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 1, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_601 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_601 )
	names_exp_params.append( 'PAL (US+P_ANS1)' )




# Exp params 7
if 7 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True}  ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_7 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_7 )
	names_exp_params.append( 'US+DENS+UNK' )




# Exp params 8
if 8 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy ],
		'active_features_params' : [ {'normalize' : True} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type          = 0
	model_params        = exp_model_params
	#d_annotators        = {}
	d_annotators        = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name        = dataset_name
	downsize            = -1
	budget              = total_budget
	percent_labeled     = init_percent_labeled
	n_queries           = n_queries_per_batch
	n_folds             = 5
	n_repeat_iterations = 1
	return_after_exp    = { 'fold_error_rates', 'fold_costs' }

	exp_params_8 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_8 )
	names_exp_params.append( 'US' )





# Exp params 9
if 9 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.density ],
		'active_features_params' : [ {'density_est_type': density_est_type, 'normalize' : True} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_9 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_9 )
	names_exp_params.append( 'DENS' )





# Exp params 10
if 10 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.random_value ],
		'active_features_params' : [ {} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_10 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_10 )
	names_exp_params.append( 'RAND' )




# Exp params 3
if 3 in exp_list:
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True} ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 2, 'p_ans_sigma' : p_ans_sigma, 'p_ans_sigma_start' : p_ans_sigma_start, 'p_ans_sigma_goal' : p_ans_sigma_goal, 'n_goal_reach_samples': n_goal_reach_samples } ],
		'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
		                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
		                     'extra_model_group_space' : 1, 'regression_method' : 'linear',
		                     'start_conditions' : [utility_functions.dpal_update_test_accuracy],
		                     'start_conditions_params' : [ {'threshold_accuracy' : threshold_accuracy} ], 
		                     'update_conditions' : [utility_functions.dpal_update_every_recurring],
		                     'update_conditions_params' : [ {'n_update_every_recurring' : n_update_every_recurring } ] },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = exp_model_params
	#d_annotators     = {}
	d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
	dataset_name     = dataset_name
	downsize         = -1
	budget           = total_budget
	percent_labeled  = init_percent_labeled
	n_queries        = n_queries_per_batch
	n_folds          = exp_n_folds
	n_repeat_iterations = exp_n_repeat_iterations
	return_after_exp = { 'fold_error_rates', 'fold_costs' }

	exp_params_3 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

	list_exp_params.append( exp_params_3 )
	names_exp_params.append( 'DPAL (US+DENS+UNK+P_ANS), Linear' )


# Make sure that we have all the necessary folders
#make_folders()





# Batch run the simulations
result_dictionary = {}

for i in range(len(list_exp_params)):
	exp_params = list_exp_params[i]
	legend = names_exp_params[i]
	print "==============================  Experiment %d, Code Name: %s  ==============================" % (i, legend)
	start_time = time.time()

	# Run the simulator
	exp_results = Simulator().run( exp_params )
	fold_error_rates = exp_results['fold_error_rates']
	fold_costs = exp_results['fold_costs']	

	# Add the result to result_dictionary
	result_dictionary[legend] = {'X':fold_costs, 'Y':fold_error_rates}
	elapsed_time = time.time() - start_time
	print "==============================  Experiment %d Done, Elapsed: %.2f  ==============================" % (i, elapsed_time)


# Draw the graph
Drawer().draw(output_filename, result_dictionary, 'Cost', 'Error Rate', '', False)