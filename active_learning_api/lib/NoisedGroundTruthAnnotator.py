__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		An Annotator who gives a noised version of ground-truth annotator with some probability.
"""

from BaseAnnotator import BaseAnnotator
import random
from utils import print_errors

class NoisedGroundTruthAnnotator(BaseAnnotator):

	def __init__(self, name="Noised Ground Truth Annotator", annotator_id=0, cost=1, params={}):
		BaseAnnotator.__init__(self, name, annotator_id, cost, params)
		self.Z = params['Z']

		# noise_level can either be a scalar value in range [0.0 ~ 1.0],
		# or a dictionary of { class_label : noise_value }
		self.noise_level = params['noise_level']

		# Noise Type
		# 0 : Overall Average   (scalar)
		# 1 : Class Conditional (dictionary of { class_label : noise_value } )
		self.noise_type  = params['noise_type'] 

	def __str__(self):
		return "%s with params: %s" % (self.name, str(self.params))


	def answer_queries(self, dataset, indices_to_query, info={}):
		"""
			Return the annotation of the queries
		"""
		Y_answered = self.Z[ indices_to_query ]


		# Depending on the noise type, we return different labels
		if self.noise_type == 0:
			# It has an average noise rate across the entire dataset
			for i in range(len(Y_answered)):
				if random.random() < self.noise_level:
					# Change the value of Y_answered[i] to something random
					Y_answered[i] = random.choice( dataset.categories )	

		elif self.noise_type == 1:
			# It has a class conditional scalar average
			for i in range(len(indices_to_query)):
				try:
					y = Y_answered[i]
					if y in self.noise_level:
						noise_value_per_class = self.noise_level[ y ]
						if random.random() < noise_value_per_class:
							# Change the value of Y_answered[i] to something random
							Y_answered[i] = random.choice( dataset.categories )
					else:
						# If the provided noise_level dictionary does not have information for this class,
						# we let the ground truth value unchanged
						pass

				except:
					# If something goes wrong, we let the ground truth value unchanged.
					print_errors( '@ NoisedGroundTruthAnnotator' )
					pass


		return Y_answered