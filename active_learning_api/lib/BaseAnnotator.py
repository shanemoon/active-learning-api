__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		A base class for Annotator.
"""

import numpy as np

class BaseAnnotator:

	def __init__(self, name="BaseAnnotator", annotator_id=0, cost=1, params={}):
		self.name = name
		self.id = annotator_id
		self.cost = cost
		self.params = params
		self.availability = True
		self.labeled_indices = np.array([], dtype=int)
		self.labeled_samples = np.array([], dtype=int)


	def __str__(self):
		return "%s with params: %s" % (self.name, str(self.params))


	def answer_queries(self, dataset, indices_to_query, info={}):
		"""
			Return the annotation of the queries
		"""
		Y_answered = []
		return Y_answered


	def setAvailability(self, availability):
		self.availability = availability


	def update_query_history(self, indices_to_query, Y_answered):
		# Update the query history
		self.labeled_indices = np.append( self.labeled_indices, indices_to_query )
		self.labeled_samples = np.append( self.labeled_samples, Y_answered )