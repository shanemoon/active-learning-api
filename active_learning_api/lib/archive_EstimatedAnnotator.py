__author__ = 'bwoods'

"""
	Author: Bronwyn Woods
	Description:
		An Annotator with parameters that are estimated by the Learner.
"""

from BaseAnnotator import BaseAnnotator
import random
from utils import print_errors

class EstimatedAnnotator(BaseAnnotator):

	def __init__(self, name="Estimated Annotator", annotator_id=0, cost=1, params={}):
		BaseAnnotator.__init__(self, name, annotator_id, cost, params)

		# ground truth
		self.Z = params['Z']

		# current estimated parameters
		# this is a dictionary indexed by class name with the class-specific parameters
		#  within each element
		self.est_params = params['prior_params']

		# true parameters (when this is a simulated annotator)
		self.true_params = params['true_params']


	def __str__(self):
		return "%s with params: %s" % (self.name, str(self.params))


	# Functions for accessing and retrieving the parameters of this annotator.
	# The estimation will be done by the Learner with access to both the Annotator
	# object and the Model object.

	def update_estimate(self, new_params = {}):
		"""
			Update the estimate of the annotator parameters.
		"""
		self.est_params = new_params

	def get_current_estimate(self):
		"""
			Access the current estimates of this annotator's parameters.
		"""
		return self.est_params

	def get_answers(self):
		# return labels this annotator has provided in the past.
		pass

	def answer_queries(self, dataset, indices_to_query, info={}):
		"""
			Return the annotation of the queries (simulated annotator)
		"""
		Y_answered = self.Z[ indices_to_query ]

		len_indices_to_query = 0
		try:
			len_indices_to_query = len(indices_to_query)
		except:
			len_indices_to_query = 1

		# Depending on the noise type, we return different labels
		if self.noise_type == 0:
			# It has an average noise rate across the entire dataset
			for i in range(len(Y_answered)):
				if random.random() < self.noise_level:
					# Change the value of Y_answered[i] to something random
					Y_answered[i] = random.choice( dataset.categories )

		elif self.noise_type == 1:
			# It has a class conditional scalar average
			for i in range(len_indices_to_query):
				try:
					if len_indices_to_query == 1:
						y = Y_answered
					else:
						y = Y_answered[i]
					if y in self.noise_level:
						noise_value_per_class = self.noise_level[ y ]
						if random.random() < noise_value_per_class:
							# Change the value of Y_answered[i] to something random
							Y_answered[i] = random.choice( dataset.categories )
					else:
						# If the provided noise_level dictionary does not have information for this class,
						# we let the ground truth value unchanged
						pass

				except:
					# If something goes wrong, we let the ground truth value unchanged.
					print_errors( '@ NoisedGroundTruthAnnotator' )
					pass


		return Y_answered