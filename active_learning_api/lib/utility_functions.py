import sys, traceback
import random
import numpy as np
from ScikitModel import ScikitModel
from TrueAnnotatorEstimator import TrueAnnotatorEstimator
from NoisedAnnotatorEstimator import NoisedAnnotatorEstimator
from VaryingNoisedAnnotatorEstimator import VaryingNoisedAnnotatorEstimator
from LabelBasedAnnotatorEstimator import LabelBasedAnnotatorEstimator
from sklearn import linear_model, svm
import itertools
from utils import nCr, save_pickle, print_errors
from sklearn.cluster import k_means, MeanShift, MiniBatchKMeans, AffinityPropagation, AgglomerativeClustering, DBSCAN
from sklearn.neighbors import NearestNeighbors
from sklearn.grid_search import GridSearchCV
from sklearn.neighbors import KernelDensity
import tensorflow as tf

"""
	Author : Shane Moon
	Description :
		Utility functions for MaxUtilityLearner.py

		Every feature method takes the following form:

		def utility_function_abstract( dataset, model, annotators ):
			# Calculate utility given a <Dataset>, <Model>, and <List> of <Annotator>s

			return { annotator_index : <np.array> of feature values }
"""

def get_utility( dataset, model, annotators, utility_params={} ):
	"""
		# Calculate utility given a <Dataset>, <Model>, and <List> of <Annotator>s

		utility_params : {
			'active_features' : <List> of handles to feature methods (defined below)
			'active_features_params' : <List> of feature params in the same order as in 'active_features'
			'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
			'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
			'dynamic' : {
                # Params related to dynamic proactive learning (DPAL)
                'weight_update_method' : e.g. utility_functions.dpal_update
                'main_percentage' : percentage of # samples for the "main' model
                'M' : # of clusters for "extra" models
                'G' : # of samples in each 'extra' model cluster
                'extra_model_group_method'
                  0 - K-means clustering
                  1 - K-means clustering + 10-NN of centroids 
                'extra_model_group_space'
                  0 - Original concept space
                  1 - Utility metrics space
            }
            'utility_sample'
              0 - Sort the utility tuples by their utility value, and select the best `n_queries' candidates
              1 - Weighted sample of tuples by their normalized utility value
            'annotator_selection'
              0 - Choose an annotator for each instance with the best proactive_features[0] value
              1 - Choose an annotator for each instance with the best overall value
              2 - Allow duplicate annotators
		}


		utility_tuples = [ (utility_value, index, annotator_index), ... ] 
		return utility_tuples
	"""

	################################    (1) Load Parameters    ################################

	# Load paramter values
	get_utilities_of = utility_params['get_utilities_of'] if ('get_utilities_of' in utility_params) else 'train_unlabeled'
	active_features = utility_params['active_features'] if ('active_features' in utility_params) else [ random_value ]
	active_features_params = utility_params['active_features_params'] if ('active_features_params' in utility_params) else [ {} ] * len(active_features) 
	proactive_features = utility_params['proactive_features'] if ('proactive_features' in utility_params) else [ ]
	proactive_features_params = utility_params['proactive_features_params'] if ('proactive_features_params' in utility_params) else [ {} ] * len(proactive_features) 
	dynamic_params = utility_params['dynamic_params'] if ('dynamic_params' in utility_params) else {}
	b_log_weight = utility_params['b_log_weight'] if ('b_log_weight' in utility_params) else False
	annotator_selection = utility_params['annotator_selection'] if ('annotator_selection' in utility_params) else 0

	# If active_features and proactive_features were passed as strings
	if len(active_features) > 0 and type(active_features[0]) == str:
		active_features = [eval(active_feature) for active_feature in active_features]
	if len(proactive_features) > 0 and type(proactive_features[0]) == str:
		proactive_features = [eval(proactive_feature) for proactive_feature in proactive_features]

	# Further determine whether it is going to be DPAL.
	if dynamic_params == {}:
		do_dynamic = False
	else:
		# If it is DPAL, load all the parameters for DPAL.
		do_dynamic = True
		active_feature_weights = dynamic_params['active_feature_weights'] if ('active_feature_weights' in dynamic_params) else np.ones( len(active_features) )
		proactive_feature_weights = dynamic_params['proactive_feature_weights'] if ('proactive_feature_weights' in dynamic_params) else np.ones( len(proactive_features) )
		regressor = dynamic_params['regressor'] if ('regressor' in dynamic_params) else None
		weight_update_method = dynamic_params['weight_update_method'] if ('weight_update_method' in dynamic_params) else dpal_update
		regression_method = dynamic_params['regression_method'] if ('regression_method' in dynamic_params) else 'linear'
		start_conditions = dynamic_params['start_conditions'] if 'start_conditions' in dynamic_params else [ dpal_update_test_accuracy ]
		start_conditions_params = dynamic_params['start_conditions_params'] if 'start_conditions_params' in dynamic_params else [ {} ]
		update_conditions = dynamic_params['update_conditions'] if 'update_conditions' in dynamic_params else [ dpal_update_every_recurring ]
		update_conditions_params = dynamic_params['update_conditions_params'] if 'update_conditions_params' in dynamic_params else [ {} ]
		stop_conditions = dynamic_params['stop_conditions'] if 'stop_conditions' in dynamic_params else [  ]
		stop_conditions_params = dynamic_params['stop_conditions_params'] if 'stop_conditions_params' in dynamic_params else [  ]
		dynamic_params['b_log_weight'] = b_log_weight
		dynamic_params['regression_method'] = regression_method
		started_dynamic = dynamic_params['started_dynamic'] if ('started_dynamic' in dynamic_params) else False

	# If anything's wrong, assert
	assert( len(active_features) == len(active_features_params) )
	assert( len(proactive_features) == len(proactive_features_params) )
	if do_dynamic:
		assert( len(active_features) == len(active_feature_weights) )
		assert( len(proactive_features) == len(proactive_feature_weights) )


	#############################    (2) Dynamic Weight Update    #############################

	# Update linear weights for DPAL.
	if do_dynamic == True:
		if started_dynamic and check_conditions(dataset, model, annotators, stop_conditions, stop_conditions_params):
			# Check if we need to stop dpal
			print "Stopping DPAL for this learning...."
			do_dynamic = False
			dynamic_params = {}
			utility_params['dynamic_params'] = {}
		elif ((not started_dynamic) and check_conditions(dataset, model, annotators, start_conditions, start_conditions_params)) or (started_dynamic and check_conditions(dataset, model, annotators, update_conditions, update_conditions_params)):
			# Obtain new updates according to the given weight_update_method
			print "Updating the DPAL weights ..."
			started_dynamic = True
			dynamic_params['started_dynamic'] = True
			active_feature_weights, proactive_feature_weights, regressor = weight_update_method( dataset, model, annotators, active_features, active_features_params, proactive_features, proactive_features_params, dynamic_params )

			# Update the parameters
			dynamic_params['active_feature_weights']    = active_feature_weights
			dynamic_params['proactive_feature_weights'] = proactive_feature_weights
			dynamic_params['regressor'] = regressor
		else:
			# Then we don't need to update weights this iteration. Simply use what we have.
			pass

	##############################    (3) Utility Calculation    ##############################

	# Active feature value vector : (n x 1) 
	indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )

	# Now we start calculating utility. The results will be saved in utility_tuples as well as separately.
	utility_tuples = []
	utility_shape = len(indices) if annotator_selection == 0 else len(indices) * len(annotators)
	utility_values = np.empty( shape=utility_shape )
	utility_indices = np.empty( shape=utility_shape, dtype=int )
	utility_annotators = np.empty( shape=utility_shape )
	
	(dict_utility, active_features_params, proactive_features_params) = \
	get_utility_metrics( indices, dataset, model, annotators, 
		active_features, active_features_params, 
		proactive_features, proactive_features_params, {'log_weight' : b_log_weight, 'proactive_all' : True, 'proactive_best' : True} )


	if len(proactive_features) > 0:

		if do_dynamic and started_dynamic:
			#######################################################################
			# To do: check if it is in the same order
			#######################################################################	
			feature_weights = np.append( active_feature_weights, proactive_feature_weights )

		if annotator_selection == 0:
			# Choose an annotator for each instance with the best proactive_features[0] value
			proactive_best = dict_utility['proactive_best']
			best_annotators = dict_utility['best_annotators']

			if do_dynamic and started_dynamic and regression_method != 'linear':
				proactive_utility_value_vector = regressor.predict( proactive_best )
			
			elif do_dynamic and started_dynamic:
				# Additive with weights
				proactive_utility_value_vector = np.dot(proactive_best, feature_weights)
			
			else:
				# Multiplicative
				proactive_utility_value_vector = np.prod( proactive_best, axis=1 )

			for j in range(len(indices)):
				index = indices[j]  # now index refers to the global index within dataset
				best_annotator_index = best_annotators[j]
				utility_value = proactive_utility_value_vector[j]
				utility_tuple = ( utility_value, index, best_annotator_index )
				utility_tuples.append( utility_tuple )
				utility_values[j] = utility_value
				utility_indices[j] = index
				utility_annotators[j] = best_annotator_index

		elif annotator_selection in [1, 2]:
			# proactive_all: [num_indices, num_cols, num_annotators]				
			proactive_all = dict_utility['proactive_all']

			# Allow duplicates
			for k in range(len(annotators)):
				# For each annotator

				if do_dynamic and started_dynamic and regression_method != 'linear':
					proactive_utility_value_vector = regressor.predict( proactive_all[:,:,k] )
				
				elif do_dynamic and started_dynamic:
					# Additive with weights
					proactive_utility_value_vector = np.dot(proactive_all[:,:,k], feature_weights)
				
				else:
					# Multiplicative			
					proactive_utility_value_vector = np.prod( proactive_all[:,:,k], axis=1 )

				# Append to the utility tuples list
				for j in range(len(indices)):
					index = indices[j]  # now index refers to the global index within dataset
					utility_value = proactive_utility_value_vector[j]
					utility_tuple = ( utility_value, index, k )
					utility_tuples.append( utility_tuple )
					utility_values[k * len(annotators) + j] = utility_value
					utility_indices[k * len(annotators) + j] = index
					utility_annotators[k * len(annotators) + j] = k

			if annotator_selection == 1:
				# Choose an annotator for each instance with the best overall value
				# Leave only the tuples with the annotators that yield the best utility for each instance
				for utility_tuple in utility_tuples:
					pass

	else:
		# active_feature_matrix: [ num_indices X num_active_features ]
		active_feature_matrix = dict_utility['active']

		if do_dynamic and started_dynamic and regression_method not in ['linear', 'linear_tensorflow']:
			active_utility_value_vector = regressor.predict( active_feature_matrix )
		
		elif do_dynamic and started_dynamic:
			# Additive with weights
			active_utility_value_vector = np.dot(active_feature_matrix, active_feature_weights).flatten()
		
		else:
			# Multiplicative
			active_utility_value_vector = np.prod( active_feature_matrix, axis=1 )

		# Append to the utility tuples list
		for j in range(len(indices)):
			index = indices[j]  # now index refers to the global index within dataset
			utility_value = active_utility_value_vector[j]
			random_annotator_index = random.choice(range(len(annotators)))
			utility_tuple = ( utility_value, index, random_annotator_index )
			utility_tuples.append( utility_tuple )
			utility_values[j] = utility_value
			utility_indices[j] = index
			utility_annotators[j] = random_annotator_index


	###########################################################################################

	return utility_tuples, utility_values, utility_indices, utility_annotators, utility_params





##########################################################################################
#
#	Below are active feature methods.
#	- f_random_value
#	- f_entropy
#	- f_density
#
#	Every feature method function needs to return in the following format:
#	Active feature value vector : (n x 1) 
#		where n := len(unlabeled_indices) if params['get_utilities_of']
#
##########################################################################################

def random_value( dataset, model, annotators, params={} ):
	"""
		Randomly assign utility values for each index in the dataset
	"""
	get_utilities_of = params['get_utilities_of']
	indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )
	
	return np.random.rand( num_samples ), params


def entropy( dataset, model, annotators, params={} ):
	"""
		Assign utility based on its entropy; 
		Instances with higher entropy (uncertainty) should get higher utility
	"""
	# ------------------------------------- #
	# ----------- Need Debuggin ----------- #
	# ------------------------------------- #

	get_utilities_of = params['get_utilities_of']
	indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )
	feature_value_vector = np.zeros( num_samples )

	# probs; [  n_samples  X  n_classes  ]	
	probs = model.predict_proba( dataset.X[ indices ] )

	for i in range(len( indices )):
		# Assuming unlabeled_indices is sorted,
		# the class_post_prob of an instance (globally) at index is at probs[i,:]
		class_post_prob = get_entropy( probs[i,:] )
		feature_value_vector[i] = class_post_prob

	if ('normalize' in params) and (params['normalize'] == True):
		# Normalization
		
		# Entropy ranges from 0 (when prob = [1,0,0,...], to -|C| * (1/|C|) * log(1/|C|))
		num_categories = len(dataset.categories)
		max_entropy = - np.log( 1/float(num_categories) )
		feature_value_vector /= max_entropy
		"""
		max_entropy = np.max(feature_value_vector)
		min_entropy = np.min(feature_value_vector)
		feature_value_vector = (feature_value_vector - min_entropy) / (max_entropy - min_entropy)
		"""

	return feature_value_vector, params

def unknownness( dataset, model, annotators, params={} ):
	"""
		Measure of unknownness of information in the region.
		This is different from 'uncertainty' metric of samples,

	"""
	#print "Estimating the unknownness ..."
	unknownness_est_type = params['unknownness_est_type'] if 'unknownness_est_type' in params else 0
	X = dataset.X

	if unknownness_est_type == 0:

		# GMM Based Density Estimation
		"""
		from sklearn import mixture
		gmm = mixture.GMM(n_components=24, covariance_type='diag', tol=1e-3, n_iter=50)

		# Estimate the density distribution of the dataset
		gmm.fit( X[dataset.train_labeled_indices] )
		print 'Done fitting ...'

		# Density has the same number of instances as dataset.X
		print 'Sampling ...'
		unknownness_dist =  -np.exp( gmm.score_samples( X )[0] )
		"""

		kd = KernelDensity(bandwidth=0.8, atol=1e-3, rtol=1e-6)

		# Estimate the density distribution of the dataset
		try:
			kd.fit( X[dataset.train_labeled_indices] )
		except:
			X = X.toarray()		
			kd.fit( X[dataset.train_labeled_indices] )
		#print 'Done fitting ...'
		
		# Density has the same number of instances as dataset.X
		#print 'Sampling ...'
		unknownness_dist =  -np.exp( kd.score_samples( X ) )

		"""
		# use grid search cross-validation to optimize the bandwidth
		gridsearch_params = {'bandwidth': np.logspace(-1, 1, 20)}
		grid = GridSearchCV(KernelDensity(), gridsearch_params)
		grid.fit( X[dataset.train_labeled_indices] )

		print("best bandwidth: {0}".format(grid.best_estimator_.bandwidth))

		# use the best estimator to compute the kernel density estimate
		kd = grid.best_estimator_
		
		# Density has the same number of instances as dataset.X
		print 'Sampling ...'
		unknownness_dist =  -np.exp( kd.score_samples( X ) )
		"""
		#print 'Sampling done ...'
	
	if unknownness_est_type == 1:
		# Quicker version of density estimation
		# Precluster, count # of unlabeled samples in the cluster, etc.
		num_clusters = 20

		if 'cluster_info' not in params:
			# Perform K-means clustering
			# centroids : (k x p)
			# labels    : (n x 1)
			(centroids, labels, inertia) = k_means(X, num_clusters, random_state=0, max_iter=100, n_init=2, verbose=False)
			num_all_samples_in_clusters = np.zeros( num_clusters, dtype=int )
			for c in range(num_clusters):
				num_all_samples_in_clusters[c] = len( np.where( labels[dataset.train_indices] == c )[0] ) + 1 # offset

			params['cluster_info'] = (centroids, labels, inertia, num_all_samples_in_clusters)

		else:
			# Load the previously recorded cluster information
			(centroids, labels, inertia, num_all_samples_in_clusters) = params['cluster_info']

		# Count the number of labeled samples per cluster
		num_labeled_samples_in_clusters = np.zeros( num_clusters, dtype=int )
		for c in range(num_clusters):
			num_labeled_samples_in_clusters[c] = len( np.where( labels[dataset.train_labeled_indices] == c )[0] ) 


		# Calculate unknownness distribution for each sample
		unknownness_dist = np.zeros( X.shape[0] )
		for c in range(num_clusters):
			unknownness_dist[ np.where( labels == c )[0] ] = 1 - num_labeled_samples_in_clusters[c] / float(num_all_samples_in_clusters[c])


	if ('normalize' in params) and (params['normalize'] == True):
		max_unknownness  = max(unknownness_dist) 
		min_unknownness  = min(unknownness_dist)
		diff_max_min = float(max_unknownness - min_unknownness)
		unknownness_dist = (unknownness_dist - min_unknownness) / diff_max_min


	# Return density for the samples we want
	get_utilities_of = params['get_utilities_of']
	indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )

	feature_value_vector = unknownness_dist[indices]

	return feature_value_vector, params

def density( dataset, model, annotators, params={} ):
	"""
		Density-based sampling, with (1) kernel desnsity estimation and (2) K-means clustering.
		`params` contains density information
	"""

	# Obtain Density
	if 'density_dist' in params:
		# If we have a density pre-calculated already
		density_dist = params['density_dist']
	else:
		print "Estimating the density ..."
		density_est_type = params['density_est_type']
		X = dataset.X

		# We estimate the density of the entire dataset
		if density_est_type == 0:
			# Kernel Density Estimation
			from sklearn.neighbors import KernelDensity
			kd = KernelDensity(bandwidth=0.5, atol=1e-2, rtol=1e-6)
			
			# Estimate the density distribution of the dataset
			try:
				kd.fit( X )
			except:
				X = X.toarray()
				kd.fit( X )
			print 'Done fitting type 0...'

			# Density has the same number of instances as dataset.X
			print 'Sampling ...'
			#density_dist = kd.score_samples( X )			
			density_dist =  np.exp( kd.score_samples( X ) )

			if ('normalize' in params) and (params['normalize'] == True):
				#density_dist /= np.linalg.norm( density_dist )
				max_density  = max(density_dist) 
				min_density  = min(density_dist)
				diff_max_min = float(max_density - min_density)
				print 'Normalizing density ... max: %.5f, min: %.5f, diff_max_min: %.5f' % (max_density, min_density, diff_max_min)
				density_dist = (density_dist - min_density) / diff_max_min
				print 'Completed normalizing density ... max: %.5f, min: %.5f' % (max(density_dist) , min(density_dist))
			else:
				pass

			print 'Sampling done ...'


		elif density_est_type == 1:
			# Density estmation based on K-means clustering
			# It will be useful for larger dataset.
			num_clusters = 20

			# Perform K-means clustering
			# centroids : (k x p)
			# labels    : (n x 1)
			(centroids, labels, inertia) = k_means(X, num_clusters, random_state=0, max_iter=100, n_init=2, verbose=False)

			# Count the number of samplese in each cluster
			num_instances_per_cluster = [ len(np.where( labels == i )[0]) for i in range(num_clusters) ]

			# Sum up the total distances of instances from centroids of each cluster
			# Divide the number of counts per each cluster by this "size" of the cluster
			density_per_cluster = []
			for i in range(num_clusters):
				centroid = centroids[i]
				distance_to_centroid = 0.0001 # offset

				for x_index in np.where( labels == i )[0]:
					x = X[x_index]
					distance_to_centroid += np.linalg.norm((x - centroid), ord=1)
			
				density_per_cluster.append( num_instances_per_cluster[i] / distance_to_centroid )

			############################################
			###  Might want to tune this a bit more ###
			############################################
			if ('normalize' in params) and (params['normalize'] == True):
				# Normalization of density so it ranges from [0,1]
				max_density  = max(density_per_cluster) 
				min_density  = min(density_per_cluster)
				diff_max_min = float(max_density - min_density)
				print 'Normalizing density ... max: %.2f, min: %.2f, diff_max_min: %.2f' % (max_density, min_density, diff_max_min)
				density_per_cluster = [ (d - min_density) / diff_max_min  for d in density_per_cluster ]
			print 'Density for each cluster: %s' % str(density_per_cluster)

			# Assign the density of an instance as the density of its enclosing cluster
			density_dist = np.zeros( X.shape[0], dtype='float' )
			for i in range(num_clusters):
				density_dist[ np.where( labels == i )[0] ] = density_per_cluster[i]


		elif density_est_type == 2:
			# GMM Based Density Estimation
			from sklearn import mixture
			#gmm = mixture.GMM(n_components=3, covariance_type='diag', tol=2e-3, n_iter=50)
			gmm = mixture.GMM(n_components=24, covariance_type='diag', tol=1e-3, n_iter=50)
			#gmm = mixture.VBGMM(n_components=3, covariance_type='diag', tol=0.001, verbose=True, n_iter=10, params='wmc', init_params='wmc')
			#gmm = mixture.DPGMM(n_components=3, covariance_type='diag', tol=0.001, verbose=True, n_iter=10, params='wmc', init_params='wmc')
			#from sklearn import decomposition
			#gmm = decomposition.PCA()

			# Estimate the density distribution of the dataset
			#X = X.toarray()
			try:
				gmm.fit( X )
			except:
				X = X.toarray()
				gmm.fit( X )			
			print 'Done fitting type 2 ...'

			# Density has the same number of instances as dataset.X
			print 'Sampling ...'
			#density_dist = gmm.score_samples( X )[0]
			density_dist =  np.exp( gmm.score_samples( X )[0] )
			#print min(density_dist), max(density_dist)
			#print density_dist

			if ('normalize' in params) and (params['normalize'] == True):
				#density_dist /= np.linalg.norm( density_dist )
				max_density  = max(density_dist) 
				min_density  = min(density_dist)
				diff_max_min = float(max_density - min_density)
				print 'Normalizing density ... max: %.5f, min: %.5f, diff_max_min: %.5f' % (max_density, min_density, diff_max_min)
				density_dist = (density_dist - min_density) / diff_max_min
				print 'Completed normalizing density ... max: %.5f, min: %.5f' % (max(density_dist) , min(density_dist))
			else:
				pass

			print 'Sampling done ...'



		if density_est_type in [0, 1, 2]:
			# These are the pre-clustering methods, thus do not need to be updated at every iteration.
			params['density_dist'] = density_dist
		else:
			# For other methods, we need to update our density estimation at next iteration.
			# We therefore do not cache 'density_dist' at all.
			pass

	# Return density for the samples we want
	get_utilities_of = params['get_utilities_of']
	indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )

	feature_value_vector = density_dist[indices]
	return feature_value_vector, params


def even_cluster( dataset, model, annotators, params={} ):
	"""
		Precluster the dataset into |P| clusters, get nearest neighbors 
		from each centroid, and order them by cycling through those points.

		Let Cij the j'th closest point from a centroid of a cluster i
		Let i \in {0, ..., I} and j \in {0, ..., |Ci|}
		The ordering is as follows:

		C00, C10, C20, .., CI0, C01, C11, C21, ...
	"""

	cluster_within_get_utilites_of = params['cluster_within_get_utilites_of'] if 'cluster_within_get_utilites_of' in params else False

	# Obtain Density
	if 'order_values' in params:
		# If we have a density pre-calculated already
		order_values = params['order_values']

	else:
		if cluster_within_get_utilites_of:
			get_utilities_of = params['get_utilities_of']
			indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )
			X = dataset.X[indices]
		else:
			X = dataset.X
		order_values = np.empty( X.shape[0] )

		# 1. Cluster the dataset
		num_clusters = params['num_clusters'] if 'num_clusters' in params else len(dataset.categories) * 2
		(centroids, labels, inertia) = k_means(X, num_clusters, random_state=0, max_iter=100, n_init=2, verbose=False)
		num_clusters = len(np.unique( labels ))


		# 2. Obtain indices of samples per each cluster
		indices_per_cluster = {}
		num_indices_left_per_cluster = {}

		for cluster_index in range(num_clusters):
			indices_at_cluster_index = np.where( labels == cluster_index )[0]

			# 3. Order indices of each cluster by their proximity to centroid
			samples_at_cluster_index = X[indices_at_cluster_index]
			centroid = centroids[cluster_index, :]

			# First get distances from the centroid to each sample in this cluster
			distances_to_centroid = np.linalg.norm( samples_at_cluster_index - centroid, axis=1 )

			# Sort indices by their representing samples' reverse proximity to the cnetroid
			# Index of the furthest point comes first.
			indices_at_cluster_index = indices_at_cluster_index[ np.argsort( -distances_to_centroid ) ]


			# 4. Record sorted indices of samples per each cluster
			indices_per_cluster[cluster_index] = indices_at_cluster_index
			num_indices_left_per_cluster[cluster_index] = len( indices_at_cluster_index )
			
		
		# 5. Cycle through each cluster and append to `order_values'
		d_cluster_indices_left = {}
		for i in range(num_clusters):
			d_cluster_indices_left[i] = True

		j = len(order_values)
		while len( d_cluster_indices_left ) > 0:

			cluster_indices_left = d_cluster_indices_left.keys()
			random.shuffle( cluster_indices_left )

			for cluster_index in cluster_indices_left:
				closest_point_index_in_this_cluster = num_indices_left_per_cluster[cluster_index] - 1
				order_values[  indices_per_cluster[cluster_index][closest_point_index_in_this_cluster]  ] = j

				# Update the number of indices left for this cluster
				num_indices_left_per_cluster[cluster_index] -= 1
				j -= 1

				# If no sample is left in this cluster, remove it from d_cluster_indices_left
				if num_indices_left_per_cluster[cluster_index] == 0:
					del d_cluster_indices_left[cluster_index]

		# Sanity check
		assert(j == 0 )

		# n. Save in params so this is computed only once per dataset.
		order_values /= len(order_values)
		params['order_values'] = order_values

	# Return density for the samples we want
	if cluster_within_get_utilites_of:
		feature_value_vector = order_values
	else:
		get_utilities_of = params['get_utilities_of']
		indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )		
		feature_value_vector = order_values[indices]

	return feature_value_vector, params


def greedy_gt_maximum_improvement( dataset, model, annotators, params={} ):
	"""
		Order samples based on their actual future improvement,
		using the ground-truth sample information.
		** For debugging only **
	"""
	get_utilities_of = params['get_utilities_of']
	indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )
	feature_value_vector = np.zeros( num_samples )


	return feature_value_vector, params




##########################################################################################
#
#	Below are proactive feature methods.
#	- pro_p_answer
#
#	Every feature method function needs to return in the following format:
#	Proactive feature value vector : |K| * (n x 1) 
#		where n := len(unlabeled_indices) if params['get_utilities_of']
#		      K := <List> of annotators
#
##########################################################################################


def pro_p_answer( dataset, model, annotators, params={} ):
	"""
		Assign value based on the probability of getting a right answer.
		
		params : {
			'p_ans_est_type' : 0 - "True" Estimate 
			                 : 1 - "Noised" Estimate
			                 : 2 - "Noised" Estimate with improving accuracy
			                 : 3 - Estimation with parametric annotator model
		}
		Return: (n x |K|) np.arrays
		'p_answers' : (N x |K|) np.arrays, where N is the entire dataset
	"""
	# Obtain probability of getting a correct answer
	if 'p_answers' in params:
		# If we have a density pre-calculated already
		p_answers = params['p_answers']
	else:
		# This is what we will return.
		num_annotators = len(annotators)
		p_answers = np.empty( ( dataset.X.shape[0], num_annotators ) )
		p_ans_est_type = params['p_ans_est_type']

		if p_ans_est_type == 0:
			# Perfect estimation; just simply load annotators' known noise rate
			estimator = TrueAnnotatorEstimator()

		elif p_ans_est_type == 1:
			# Noised estimation; 
			estimator = NoisedAnnotatorEstimator()

		elif p_ans_est_type == 2:
			# Noised estimation with improving accuracy (decreasing sigma)
			estimator = VaryingNoisedAnnotatorEstimator()			

		elif p_ans_est_type == 3:
			# Estimation with each annotator's labels
			if isinstance(model, ScikitModel):
				model_params = {
					'clf_type' : model.clf_type,
					'clf_params' : model.clf_params
				}
				params['model_params'] = model_params
				params['model_type'] = 0
			estimator = LabelBasedAnnotatorEstimator()			

		else:
			# Else, return a random estimation
			estimator = RandomAnnotatorEstimator()

		p_answers = estimator.estimate(dataset, annotators, params)

		if p_ans_est_type in [0, 1]:
			# These are the pre-estimation methods, thus do not need to be updated at every iteration.
			params['p_answers'] = p_answers
		else:
			# For other methods, we need to update our density estimation at next iteration.
			# We therefore do not cache 'p_answers' at all.
			pass

	get_utilities_of = params['get_utilities_of']
	indices, num_samples = get_indices_num_samples( dataset, get_utilities_of )
	feature_value_matrix = p_answers[ indices, : ]
	return feature_value_matrix, params




##########################################################################################
#
#	Below are methods related to dynamic proactive weight update
#	- dpal_update
#
##########################################################################################


def dpal_update( dataset, model, annotators, active_features, active_features_params, proactive_features, proactive_features_params, dynamic_params ):
	# 0. Unpack all the params
	main_percentage = dynamic_params['main_percentage'] if ('main_percentage' in dynamic_params) else 0.8
	M = dynamic_params['M'] if ('M' in dynamic_params) else 20
	G = dynamic_params['G'] if ('G' in dynamic_params) else 20
	choose_M_method = dynamic_params['choose_M_method'] if ('choose_M_method' in dynamic_params) else 0	
	extra_model_group_method = dynamic_params['extra_model_group_method'] if ('extra_model_group_method' in dynamic_params) else 1
	extra_model_group_space = dynamic_params['extra_model_group_space'] if ('extra_model_group_space' in dynamic_params) else 1
	b_log_weight = dynamic_params['b_log_weight']
	regression_method = dynamic_params['regression_method']
	#regression_method_params = dynamic_params['regression_method_params'] if 'regression_method_params' in dynamic_params else {}
	improvement_measure = 1

	# 1. Choose main_percentage ~:= 80%, and |M|=20 clusters for the remaining indices, and build models for each
	test_indices = dataset.test_indices
	labeled_indices = dataset.train_labeled_indices
	M = min( M, int( len(labeled_indices) * (1-main_percentage) ) )

	"""
		To do: in building the "main" model:
		- Option 1. Randomly select 60~70 percent of labeled indices
		- Option 2. Select the first 60~70 percent of labeled indices by the order they were annotated
		- Option 3. Select the "center-most" 60~70 percent of labeled indices
	"""
	if choose_M_method == 0:
		# Choose first main_percentage samples by their indices
		n_main_samples = int(len(labeled_indices) * main_percentage)
		main_indices = labeled_indices[0:n_main_samples]	

		# Takes the rest after main_percentage indices as pool
		extra_indices_pool = labeled_indices[n_main_samples:] 

	# 2. Build the main model
	parent_model_params = model.params
	main_model = ScikitModel( params=parent_model_params )
	main_model.train( dataset.X[main_indices], dataset.Ysingle[main_indices] )


	# 3. Build extra tuned models	
	list_extra_models = []
	list_extra_indices = []

	"""
		To do: in building the "extra" models:
		- Option 1: Apply conventional clustering methods (uneven) on the rest of the labeled indices
		- Option 2: Obtain centroids via conventional methods, make new groupings by taking |G|-closest neighbors of each centroid.
		- Option 3: Equal size clustering method.

		Output:
			- list_extra_models
			- list_extra_indices
	"""	

	if extra_model_group_space == 0:
		# Group in the original concept space
		X = dataset.X[extra_indices_pool]

	elif extra_model_group_space == 1:
		# Group in utility metrics space
		(dict_utility, active_features_params, proactive_features_params) = \
		get_utility_metrics( extra_indices_pool, dataset, main_model, annotators, 
			active_features, active_features_params, 
			proactive_features, proactive_features_params, {'log_weight' : b_log_weight, 'proactive_real' : True} )

		# dict_utility['proactive_real'] : [ num_indices X num_active_features + num_proactive_features ]
		if len(proactive_features) > 0:
			X = dict_utility['proactive_real']
		else:
			X = dict_utility['active']

	if extra_model_group_method == 0:
		# (0): Perform K-means clustering
		# labels    : (n x 1)
		clusterer = MiniBatchKMeans(n_clusters=M, init='k-means++', n_init=1, init_size=1000, batch_size=1000)
		clusterer.fit( X )
		labels = clusterer.labels_
		#assert( M == len(np.unique( labels )) )

		sizes_of_clusters = [ len(np.where( labels == label )[0] ) for label in np.unique( labels ) ]
		min_cluster_size = min(sizes_of_clusters)

		for label in np.unique( labels ):
			# Get indices of each cluster
			extra_indices = extra_indices_pool[ np.where( labels == label ) ]

			# Make sure that every cluster has the same size
			extra_indices = extra_indices[:min_cluster_size]

			# Append it to the main indices
			indices = np.append( main_indices, extra_indices )

			######################################################################
			### The parent might not be a <ScikitModel>. For now it is assumed ###
			######################################################################	
			extra_model = ScikitModel( params=parent_model_params )
			extra_model.train( dataset.X[indices], dataset.Ysingle[indices] )
			list_extra_models.append( extra_model )
			list_extra_indices.append( extra_indices )

	elif extra_model_group_method == 1:
		# (1): Perform K-means clustering, get centroids, and get |G| closest neighbors for each cluster.
		# labels    : (n x 1)
		# centroids : (n x p)
		clusterer = MiniBatchKMeans(n_clusters=M, init='k-means++', n_init=1, init_size=1000, batch_size=1000)
		clusterer.fit( X )
		labels = clusterer.labels_
		centroids = clusterer.cluster_centers_

		sizes_of_clusters = []

		# Calculate nearest neighbors for X
		G = min(G, X.shape[0])
		#nbrs = NearestNeighbors().fit( X )
		nbrs = NearestNeighbors(n_neighbors=G).fit( X )

		for cluster_index in range(len(np.unique( labels ))):
			# Get centroid of each cluster
			centroid = centroids[cluster_index, :]

			# Get |G|-closest indices of each cluster.
			#extra_indices = extra_indices_pool[ np.where( labels == label ) ]
			distances, neighbor_indices = nbrs.kneighbors( centroid )
			extra_indices = extra_indices_pool[ neighbor_indices[0] ]

			# Sanity check; extra indices and main_indices don't overlap
			#assert( len(np.unique(np.append(extra_indices, main_indices))) == len(extra_indices) + len(main_indices) )

			sizes_of_clusters.append( len(extra_indices) )

			# Append it to the main indices
			indices = np.append( main_indices, extra_indices )

			######################################################################
			### The parent might not be a <ScikitModel>. For now it is assumed ###
			######################################################################	
			extra_model = ScikitModel( params=parent_model_params )
			extra_model.train( dataset.X[indices], dataset.Ysingle[indices] )
			list_extra_models.append( extra_model )
			list_extra_indices.append( extra_indices )

		min_cluster_size = min(sizes_of_clusters)


	elif extra_model_group_method == 2:
		# no grouping, use all data
		for extra_index in extra_indices_pool:
			indices = np.append( main_indices, extra_index )

			######################################################################
			### The parent might not be a <ScikitModel>. For now it is assumed ###
			######################################################################	
			extra_model = ScikitModel( params=parent_model_params )
			extra_model.train( dataset.X[indices], dataset.Ysingle[indices] )
			list_extra_models.append( extra_model )
			list_extra_indices.append( np.array([extra_index]) )

		sizes_of_clusters = []
		min_cluster_size = 1

	# 4. For each extra model
	num_rows = len(list_extra_models)
	num_cols = len(active_features) + len(proactive_features)

	delta_feature_matrix = np.empty( (num_rows, num_cols) )
	delta_accuracies = np.empty( num_rows )

	for m in range(len(list_extra_models)):
		# 4.1 Calculate the sum of utilities over extra_indices to the main model. These are 'X' (\psi(X, K, L))
		extra_model   = list_extra_models[m]
		extra_indices = list_extra_indices[m]
		len_extra_indices = len(extra_indices)

		# Get utility metrics
		(dict_utility, active_features_params, proactive_features_params) = \
		get_utility_metrics( extra_indices, dataset, extra_model, annotators, 
			active_features, active_features_params, 
			proactive_features, proactive_features_params, {'log_weight' : b_log_weight, 'avg_rows' : True, 'proactive_real' : True} )
		
		# 4-2. Calculate the difference in overall accuracy over dataset.test_indices between two models. These are 'Y' (\delta accuracy)
		if improvement_measure == 0:
			accuracy_extra = extra_model.test( dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )
			accuracy_main  = main_model.test(  dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )

		elif improvement_measure == 1:
			accuracy_extra = -extra_model.measure_avg_entropy( dataset.X[dataset.test_indices] )
			accuracy_main  = -main_model.measure_avg_entropy(  dataset.X[dataset.test_indices] )

		delta_accuracy = (accuracy_extra - accuracy_main)

		# Append the results
		if len(proactive_features) > 0:
			delta_feature_matrix[m, :] = dict_utility['avg_rows_proactive_real']
		else:
			delta_feature_matrix[m, :] = dict_utility['avg_rows_active']			
		delta_accuracies[m] = delta_accuracy * 100


	# 5. Perform Linear Regression on 'X' and 'Y'. Divide it into `active_feature_weights' and `proactive_feature_weights'
	"""
		(2) To Do:
		Instead of linear regression, kernel regression?

		sklearn.svm.LinearSVR
		sklearn.svm.SVR(kernel='linear')
	"""	

	# Weird things happen especially with an SVD classifier
	delta_feature_matrix[ np.isnan(delta_feature_matrix) ] = 1.0

	if regression_method == 'linear':
		#clf = linear_model.LinearRegression()
		clf = svm.SVR(kernel='linear')
		clf.fit( delta_feature_matrix, delta_accuracies )
		#weights = clf.coef_   # for LinearRegression()
		weights = clf.coef_[0] # for SVR

		active_feature_weights = weights[0:len(active_features)]
		proactive_feature_weights = weights[len(active_features):]


	elif regression_method == 'linear_tensorflow':

		with tf.Session() as sess:
			# Placeholders
			if 'regressor' in dynamic_params:
				clf = dynamic_params['regressor']
			else:
				clf = {}

			p_delta_feature_matrix = tf.placeholder(tf.float32, shape=[None, len(active_features)+len(proactive_features)+1])
			p_delta_accuracies = tf.placeholder(tf.float32, shape=[None])

			if 'weights' in clf:
				t_weights = tf.Variable( clf['weights'] )
			else:
				t_weights = tf.Variable( tf.truncated_normal( [len(active_features)+len(proactive_features)+1, 1], stddev=0.01, dtype=tf.float32 ) )

			# Define loss function
			diff = p_delta_accuracies - tf.matmul( p_delta_feature_matrix, t_weights )
			diff_sq = tf.square( diff )
			loss = tf.reduce_mean( diff_sq ) + tf.reduce_max( diff_sq ) * 0.01
			#loss = tf.reduce_mean( diff_sq )
			#loss = tf.reduce_max( diff_Sq )

			# Define train step
			train_step = tf.train.GradientDescentOptimizer(0.05).minimize( loss )

			# Append bias to the feature matrix
			n_epochs = 1000
			delta_feature_matrix_with_bias = np.ones( (delta_feature_matrix.shape[0], delta_feature_matrix.shape[1]+1) )
			delta_feature_matrix_with_bias[:,:-1] = delta_feature_matrix

			# Optimize
			init = tf.initialize_all_variables()
			sess.run( init )
			for i in range(n_epochs):
				sess.run( train_step, feed_dict={ p_delta_feature_matrix : delta_feature_matrix_with_bias, p_delta_accuracies : delta_accuracies } )

				if i % 200 == 0:
					print i, sess.run( loss, feed_dict={ p_delta_feature_matrix : delta_feature_matrix_with_bias, p_delta_accuracies : delta_accuracies } )

			# Prepare return variables
			weights = t_weights.eval()
			clf['weights'] = weights
			active_feature_weights = weights[0:len(active_features)]
			proactive_feature_weights = weights[len(active_features):len(active_features)+len(proactive_features)]


	elif regression_method == 'svr_kernel':
		clf = svm.SVR(kernel='rbf')
		clf.fit( delta_feature_matrix, delta_accuracies )

		# These are placeholder weights
		# The actual regressor is returned as clf
		weights = np.ones( len(active_features) + len(proactive_features) )
		active_feature_weights = np.ones( len(active_features) )
		proactive_feature_weights = np.ones( len(proactive_features) )

	elif regression_method in ['lr1', 'lr2']:
		penalty = 'l1' if regression_method == 'lr1' else 'l2'
		try:
			clf = linear_model.LogisticRegression(penalty=penalty)
			clf.fit( delta_feature_matrix, delta_accuracies )
		except:
			# LR fails when there is only one regression response. This is just a quick remedy
			clf = svm.SVR(kernel='rbf')
			clf.fit( delta_feature_matrix, delta_accuracies )			

		# These are placeholder weights
		# The actual regressor is returned as clf
		weights = np.ones( len(active_features) + len(proactive_features) )
		active_feature_weights = np.ones( len(active_features) )
		proactive_feature_weights = np.ones( len(proactive_features) )

	else:
		print 'please check'
		pass

	# For debugging purpose, save the delta feature matrix to see if we can actually fit it
	save_pickle(delta_feature_matrix, './temp_out_feature_matrix.pkl')
	save_pickle(delta_accuracies, './temp_out_accuracies.pkl')
	save_pickle(weights, './temp_out_weights.pkl')

	print
	print delta_feature_matrix[0:5,:]
	print delta_accuracies[0:5]
	print 'Lenth Extra Indices Pool: %d' % len(extra_indices_pool)
	print 'Sizes of cluster: %s, # clusters: %d, minimum cluster size: %d' % (str(sizes_of_clusters), len(sizes_of_clusters), min_cluster_size)
	print 'Weights: %s ' % weights
	if regression_method not in ['lr1', 'lr2', 'linear_tensorflow']:
		print 'R score: %.3f' % clf.score(delta_feature_matrix, delta_accuracies)
	return active_feature_weights, proactive_feature_weights, clf


##########################################################################################
#
#   Start and update conditions for performing dynamic proactive learning
#
##########################################################################################

def check_conditions(dataset, model, annotators, conditions=[], conditions_params=[]):
	# Sanity check: # conditions should equal # conditions_params
	assert( len(conditions) == len(conditions_params) )

	# Return True only if all the conditions are met.
	if conditions == []:
		return False

	for c in range(len(conditions)):
		condition = conditions[c]
		condition_params = conditions_params[c]

		if type(condition) == str:
			condition = eval(condition)

		flag = condition(dataset, model, annotators, condition_params)
		if flag == False:
			return False
	
	# Now all the conditions are met True.
	return True


##########################################################################################
#
#	Below are methods that decide whether to perform / stop dynamic proactive weight update.
#	- dpal_update_always
#   - dpal_update_num_labeled
#   - dpal_update_test_accuracy
#   - dpal_update_every_recurring
#   - dpal_stop_test_accuracy
#
##########################################################################################

def dpal_update_always(dataset, model, annotators, dpal_update_params={}):
	return True

def dpal_update_num_unlabeld(dataset, model, annotators, dpal_update_params={}):
	num_unlabeled_portion = dpal_update_params['num_unlabeled_portion'] if 'num_unlabeled_portion' in dpal_update_params else 0.1
	
	if len(dataset.train_labeled_indices) >= len(dataset.train_indices) * num_unlabeled_portion:
		return True
	else:
		return False

def dpal_update_test_accuracy(dataset, model, annotators, dpal_update_params={}):
	threshold_accuracy = dpal_update_params['threshold_accuracy'] if 'threshold_accuracy' in dpal_update_params else 0.55

	if model.test(dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices]) >= threshold_accuracy:
		return True
	else:
		return False

def dpal_update_every_recurring(dataset, model, annotators, dpal_update_params={}):
	return b_update_every_recurring(dataset, dpal_update_params)

def dpal_stop_test_accuracy(dataset, model, annotators, dpal_stop_params={}):
	# Stop if test accuracy is better than threshold accuracy
	return dpal_update_test_accuracy(dataset, model, annotators, dpal_stop_params)



##########################################################################################
#
#	Below are miscellaneous methods
#	- get_entropy
#   - get_indices_num_samples
#   - b_update_every_recurring
#
##########################################################################################

def get_entropy( class_posterior_prob ):
	# class_posterior_prob : [ |classes| x 1 ]
	class_posterior_prob[class_posterior_prob<0.001] = 0.001
	return - np.dot( class_posterior_prob, np.log( class_posterior_prob ) )


def get_indices_num_samples( dataset, get_utilities_of ):
	
	# Get number of samples: now n is the number of samples we are calculating utility values for
	if get_utilities_of == 'train_unlabeled':
		indices = dataset.train_unlabeled_indices
	elif get_utilities_of == 'test':
		indices = dataset.test_indices
	elif get_utilities_of == 'all':
		indices = np.arange( len(dataset.Z) )
	elif (type(get_utilities_of) == type({})) and ('custom' in get_utilities_of):
		indices = get_utilities_of['custom']
	else:
		print 'Warning: get_utilities_of not specified!'
		indices = dataset.train_indices
	num_samples = len(indices)

	return indices, num_samples

def b_update_every_recurring(dataset, params={}):
	"""
		Return a boolean value whether to update something or not, depending on
		how many labels were obtained compared to the last time this function was called.

		Need: params['n_update_every_recurring']
	"""
	n_update_every_recurring = params['n_update_every_recurring'] if 'n_update_every_recurring' in params else 50

	if 'last_updated_num_train_labeled' in params:
		# If there is already a record of dpal update,
		# check whether it has passed more than n_update_every_recurring since that point
		if len(dataset.train_labeled_indices) >= params['last_updated_num_train_labeled'] + n_update_every_recurring:
			params['last_updated_num_train_labeled'] = len(dataset.train_labeled_indices)
			return True
		else:
			return False
	else:
		# If there is no record, it is the first time that this function is called.
		params['last_updated_num_train_labeled'] = len(dataset.train_labeled_indices)
		print "********** Sanity check: this should be printed only once in each fold"
		return True

def get_utility_metrics( indices, dataset, model, annotators, active_features, active_features_params, proactive_features, proactive_features_params, result_format ):
	"""
		Given active_features and proactive_features,
		it computes (pro)active feature vectors of the input indices of the dataset

		result_format : {
			'log_weight'     - if True, return values in logarithmic 
			'avg_rows'       - if True, return utility metric averaged over rows (one value for each active learning criteria). Key values below get appended with "avg_"
			'proactive_best' - if True, include in dict_utility the values that have the highest "proactive_features[0]" among annotators 
			'best_annotators'- if 'proactive_best' is True, return (N x 1) np.array of annotator indices that have the highest "proactive_features[0]" for each instance	
			'proactive_avg'  - if True, include in dict_utility the average values among annotators
			'proactive_real' - if True, include in dict_utility as they were annotated
			'real_annotators'- if 'proactive_real' is True, return (N x 1) np.array of actual annotators indices 
			'proactive_all'  - if True, return (N x P x K) np.arrays
		}

		return: (dict_utility, active_features_params, proactive_features_params)
	"""

	# 0 - (1). Load paramter values
	avg_rows = result_format['avg_rows'] if ('avg_rows' in result_format) else False
	log_weight = result_format['log_weight'] if ('log_weight' in result_format) else False
	proactive_best = result_format['proactive_best'] if ('proactive_best' in result_format) else False
	proactive_avg = result_format['proactive_avg'] if ('proactive_avg' in result_format) else False
	proactive_real = result_format['proactive_real'] if ('proactive_real' in result_format) else False	
	proactive_all = result_format['proactive_all'] if ('proactive_all' in result_format) else False	

	# 0 - (2). Initialize variables
	dict_utility = {}
	num_indices = len(indices)
	num_active_features = len(active_features)
	num_proactive_features = len(proactive_features)
	num_cols = num_active_features + num_proactive_features
	num_annotators = len(annotators)


	# 1. Active Features: [ n x num_active_features ]
	active_feature_matrix = np.zeros(shape=[num_indices, num_active_features])

	for i in range( num_active_features ):
		# For each active feature
		active_feature = active_features[i]
		active_feature_params = active_features_params[i]
		active_feature_params['get_utilities_of'] = { 'custom' : indices }

		# Obtain feature value vector for each feature
		each_active_feature_value_vector_main, new_feature_params = active_feature( dataset, model, annotators, active_feature_params )

		# Update active_feature_matrix
		active_feature_matrix[:,i] = each_active_feature_value_vector_main

		#######################################################################
		### Debug if there's any insanity i.r.t updating new_feature_params ###
		#######################################################################	
		# Update active_feature_params in case we have an updated params
		active_features_params[i] = new_feature_params	


	# 2. Proactive Features: [ n x num_proactive_features x num_annotators ]
	if num_proactive_features > 0:
		proactive_feature_matrix = np.empty( (num_indices, num_proactive_features, num_annotators) )

		for j in range( num_proactive_features ):
			# For each active feature
			proactive_feature = proactive_features[j]
			proactive_feature_params = proactive_features_params[j]
			proactive_feature_params['get_utilities_of'] = { 'custom' : indices }

			# Obtain feature value matrix for each feature
			# each_proactive_feature_value_matrix_main: (N x |K|) np.arrays
			each_proactive_feature_value_matrix_main, new_feature_params = proactive_feature( dataset, model, annotators, proactive_feature_params )	

			# Update proactive_feature_matrix
			proactive_feature_matrix[:,j,:] = each_proactive_feature_value_matrix_main
			#for k in range( num_annotators ):
			#	proactive_feature_matrix[:,j,k] = each_proactive_feature_value_matrix_main[:,k]

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################	
			# Update proactive_feature_params in case we have an updated params
			proactive_features_params[j] = new_feature_params		


	# 3. Aggregate the results for each format we want
	if avg_rows == True:
		# Sum per column (criterion), and divide by the number of samples
		# avg_active_feature_vector: [ num_active_features, 1 ]
		avg_active_feature_vector = np.sum(active_feature_matrix, axis=0) / num_indices

		if num_proactive_features > 0:
			# If it has proactive features
			# avg_proactive_feature_matrix: [ num_proactive_features, num_annotators ]
			avg_proactive_feature_matrix = np.sum(proactive_feature_matrix, axis=0) / num_indices

			if proactive_best:
				# [ num_cols X 1 ]
				avg_rows_proactive_best = np.empty( num_cols )
				avg_rows_proactive_best[0:num_active_features] = avg_active_feature_vector
				avg_rows_proactive_best[num_active_features:] = np.amax(avg_proactive_feature_matrix, axis=1)
				dict_utility['avg_rows_proactive_best'] = avg_rows_proactive_best
				dict_utility['avg_rows_best_annotators'] = np.argmax(avg_proactive_feature_matrix[0,:])

			if proactive_avg:
				# [ num_cols X 1 ]
				avg_rows_proactive_avg = np.empty( num_cols )
				avg_rows_proactive_avg[0:num_active_features] = avg_active_feature_vector
				avg_rows_proactive_avg[num_active_features:] = np.sum(avg_proactive_feature_matrix, axis=1)	/ num_annotators
				dict_utility['avg_rows_proactive_avg'] = avg_rows_proactive_avg

			if proactive_real:
				# [ num_cols X 1 ]
				# Not implemented yet
				avg_rows_proactive_real = np.empty( num_cols )
				avg_rows_proactive_real[0:num_active_features] = avg_active_feature_vector
				avg_rows_proactive_real[num_active_features:] = np.amax(avg_proactive_feature_matrix, axis=1)
				dict_utility['avg_rows_proactive_real'] = avg_rows_proactive_real
				dict_utility['avg_rows_real_annotators'] = np.argmax(avg_proactive_feature_matrix[0,:])				
			
			if proactive_all:
				# [ num_cols X num_annotators ]
				avg_rows_proactive_all = np.zeros(shape=[num_cols, num_annotators])
				for k in range(num_annotators):
					avg_rows_proactive_all[0:num_active_features, k] = avg_active_feature_vector
				avg_rows_proactive_all[num_active_features:,:] = avg_proactive_feature_matrix
				dict_utility['avg_rows_proactive_all'] = avg_rows_proactive_all
		else:
			# [ num_active_features X 1 ]
			dict_utility['avg_rows_active'] = avg_active_feature_vector
	else:
		if num_proactive_features > 0:	
			# If it has proactive features
			if proactive_best:
				# [num_indices, num_cols]
				proactive_best = np.zeros(shape=[num_indices, num_cols])
				proactive_best[:,0:num_active_features] = active_feature_matrix
				proactive_best[:,num_active_features:] = np.amax(proactive_feature_matrix, axis=2)				
				dict_utility['proactive_best'] = proactive_best
				dict_utility['best_annotators'] = np.argmax(proactive_feature_matrix[:,0,:], axis=1)

			if proactive_avg:
				# [num_indices, num_cols]				
				proactive_avg = np.zeros(shape=[num_indices, num_cols])
				proactive_avg[:,0:num_active_features] = active_feature_matrix
				proactive_avg[:,num_active_features:] = np.sum(proactive_feature_matrix, axis=2) / num_annotators
				dict_utility['proactive_avg'] = proactive_avg
			
			if proactive_real:
				# [num_indices, num_cols]
				# To be implemented			
				proactive_real = np.zeros(shape=[num_indices, num_cols])
				proactive_real[:,0:num_active_features] = active_feature_matrix
				proactive_real[:,num_active_features:] = np.amax(proactive_feature_matrix, axis=2)								
				dict_utility['proactive_real'] = proactive_real
				dict_utility['real_annotators'] = np.argmax(proactive_feature_matrix[:,0,:], axis=1)				
			
			if proactive_all:
				# [num_indices, num_cols, num_annotators]				
				proactive_all = np.empty( (num_indices, num_cols, num_annotators) )
				for k in range(num_annotators):
					proactive_all[:,0:num_active_features, k] = active_feature_matrix
				proactive_all[:,num_active_features:,:] = proactive_feature_matrix
				dict_utility['proactive_all'] = proactive_all
		else:
			# [num_indices, num_active_features]							
			dict_utility['active'] = active_feature_matrix


	# 4. Return in log scale if requested
	if log_weight == True:
		for result_type in dict_utility.keys():
			dict_utility[result_type] = np.log( dict_utility[result_type] )

	return (dict_utility, active_features_params, proactive_features_params)
