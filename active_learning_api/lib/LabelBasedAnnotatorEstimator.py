__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Parent class of <AnnotatorEstimator>, which make estimation of annotators
		based on the query history
"""

import numpy as np
from NoisedGroundTruthAnnotator import NoisedGroundTruthAnnotator
from GroundTruthAnnotator import GroundTruthAnnotator
from AnnotatorEstimator import AnnotatorEstimator
import Simulator
import utility_functions
from utils import print_errors

class LabelBasedAnnotatorEstimator(AnnotatorEstimator):

	def __init__(self):
		AnnotatorEstimator.__init__(self)
		pass

	def estimate(self, dataset, annotators, params={}):
		"""
			Return type: [ # Samples X # Annotators] np.array where each element \in [0, 1]
		"""

		# This is what we will return.
		num_annotators = len(annotators)
		p_answers = np.empty( ( dataset.X.shape[0], num_annotators ) )

		# Unload parameters input
		model_type  = params['model_type']
		model_params = params['model_params']

		for k in range(num_annotators):
			# Obtain annotator and its label history
			annotator = annotators[k]

			if annotator.labeled_indices.shape[0] == 0:
				# "First estimation of annotator %s" % annotator.name
				p_answers[:,k] = np.ones( dataset.X.shape[0] )
			else:
				annotated_X = dataset.X[annotator.labeled_indices]
				annotated_Y = annotator.labeled_samples

				try:
					# Build and train a model for this annotator
					annotator_model = Simulator.load_model(model_type, model_params)
					annotator_model.train( annotated_X, annotated_Y )

					# Record this annotator's prediction
					entropy_values = np.ones( dataset.X.shape[0] )
					probs = annotator_model.predict_proba( dataset.X )
					for i in range(entropy_values.shape[0]):
						entropy_values[i] = utility_functions.get_entropy( probs[i,:] )

					# Entropy ranges from 0 (when prob = [1,0,0,...], to -|C| * (1/|C|) * log(1/|C|))
					max_entropy = - np.log( 1/float(len(dataset.categories)) )
					normalized_entropy = entropy_values / max_entropy

					# Estimate this annotator's performance via reverse normalized entropy
					p_answers[:,k] = 1 - normalized_entropy
				except:
					# "Classes seen so far: %s for annotator name %s" % ( np.unique( annotated_Y ), annotator.name )
					p_answers[:,k] = np.ones( dataset.X.shape[0] )

		# Make sure they range from 0 to 1
		p_answers[p_answers > 1] = 1
		p_answers[p_answers < 0] = 0

		return p_answers