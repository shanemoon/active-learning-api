__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Build a simulated <Annotator> object.
"""

class AnnotatorBuilder:
	def __init__(self):
		pass

	def build_experts_from_dictionary(self, d_annotators, dataset):
		"""
			'd_annotators' : a dictionary for ( 'auto'   : ( annotators_type, num_annotators, auto_params ), 
				                                'manual' : { 'name' : [ annotator_id, cost, type, manual_params ] } )
			where 'params' : a dictionary that stores specific information
		"""		
		annotators = []

		if 'manual' in d_annotators:
			# Manual addition mode
			for key, value in d_annotators['manual']:
				name = key
				cost = value[0]
				annotator_type = value[1]
				annotator_params = value[2]
				
				# Create a new <Annotator> object
				annotator = self.build( annotator_type, name, cost, annotator_params )
				annotators.append( annotator )
		
		elif 'auto' in d_annotators:
			# Automatic creation with just a few parameters
			annotators_type, num_annotators, auto_params = d_annotators['auto']

			if annotators_type == 'build_k_domain_experts':
				# Return a <List> of |K| <Annotator>s, each having its unique expertise area.
				annotator_type = 1 # Noised Ground Truth Annotator
				noise_rate = auto_params['noise_rate']
				annotators = self.build_k_domain_experts( annotator_type, num_annotators, noise_rate, dataset )
			
			elif annotators_type == 'build_k_human_annotators':
				annotators = self.build_k_human_annotators( num_annotators )
			else:
				# If the annotator type is not recognizable, return a GTA.
				print 'Annotators Type is not recognizable'
				from GroundTruthAnnotator import GroundTruthAnnotator				
				annotators.append( GroundTruthAnnotator( name="Ground Truth Annotator", annotator_id=-1, cost=1, params={ 'Z' : dataset.Z} ) )
		else:
			# The dictionary is probably empty, return a GTA.
			from GroundTruthAnnotator import GroundTruthAnnotator				
			annotators.append( GroundTruthAnnotator( name="Ground Truth Annotator", annotator_id=-1, cost=1, params={ 'Z' : dataset.Z} ) )
		return annotators


	def build(self, annotator_type, name, annotator_id, cost, annotator_params):
		"""
			Return a new <Annotator> object 
		"""

		if annotator_type == 0:
			# GroundTruth Annotator
			from GroundTruthAnnotator import GroundTruthAnnotator
			return GroundTruthAnnotator(name, annotator_id, cost, annotator_params)

		elif annotator_type == 1:
			# Noised Ground Truth Annotator
			from NoisedGroundTruthAnnotator import NoisedGroundTruthAnnotator
			return NoisedGroundTruthAnnotator(name, annotator_id, cost, annotator_params)
		
		return


	def build_k_domain_experts( self, annotator_type, num_annotators, noise_rate, dataset ):
		"""
			Return a <List> of |K| <Annotator>s, each having its unique expertise area.
			For example, for a <Datset> with |C| categories,
			each annotator has expertise in |C| / |K| categories.

			In their expertise area, they have ground-truth performance.
			In their non-expertise area, they perform per their given noise_rate.
		"""
		from NoisedGroundTruthAnnotator import NoisedGroundTruthAnnotator
		annotators = []
		categories = dataset.categories
		num_categories = len(categories)
		num_cover_categories = num_categories / num_annotators
		cost = 1.0
		
		for i in range(num_annotators):
			expertise_area = range(i * num_cover_categories, (i+1) * num_cover_categories)
			name = 'Noised Ground Truth Annotator with expertise %s' % str(expertise_area)			
			annotator_id = i
			annotator_params = {}
			annotator_params['Z'] = dataset.Z		
			annotator_params['noise_type']  = 1	
			annotator_params['noise_level'] = {}

			for category in categories:
				if category in expertise_area:
					annotator_params['noise_level'][category] = 0.0
				else:
					annotator_params['noise_level'][category] = noise_rate

			annotator = NoisedGroundTruthAnnotator(name, annotator_id, cost, annotator_params)
			annotators.append( annotator )

		return annotators


	def build_k_human_annotators( self, num_annotators ):
		from HumanAnnotator import HumanAnnotator
		annotators = []

		for i in range(num_annotators):
			annotators.append( HumanAnnotator(annotator_id=i) )
		return annotators

