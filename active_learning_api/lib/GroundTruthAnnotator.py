__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		An Annotator who always gives a ground-truth annotator.
"""

from BaseAnnotator import BaseAnnotator

class GroundTruthAnnotator(BaseAnnotator):

	def __init__(self, name="Ground Truth Annotator", annotator_id=-1, cost=1, params={}):
		# Ground Truth Annotator's id is fixed to be -1.
		BaseAnnotator.__init__(self, name, -1, cost, params)
		self.Z = params['Z']


	def __str__(self):
		return "%s with params: %s" % (self.name, str(self.params))


	def answer_queries(self, dataset, indices_to_query, info={}):
		"""
			Return the annotation of the queries
		"""
		Y_answered = self.Z[ indices_to_query ]
		return Y_answered