__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Parent class of <AnnotatorEstimator>, which make perfect estimation of
		annotators if possible, e.g. for <GroundTruthAnnotator> or <NoisedGroundTruthAnnotator>
"""

import numpy as np
from NoisedGroundTruthAnnotator import NoisedGroundTruthAnnotator
from GroundTruthAnnotator import GroundTruthAnnotator
from AnnotatorEstimator import AnnotatorEstimator

class TrueAnnotatorEstimator(AnnotatorEstimator):

	def __init__(self):
		AnnotatorEstimator.__init__(self)
		pass

	def estimate(self, dataset, annotators, params={}):
		"""
			Return the 
			Return type: [ # Samples X # Annotators] np.array where each element \in [0, 1]
		"""

		# This is what we will return.
		num_annotators = len(annotators)
		p_answers = np.empty( ( dataset.X.shape[0], num_annotators ) )

		# Perfect estimation; just simply load annotators' known noise rate
		# It can handle <GroundTruthAnnotators> instances or <NoisedGroundTruthAnnotator>s.
		
		for k in range(num_annotators):
			# For each annotator, we calculate p_answer for every sample in this dataset
			annotator = annotators[k]
			p_answer = np.ones( dataset.X.shape[0] )

			if isinstance( annotator, NoisedGroundTruthAnnotator ):
				noise_level = annotator.noise_level
				noise_type  = annotator.noise_type

				if noise_type == 0:
					# This has a uniform average noise rate across the entire dataset
					# `noise_level' is a scalar value.
					p_answer = p_answer * (1 - noise_level)

				else:
					# This has a class conditional scalar average for each category
					# `noise_level' is a dictionary of category -> scalar noise rate
					for category in dataset.categories:
						try:
							noise_rate_per_category = noise_level[category]
						except:
							print "Cannot find noise_rate for this category %s" % str(category)
							noise_rate_per_category = 1.0 # (doesn't know anything.)
						
						category_indices = np.where( dataset.Z == category )

						# P_answer for this category is the same as (1 - noise_rate_per_category)
						p_answer[ category_indices ] = 1 - noise_rate_per_category
			
			elif isinstance( annotator, GroundTruthAnnotator ):
				p_answer[:] = 1.0

			else:
				# We know nothing
				p_answer[:] = 0.0

			# Append p_answer for each annotator to p_answers
			p_answers[:,k] = p_answer

		return p_answers

