__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Base class of which an instance makes estimate of
		labelers' expertise
"""

import numpy as np

class AnnotatorEstimator:

	def __init__(self):
		pass

	def estimate(self, dataset, annotators, params={}):
		"""
			Return the 
			Return type: [ # Samples X # Annotators] np.array where each element \in [0, 1]
		"""

		num_annotators = len(annotators)
		p_answers = np.empty( ( dataset.X.shape[0], num_annotators ) )
		return p_answers

	def estimate_of(self, indices, dataset, annotators, params={}):
		p_answers = self.estimate(indices, dataset, annotators, params)
		return p_answers[ indices, : ]
