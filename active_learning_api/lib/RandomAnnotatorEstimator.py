__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Parent class of <AnnotatorEstimator>, which make random estimation
"""

import numpy as np
from AnnotatorEstimator import AnnotatorEstimator

class RandomAnnotatorEstimator(AnnotatorEstimator):

	def __init__(self):
		AnnotatorEstimator.__init__(self)
		pass

	def estimate(self, dataset, annotators, params={}):
		"""
			Return the 
			Return type: [ # Samples X # Annotators] np.array where each element \in [0, 1]
		"""

		num_annotators = len(annotators)
		p_answers = np.random.rand ( dataset.X.shape[0], num_annotators )
		return p_answers