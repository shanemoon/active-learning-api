__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		A child class for <BaseLearner>.

		The <MaxUtilityLearner> performs active learning, of which the 
		selection criteria is based on some utility function.

		Therefore, the problem is casted as choosing the instances
		that maximizes the utility at each iteration.

		Utility function can be any of the following, for example:
		- Uncetainty-based Sampling
			- Entropy-based
		- Density Weighted Uncertainty Sampling (DWUS)
		- ...
"""

from BaseAnnotator import BaseAnnotator
from BaseLearner import BaseLearner
import numpy as np
import utility_functions

class MaxUtilityLearner(BaseLearner):

	def __init__(self, name="Max Utility Learner", params={}):
		BaseLearner.__init__(self, name, params)
		
		# utility_function is a function that takes a <Dataset> as an input,
		# and returns an array of utility values for each instance.
		try:
			utility_params = params['utility_params']
		except:
			# If the utility function was not provided:
			print "The utility function is not provided; choose queries randomly"
			utility_params = { 'unlabeled_only' : True, 'active_features' : [ utility_functions.random_value ] }
		
		self.utility_params = utility_params

	def __str__(self):
		return "%s with params: %s" % (self.name, str(self.params))


	def choose_queries(self, dataset, model, annotators, n_queries=1, info={}):
		"""
			Return indices of the dataset that need to be 
			queried to annotators.
		"""

		# utility_tuples := [ (utility_value, index, annotator_index), ... ] 
		utility_tuples, utility_values, utility_indices, utility_annotators, utility_params = utility_functions.get_utility( dataset, model, annotators, self.utility_params )

		# Update the parameters
		self.utility_params = utility_params

		utility_sample = self.utility_params['utility_sample'] if ('utility_sample' in self.utility_params) else 0
		best_annotater_per_sample = self.utility_params['best_annotater_per_sample'] if ('best_annotater_per_sample' in self.utility_params) else True

		# Leave only the utility tuples for each sample that have the highest utility among those annotated by
		# multiple annotators
		if best_annotater_per_sample == True:
			# To be implemented
			pass


		if utility_sample == 0:
			# Sort the utility tuples by their utility value, and select the best `n_queries' candidates
			utility_tuples.sort(reverse=True)
			best_query_tuples = utility_tuples[:n_queries]
		else:
			# Weighted sample of tuples by their normalized utility value
			sampleweights = np.array([i[0] for i in utility_tuples])

			# set all zero weighted elements to the minimum of the min value and some epsilon
			sampleweights[sampleweights<0] = 0
			normsum = sum(sampleweights)
			if normsum==0:
				inds = np.random.choice(len(utility_tuples), n_queries, replace=False)
			else:
				mval = min(sampleweights[sampleweights>0].min(), 0.01)
				sampleweights[sampleweights==0] = mval
				normsum = sum(sampleweights)
				inds = np.random.choice(len(utility_tuples), n_queries, replace=False, p = sampleweights/normsum)

			best_query_tuples = list( utility_tuples[i] for i in inds )

		# Organize by { annotator_index :  }
		annotator_index_to_indices_to_query = {}
		
		for query_tuple in best_query_tuples:
			utility_value, index, annotator_index = query_tuple
			if annotator_index not in annotator_index_to_indices_to_query:
				# Create a new indices_to_query array if this annotator has not been seen before
				# note: ndmin=1 ensures this can be treated as a vector when only length 1, reduces checking elsewhere
				annotator_index_to_indices_to_query[annotator_index] = np.array( index, ndmin=1 )
			else:
				# Append the index of the data to the indices_to_query for each annotator
				indices_to_query = annotator_index_to_indices_to_query[annotator_index]
				annotator_index_to_indices_to_query[annotator_index] = \
				np.append( indices_to_query, index )
			
		# Return : a dictionary { annotator_index : [index_to_query, ...], ... }
		return annotator_index_to_indices_to_query
