__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Parent class of <AnnotatorEstimator>, which make gaussian noised estimation of
		annotators if possible, e.g. for <GroundTruthAnnotator> or <NoisedGroundTruthAnnotator>
"""

import numpy as np
from NoisedGroundTruthAnnotator import NoisedGroundTruthAnnotator
from GroundTruthAnnotator import GroundTruthAnnotator
from AnnotatorEstimator import AnnotatorEstimator
from TrueAnnotatorEstimator import TrueAnnotatorEstimator

class NoisedAnnotatorEstimator(AnnotatorEstimator):

	def __init__(self):
		AnnotatorEstimator.__init__(self)
		pass

	def estimate(self, dataset, annotators, params={}):
		"""
			Return the 
			Return type: [ # Samples X # Annotators] np.array where each element \in [0, 1]
		"""

		# This is what we will return.
		num_annotators = len(annotators)
		p_answers = np.empty( ( dataset.X.shape[0], num_annotators ) )

		# First get a perfect estimate of annotators
		p_answers = TrueAnnotatorEstimator().estimate( dataset, annotators, params )

		# Obtain sigma, and artifically add Gaussian noise to it
		sigma = params['p_ans_sigma'] if 'p_ans_sigma' in params else 0.15
		p_answers += np.random.normal( loc=0, scale=sigma, size=p_answers.shape )

		# Make sure they range from 0 to 1
		p_answers[p_answers > 1] = 1
		p_answers[p_answers < 0] = 0

		return p_answers