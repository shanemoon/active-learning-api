__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		A child class for <BaseModel>, which can predict, be trained, etc.
		Specifically, this class uses the python module `scikit-learn`
		to build a classifier.

		The specific name of the classifier can be passed in params as:
		params['clf_type'] = (e.g.) 'LR'
"""

from BaseModel import BaseModel
import numpy as np

class ScikitModel(BaseModel):

	def __init__(self, model_name='Scikit Learner', params={}):
		BaseModel.__init__(self, model_name, params)
		self.clf_type = params['clf_type'] if 'clf_type' in params else 'LR'
		self.clf_params = params['clf_params'] if 'clf_params' in params else {}

		self.initialize_classifier()

	def initialize_classifier(self):

		if self.clf_type == 'LR':
			from sklearn.linear_model import LogisticRegression
			clf = LogisticRegression()
		
		elif self.clf_type == 'NB':
			from sklearn.naive_bayes import MultinomialNB
			clf = MultinomialNB(alpha=.01)
		
		elif self.clf_type == 'SVC':
			from sklearn.svm import SVC
			clf = SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, shrinking=True, 
				probability=True, tol=0.001, cache_size=200, class_weight=None, verbose=False, 
				max_iter=-1, random_state=None)
		
		elif self.clf_type == 'GNB':
			from sklearn.naive_bayes import GaussianNB
			clf = GaussianNB()
		
		elif self.clf_type == 'GMM':
			from sklearn.mixture import GMM
			clf = GMM(n_components=1, covariance_type='diag', random_state=None, thresh=None, 
				tol=0.001, min_covar=0.001, n_iter=100, n_init=1, params='wmc', init_params='wmc')
		
		elif self.clf_type == 'SGD':
			from sklearn.linear_model import SGDClassifier
			# Unpack clf_params
			loss = self.clf_params['loss'] if 'loss' in self.clf_params else 'log' # 'hinge', 'huber', etc.

			clf = SGDClassifier(loss=loss, penalty='l2', alpha=0.0001, l1_ratio=0.15, 
				fit_intercept=True, n_iter=5, shuffle=True, verbose=0, epsilon=0.1, n_jobs=1, 
				random_state=None, learning_rate='optimal', eta0=0.0, power_t=0.5, class_weight=None, 
				warm_start=False, average=False)
			#self.can_learn_incrementally = True

		else:
			print 'Cannot recognize the classifier type %s' % self.clf_type

		self.clf = clf

	def predict_proba(self, X):
		if not self.trained:
			print "Warning: the Model has not been trained yet."

		if self.clf_type == 'SGD':
			# There is some weird behavior with SGD predict_proba
			predicted_prob = self.clf.predict_proba(X)
			num_classes = predicted_prob.shape[1]
			unknown_prob = 1.0 / num_classes
			predicted_prob[ np.isnan( predicted_prob ) ] = unknown_prob
			#predicted_prob[ np.sum( predicted_prob, axis=1 ) < 0.9] = np.ones(num_classes) * unknown_prob
			#predicted_prob[ np.sum( predicted_prob, axis=1 ) > 1.1] = np.ones(num_classes) * unknown_prob
			return predicted_prob

		else:
			return self.clf.predict_proba(X)

	def predict(self, X):
		if not self.trained:
			print "Warning: the Model has not been trained yet."		
		return self.clf.predict(X)

	def train(self, X, Y, annotators=[]):
		self.clf.fit(X, Y)
		self.trained = True

	def train_incremental(self, X, Y):		
		if not self.trained:
			print "Warning: the Model has not been trained yet."
			print "Avoiding partial_fit(), using fit() instead ..."
			self.train(X, Y)
		else:
			self.clf.partial_fit(X, Y)
