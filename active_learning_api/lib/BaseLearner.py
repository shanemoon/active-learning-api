__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		A base class for Active Learners.
		Each Active Learner defines its own method for 
		choosing queries given a dataset.
"""

from BaseAnnotator import BaseAnnotator

class BaseLearner:
	
	def __init__(self, name="BaseLearner", params={}):
		self.name = name
		self.params = params


	def __str__(self):
		return "%s with params: %s" % (self.name, str(self.params))


	def choose_queries(self, dataset, model, annotators, n_queries=1, info={}):
		"""
			Return indices of the dataset that need to be 
			queried to annotators.
		"""
		# Return : a dictionary { annotator_index : [index_to_query, ...], ... }
		return { 0 : [] }