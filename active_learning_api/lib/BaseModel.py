__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		A base wrapper class for a classifier
		which can predict, be trained, etc.
"""

from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
import utility_functions

class BaseModel:
	def __init__(self, model_name='Base Model', params={}):
		self.model_name = model_name
		self.params = params
		self.can_learn_incrementally = False
		self.trained = False

	def __str__(self):
		return "<%s>" % self.model_name

	def predict(self, X):
		return

	def predict_proba(self, X):
		# X : array-like, shape = [n_samples, n_features]
		# T : array-like, shape = [n_samples, n_classes]
		return

	def train(self, X, Y, annotators=[]):
		self.trained = True
		return

	def train_incremental(self, X, Y):
		self.trained = True
		return

	def test(self, X_test, Z_test):
		Y_pred = self.predict( X_test )
		accuracy = accuracy_score( Z_test, Y_pred )
		return accuracy

	def measure_avg_entropy(self, X_test):
		# X : array-like, shape = [n_samples, n_features]
		# T : scalar value
		proba = self.predict_proba( X_test )
		total_entropy = 0.0
		num_samples = X_test.shape[0]
		for i in range(num_samples):
			total_entropy += utility_functions.get_entropy(proba[i,:])
		return total_entropy / num_samples

	def compare_prediction_results(self, X_test, Z_test):
		print 'Y_pred: %s' % self.predict( X_test )[:100]
		print 'Z_test: %s' % Z_test[:100]
