__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Simulate the performance of an active learning algorithm.
		Experiment presets are loaded in `exp_params` passed to this
		<Simulator>
"""

from ALWorker import ALWorker
from ProgressBar import AnimatedProgressBar
from Drawer import Drawer
from AnnotatorBuilder import AnnotatorBuilder
import os
import utility_functions
from utils import save_pickle, load_pickle
import numpy as np
import copy

class Simulator:
	
	def __init__(self):	
		pass


	def run(self, exp_params):
		"""
			Run an active learning simulation given an experimental setup.
			Ultimately, this function returns a prediction report for this experiment

			- exp_params: a dictionary that contains the setup, keys of which are as follows:

				'learner_type'    : type of <Learner> that defines the AL algorithm
				                    0 - <RandomLearner>
				                    1 - <MaxUtilityLearner>
				'model_type'      : type of <Model> that defines the classifier
				                    0 - <ScikitModel>
				'model_params'    : `params` passed to a <Model> instance.
				                    'clf_type' : (e.g.) 'LR', 'SVM', etc.				                    
				'd_annotators'    : a dictionary for ( 'auto'   : { type, num_annotators }, 
					                                   'manual' : { 'name' : [ annotator_id, cost, type, params ] } )
				'dataset_name'    : name of the dataset
				'budget'          : total budget assigned for a single fold task
				'percent_labeled' : percentage of initially labeled instances
				'n_queries'       : # of queries that will be made at each iteration
			    'n_folds'         : # of folds that will be performed for cross validation
		"""
		# 0. Load the input parameters
		path_dataset     = exp_params['path_dataset']
		learner_type     = exp_params['learner_type']
		learner_params   = exp_params['learner_params']
		model_type       = exp_params['model_type']
		model_params     = exp_params['model_params']
		d_annotators     = exp_params['d_annotators']
		dataset_name     = exp_params['dataset_name']
		budget           = exp_params['budget']
		percent_labeled  = exp_params['percent_labeled']
		n_queries        = exp_params['n_queries']
		n_folds          = exp_params['n_folds']
		return_after_exp = exp_params['return_after_exp']
		downsize         = exp_params['downsize']
		n_repeat_iterations = exp_params['n_repeat_iterations'] if 'n_repeat_iterations' in exp_params else 1

		# Back up some of the exp_params that might change every fold
		learner_params_copy = copy.deepcopy( learner_params )

		# Prepare the experimental results format
		exp_results = {}
		if 'fold_error_rates' in return_after_exp:
			exp_results['fold_error_rates'] = []
		if 'fold_costs' in return_after_exp:
			exp_results['fold_costs'] = []			
		if 'workers' in return_after_exp:
			exp_results['workers'] = []			

	
		for i_repeat_iter in range(n_repeat_iterations):
			print 'Starting Repeat Iteration %d (out of %d) ...' % (i_repeat_iter, n_repeat_iterations)
			
			for i_fold in range(n_folds):
				
				if i_fold >= 1:
					# For fast debugging, break after the first break
					# comment this out for actual experiments.
					#break
					pass
				
				print 'Starting Fold %d (out of %d) ...' % (i_fold, n_folds)

				# Initialize a learner. A learner decides how to perform active learning
				# (e.g.) what to query, which annotators to choose, etc.
				learner = load_learner( learner_type, learner_params )

				# Create a new Dataset initialization for this fold
				dataset = load_dataset( path_dataset, n_folds, i_fold, dataset_name, downsize, rand_seed=i_repeat_iter )

				# Initialize a model for this fold
				model = load_model( model_type, model_params )

				# Get annotators
				annotators = load_annotators( d_annotators, dataset )

				# Create an Active Learning Worker
				worker = ALWorker( learner, dataset, model, budget, annotators=annotators )

				# Randomly assign ground truth samples to start active learning with
				worker.initialize_rand_ground_truth( percent_labeled=percent_labeled, rand_seed=0 )

				# Check the initial performance.
				# Initialize `error_rates` and `cumulative_costs` which track the performance
				# at each iteration.
				accuracy =  worker.test()
				error_rates = [ 1 - accuracy ]
				cumulative_costs = [ 0 ]

				# Repeat quering until the budget runs out.
				# Start an animated progress bar.
				p = AnimatedProgressBar(end=budget, width=100)
				i = 0

				TERMINATE = False
				while (worker.budget > 0) and (TERMINATE == False):

					# Choose the best queries to make
					annotator_index_to_indices_to_query = worker.choose_queries( n_queries=n_queries )

					# For each annotator, make queries
					for annotator_index, indices_to_query in annotator_index_to_indices_to_query.items():
						annotator = annotators[annotator_index]
					
						# Get annotations for these queries by the chosen annotator
						(new_Y, response_type) = worker.ask( annotator, indices_to_query )

						# response_type 1: normal query, received an answer from the annotator
						if response_type == 1:
							# Update the model and the dataset record
							worker.update( indices_to_query, new_Y, annotator )

							# Get the accuracy at this iteration on the held-out test dataset.
							accuracy =  worker.test()
							error_rates.append( 1 - accuracy )

							# Update the cumulative cost so far
							cumulative_costs.append( budget - worker.budget )

							# Update the progress bar
							p.set_absolute_progress_and_iteration(budget - worker.budget, i)
							p.show_progress()

						elif response_type == 2:
							# We ran out of budget, thus terminate the process
							TERMINATE = True
						else:
							# Optionally, it could be because the annotator
							# simply refused to answer, due to insufficient confidence level, etc.
							# To accommodate this, we can have different return-types for worker.ask()
							#####################################
							######    To be implemented    ######
							#####################################
							pass
					i += 1

				# We are done with the loop
				print "End of the active learning process: Final error rate: %.3f" % (1 - accuracy)

				# Optionally printout the query history
				#worker.print_query_history()

				# Optionally clear annotation at every fold.
				#dataset.clear_annotations()

				# Append the error_rates & costs result to fold_error_rates & fold_costs
				if 'fold_error_rates' in return_after_exp:
					exp_results['fold_error_rates'].append( error_rates )
				if 'fold_costs' in return_after_exp:
					exp_results['fold_costs'].append( cumulative_costs )
				if 'workers' in return_after_exp:
					exp_results['workers'].append( worker )

				# Reset some of the parameters that have been changed
				learner_params = copy.deepcopy(learner_params_copy)

				print [len(np.where(dataset.Z[dataset.train_labeled_indices] == category)[0]) for category in dataset.categories]

		return exp_results


def load_learner( learner_type, learner_params ):
	"""
		'learner_type' : type of <Learner> that defines the AL algorithm
		               0 - <RandomLearner>
		               1 - <MaxUtilityLearner>	
	"""
	if learner_type == 0:
		from RandomLearner import RandomLearner
		return RandomLearner( params=learner_params )

	elif learner_type == 1:
		from MaxUtilityLearner import MaxUtilityLearner
		return MaxUtilityLearner( params=learner_params )


def load_model( model_type, model_params ):
	"""
		'model_type' : type of <Model> that defines the classifier
		               0 - <ScikitModel>	
	"""
	if model_type == 0:
		from ScikitModel import ScikitModel
		return ScikitModel( params=model_params )


def load_dataset( path_dataset, n_folds, i_fold, dataset_name, downsize=-1, rand_seed=0 ):
	from Dataset import Dataset
	dataset = Dataset(filename=path_dataset, n_folds=n_folds, dataset_name=dataset_name, downsize=downsize)
	dataset.initialize_fold( n_folds, i_fold, rand_seed=rand_seed )
	return dataset

def load_annotators( d_annotators, dataset ):
	"""
		'd_annotators' : a dictionary for ( 'auto'   : { type, num_annotators }, 
			                                'manual' : { 'name' : [ annotator_id,, cost, type, params ] } )
		where 'params' : a dictionary that stores specific information
	"""
	return AnnotatorBuilder().build_experts_from_dictionary( d_annotators, dataset )


def generate_exp_params(path_dataset, downsize,
	                    learner_type, learner_params, model_type, model_params, d_annotators,
	                    dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp ):	
	exp_params = {}

	# Set Parameters
	exp_params['path_dataset']        = path_dataset
	exp_params['downsize']            = downsize
	exp_params['learner_type']        = learner_type
	exp_params['learner_params']      = learner_params
	exp_params['model_type']          = model_type
	exp_params['model_params']        = model_params
	exp_params['d_annotators']        = d_annotators
	exp_params['dataset_name']        = dataset_name
	exp_params['budget']              = budget
	exp_params['percent_labeled']     = percent_labeled
	exp_params['n_queries']           = n_queries
	exp_params['n_folds']             = n_folds
	exp_params['n_repeat_iterations'] = n_repeat_iterations
	exp_params['return_after_exp']    = return_after_exp


	return exp_params


def make_folders():
    path = os.getcwd()
    path_split = os.path.split(path)
    bottom_two_levels = os.path.join(os.path.split(path_split[0])[1],
                                     path_split[1])
    # the current working directory better be here or else the results folder
    #   goes somewhere strange
    print bottom_two_levels
    assert(bottom_two_levels == 'active-learning-api/active_learning_api')
    os.system('mkdir ./../results/')
    os.system('mkdir ./../results/graphs')


if __name__ == '__main__':
	"""
		Description : Batch run the experiments.
		              Right now it is a bunch of copies and pastes.
		              Ideally, it needs some automation mechanism, which I won't worry about now.

		< Parameters > 
		'learner_type'    : type of <Learner> that defines the AL algorithm
		                    0 - <RandomLearner>
		                    1 - <MaxUtilityLearner>
		'learner_params'  : `params` passed to a <Learner> instance.
		                    'utility_params' : {
		                        'unlabeld_only' : (Boolean) to indicate if we are only considering utilities of unlabeled examples
		                        'active_features' : <List> of handles to feature methods (defined below)
		                        'active_features_params' : <List> of feature params in the same order as in 'active_features' {
		                            'density_est_type' : for utility functions that require density estimation
		                              0 - Kernel Density Estimation
		                              1 - K-means Reverse Count
		                        }
		                        'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
		                        'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
		                        'dynamic_params' : {
		                            # Params related to dynamic proactive learning (DPAL)
		                            'weight_update_method' : e.g. utility_functions.dpal_update
		                            'main_percentage' : percentage of # samples for the "main' model
		                            'M' : # of clusters for "extra" models
		                            'extra_model_group_method'
		                              0 - K-means clustering
		                              1 - K-means clustering + 10-NN of centroids 
		                            'extra_model_group_space'
		                              0 - Original concept space
		                              1 - Utility metrics space
		                        }
		                        'utility_sample' (for <MaxUtilityLearner>)
		                          0 - Sort the utility tuples by their utility value, and select the best `n_queries' candidates
		                          1 - Weighted sample of tuples by their normalized utility value
		                        'annotator_selection' (for <MaxUtilityLearner>)
		                          0 - Choose an annotator for each instance with the best proactive_features[0] value
		                          1 - Choose an annotator for each instance with the best overall value
		                          2 - Allow duplicate annotators
		                    }
		'model_type'      : type of <Model> that defines the classifier
		                    0 - <ScikitModel>
		'model_params'    : `params` passed to a <Model> instance.
		                    'clf_type' : (e.g.) 'LR', 'SVM', 'NB' etc.
		'd_annotators'    : a dictionary for ('name' : [ cost, type, params ] )
		'dataset_name'    : name of the dataset
		'budget'          : total budget assigned for a single fold task
		'percent_labeled' : percentage of initially labeled instances
		'n_queries'       : # of queries that will be made at each iteration
	    'n_folds'         : # of folds that will be performed for cross validation
	"""

	"""
		'active_features' : [ utility_functions.entropy, utility_functions.unknownness, utility_functions.density ],
		'active_features_params' : [ {'normalize':True}, {'unknownness_est_type':0, 'normalize':True} , {'density_est_type' : 0, 'normalize' : True } ],	
	"""

	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'unlabeld_only' : True,
		'active_features' : [ utility_functions.entropy, utility_functions.unknownness, utility_functions.density ],
		'active_features_params' : [ {'normalize':True}, {'unknownness_est_type':0, 'normalize':True} , {'density_est_type' : 0, 'normalize' : True } ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {
			'main_percentage' : 0.5,
			'regression_method' : 'linear_tensorflow',
			'extra_model_group_space' : 0,
			'extra_model_group_method' : 2,
			'update_conditions_params' : [{'n_update_every_recurring':30}],
			'M':30,
			'G':1,
		}, 
		'utility_sample' : 0
	 }
	}
	model_type          = 0
	model_params        = { 'clf_type' : 'LR' }
	#d_annotators        = {}
	d_annotators        = { 'auto' : ('build_k_domain_experts', 1, {'noise_rate' : 0.0}) }
	dataset_name        = 'alldata_nz2_pca'
	downsize            = -1
	budget              = 300
	percent_labeled     = 0.002
	n_queries           = 1
	n_folds             = 5
	n_repeat_iterations = 1
	return_after_exp    = { 'fold_error_rates', 'fold_costs' }

	exp_params = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )


	# Make sure that we have all the necessary folders
	#make_folders()
	exp_results = Simulator().run( exp_params )
	fold_error_rates = exp_results['fold_error_rates']
	fold_costs = exp_results['fold_costs']
	
	# Draw the graph
	output_filename = './../results/graphs/alldata_nz2_pca_dpal_unk0.eps'
	Drawer().draw(output_filename, {
		'test' : {'X':fold_costs, 'Y':fold_error_rates}
		}, 'Cost', 'Error Rate', '', False)




