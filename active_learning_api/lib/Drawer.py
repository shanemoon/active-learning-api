__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
"""

from pylab import *

class Drawer:
	def __init__(self):
		pass

	def draw(self, filename, d_graphs, x_label, y_label, plot_title, scale_graph):
		"""
			d_graphs: {'legend_name' : { 'X': [ [ X_round_i ], [ X_round_i+1 ], ... ] ]  , 'Y': [ [Y_i], [Y_i+1], ... ],
			           'legend_order' : ['legend_name_1', ..., ]  }
		"""
		
		if 'legend_order' in d_graphs:
			legend_order = d_graphs.pop('legend_order')
		else:
			legend_order = d_graphs.keys()

		linestyles = ['solid', 'dashed', 'dashdot', 'dotted']
		colors = ['k', 'r', 'g', 'b', 'y', 'c', 'g']

		#fig = figure(num=None, figsize=(12, 12), dpi=300, facecolor='w', edgecolor='k')
		fig = figure(num=None, figsize=(12, 6), dpi=300, facecolor='w', edgecolor='k')

		for i in range(len(legend_order)):
			data_legend = legend_order[i]
			data = d_graphs[ data_legend ]

			# For now, we assume that every data has roughly the same step size with same vector length
			#### Need to modify here ####
			num_ys = [len(Y) for Y in data['Y']]
			num_Ys  = len( data['Y'] )
			min_num_ys = min(num_ys)

			avg_X  = [0.0] * min_num_ys
			avg_Y  = [0.0] * min_num_ys

			for j in range(num_Ys):
				X = data['X'][j]
				Y = data['Y'][j]

				for k in range( min_num_ys ):
					x = X[k]
					y = Y[k]

					avg_X[k] += x / num_Ys
					avg_Y[k] += y / num_Ys

			#############################
			if scale_graph == True:
				max_X = max(avg_X)
				min_X = min(avg_X)
				for k in range(len(avg_X)):
					avg_X[k] = (avg_X[k] - min_X) / (max_X - min_X)


			linestyle = linestyles[i % len(linestyles)]
			color = colors[i % len(colors)]

			plot(avg_X, avg_Y, label=data_legend, linewidth=3, linestyle=linestyle, color=color)
			hold(1)

		legend(labelspacing=0.2, prop={'size':27})
		xlabel(x_label, fontsize=27)
		ylabel(y_label, fontsize=27)

		font = {'size'   : 27}
		matplotlib.rc('font', **font)
		grid(which='major')
		title(plot_title, fontsize=27)

		savefig(filename)
		clf()

	def draw_prototype(self, filename, Xs, Ys, x_label, y_label, plot_title):
		Y_average = [ 0 ] * len(Ys[0])

		for i in range(len(Ys[0])):
			for j in range(len(Ys)):
				Y_average[i] += Ys[j][i]
			Y_average[i] = Y_average[i] / float( len(Ys) )

		X_average = Xs[0]

		plot(X_average, Y_average, linewidth=3, linestyle='solid', color='k')

		xlabel(x_label, fontsize=27)
		ylabel(y_label, fontsize=27)

		font = {'size'   : 27}
		matplotlib.rc('font', **font)
		grid(which='major')

		savefig(filename)
		clf()


	def draw_prototype_2(self, filename, d_graphs, x_label, y_label, plot_title, scale_graph):
		"""
			d_graphs: {'legend_name' : { 'X': [ [ X_round_i ], [ X_round_i+1 ], ... ] ]  , 'Y': [ [Y_i], [Y_i+1], ... ],
			           'legend_order' : ['legend_name_1', ..., ]  }
		"""
		
		legends    = d_graphs.keys()
		list_data  = d_graphs.values()

		linestyles = ['solid', 'solid', 'dashed', 'dashdot', 'dotted']
		colors = ['k', 'r', 'g', 'b', 'c', 'y', 'g']

		fig = figure(num=None, figsize=(12, 12), dpi=300, facecolor='w', edgecolor='k')

		for i in range(len(list_data)):
			#### Need to modify here ####
			X = list_data[i]['X'][0]
			Y = list_data[i]['Y'][0]
			#############################
			data_legend = legends[i]

			linestyle = linestyles[i % len(linestyles)]
			color = colors[i % len(colors)]

			plot(X, Y, label=data_legend, linewidth=3, linestyle=linestyle, color=color)
			hold(1)

		legend(labelspacing=0.2, prop={'size':27})
		xlabel(x_label, fontsize=27)
		ylabel(y_label, fontsize=27)

		font = {'size'   : 27}
		matplotlib.rc('font', **font)
		grid(which='major')

		savefig(filename)
		clf()

def show_graph(n_iterations, n_samples_per_iteration, list_data, legends, group_size, x_label, y_label, plot_title, filename, scale_graph):

	linestyles = ['solid', 'solid', 'dashed', 'dashdot', 'dotted']
	colors = ['k', 'r', 'g', 'b', 'c', 'y', 'g']

	fig = figure(num=None, figsize=(12, 12), dpi=300, facecolor='w', edgecolor='k')
	for i in range(len(list_data)):
		data = list_data[i]
		data_legend = legends[i]

		if len(data) != n_iterations:
			print "Dimension is different. Maybe a check?: %d, %d" % (len(data), n_iterations)
			n_data_points = len(data)
		else:
			n_data_points = n_iterations
		
		if scale_graph:	
			t = arange(0, n_data_points, group_size)
			t = t / float(n_data_points - 1)
		else:
			t = arange(0, n_data_points, group_size)
			t = t * n_samples_per_iteration

		grouped_data = []
		group_sum = 0

		for j in range(len(data)):
			data_point = data[j]
			group_sum += data_point

			if (j % group_size) == group_size - 1:
				grouped_data.append( group_sum / group_size )
				group_sum = 0

		if len(t) != len(grouped_data):
			print "Warning: length of the groups is different. Maybe a check?"
			print "Length of grouped data: ", len(grouped_data)
			print "Length of t: ", len(t)

		length_groups = min(len(grouped_data), len(t))

		linestyle = linestyles[i % len(linestyles)]
		color = colors[i % len(colors)]

		plot(t[0:length_groups], grouped_data[0:length_groups], label=data_legend, linewidth=3, linestyle=linestyle, color=color)
		hold(1)

	#legend(loc=(0.5,0.08))
	legend(labelspacing=0.2, prop={'size':27})
	xlabel(x_label, fontsize=27)
	ylabel(y_label, fontsize=27)
	#title(plot_title, fontsize=27)
	#font = {'family' : 'normal', 'weight' : 'bold', 'size'   : 22}
	font = {'size'   : 27}
	matplotlib.rc('font', **font)
	grid(which='major')

	savefig(filename)
	clf()