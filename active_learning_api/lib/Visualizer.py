__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Visualize various aspects of the active learning algorithm
"""

from ALWorker import ALWorker
from ProgressBar import AnimatedProgressBar
from Drawer import Drawer
from AnnotatorBuilder import AnnotatorBuilder
import os
import utility_functions
from utils import save_pickle, load_pickle
import numpy as np
from Simulator import Simulator, generate_exp_params
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from ScikitModel import ScikitModel
from sklearn.cluster import k_means, MeanShift, MiniBatchKMeans
from sklearn.neighbors import NearestNeighbors
import copy

class Visualizer:
	
	def __init__(self, worker, output_folder, do_PCA=True, draw_in_utility_space=False):
		"""
			worker : an instance of <Worker>. <Visualizer> creates visuals of the current snippets
			         or the information contained in this <Worker>.
		"""
		# Load the worker that this Visualizer will work with
		self.worker = worker
		self.draw_in_utility_space = draw_in_utility_space

		# Perform PCA with components 2
		if do_PCA:
			print "Performing PCA on the dataset ..."
			try:
				self.worker.dataset.X = PCA(n_components=2).fit_transform( self.worker.dataset.X )
			except:
				self.worker.dataset.X = PCA(n_components=2).fit_transform( self.worker.dataset.X.toarray() )
			print "PCA done!"

		# Train a new model using the PCA'd data
		# Assumes that the input model is an instance of <ScikitModel>
		print "Training a new model based on the PCA'd data"
		try:
			self.worker.model = ScikitModel( params={ 'clf_type' : self.worker.model.clf_type } )
			self.worker.train()
		except:
			self.worker.model = ScikitModel( params={ 'clf_type' : 'SVC' } )
			self.worker.train()
		print "Training done!"
		print "Test accuracy: %.3f" % self.worker.test()

		# Output folder map
		self.output_folder = output_folder


	def time_snap(self, time_snap_points):
		"""
			Time travel the worker to the point that it has `num_train_labeled_indices' labeled samples.
			Re-train the worker model, etc.
			Note: it can only go back to the past!
		"""

		print "Training a new model based on the PCA'd data, using the first min(%d, %d) samples" % (time_snap_points, self.worker.dataset.train_labeled_indices.shape[0])
		time_snap_points = min(time_snap_points, self.worker.dataset.train_labeled_indices.shape[0])

		# Forget the recent past
		self.worker.dataset.train_unlabeled_indices = np.append( self.worker.dataset.train_unlabeled_indices, self.worker.dataset.train_labeled_indices[time_snap_points:] )
		self.worker.dataset.train_labeled_indices = self.worker.dataset.train_labeled_indices[:time_snap_points]

		try:
			self.worker.model = ScikitModel( params={ 'clf_type' : self.worker.model.clf_type } )
			self.worker.train()			
		except:
			self.worker.model = ScikitModel( params={ 'clf_type' : 'SVC' } )
			self.worker.train()
		
		print "Training done!"
		print "Test accuracy: %.3f" % self.worker.test()


	def save_visualizer(self, output_name):
		# Save itself
		pass


	def draw_decision_boundaries(self, output_filename='', title='', show_labeled_data=False, show_unlabeled_data=False, hold=False):
		if self.draw_in_utility_space:
			# I'm not sure how to handle this.
			# Let's simply skip it, and maybe print why it won't draw it.
			print "Can't draw a decision boundary when self.draw_in_utility_space == True"

		else:
			model = self.worker.model
			dataset = self.worker.dataset

			# Create a mesh to plot in
			x_min, x_max = dataset.X[:, 0].min(), dataset.X[:, 0].max()
			y_min, y_max = dataset.X[:, 1].min(), dataset.X[:, 1].max()

			# step size in the mesh
			num_points = 300
			x_margin = (x_max - x_min) * 0.1
			y_margin = (y_max - y_min) * 0.1
			xh = (x_max - x_min + 2 * x_margin ) / num_points  # .02
			yh = (y_max - y_min + 2 * y_margin ) / num_points
			xx, yy = np.meshgrid(np.arange(x_min - x_margin, x_max + x_margin, xh),
			                     np.arange(y_min - y_margin, y_max + y_margin, yh))

			Z = model.predict(np.c_[xx.ravel(), yy.ravel()])

			# Put the result into a color plot
			Z = Z.reshape(xx.shape)
			plt.contourf(xx, yy, Z, cmap=plt.cm.Paired)
			plt.axis('off')

			# Plot also the data points if requested
			if show_labeled_data or show_unlabeled_data:
			
				color_map = { 0: (0, 0, .9), 1: (1, 0, 0), 2: (.8, .6, 0), 3: (0, 1, 0)}
				
				if show_labeled_data and show_unlabeled_data:			
					colors = [color_map[y] if y in color_map else (1, 1, 1) for y in dataset.Ysingle[ dataset.train_indices  ] ]
					plt.scatter(dataset.X[ dataset.train_indices ][:, 0], dataset.X[ dataset.train_indices ][:, 1], c=colors, cmap=plt.cm.Paired)

				elif show_labeled_data:
					colors = [color_map[y] for y in dataset.Ysingle[ dataset.train_labeled_indices  ] ]
					plt.scatter(dataset.X[ dataset.train_labeled_indices ][:, 0], dataset.X[ dataset.train_labeled_indices ][:, 1], c=colors, cmap=plt.cm.Paired)

				elif show_unlabeled_data:
					plt.scatter(dataset.X[ dataset.train_unlabeled_indices ][:, 0], dataset.X[ dataset.train_unlabeled_indices ][:, 1] , c=(1,1,1))

			# Show the plot
			if hold:
				plt.hold(True)
			else:
				plt.title( title )
				if output_filename == '':
					plt.show()
				else:
					plt.gcf().set_size_inches(20, 13.3)
					plt.savefig( os.path.join(self.output_folder, output_filename) )
				plt.clf()


	def draw_utility_values(self, utility_params, num_utility_points=-1, output_filename='', title='', hold=False ):
		dataset = self.worker.dataset
		model = self.worker.model
		annotators = self.worker.annotators['available']

		"""
		if get_utilities_of == 'train_unlabeled':
			indices = dataset.train_unlabeled_indices
		elif get_utilities_of == 'test':
			indices = dataset.test_indices
		elif (type(get_utilities_of) == type({})) and ('custom' in get_utilities_of):
			indices = get_utilities_of['custom']
		"""

		# Determine how many points to draw
		if num_utility_points == -1:
			# If it is a default value, we simply follow whatever was in utility_params['get_utilities_of']
			get_utilities_of = utility_params['get_utilities_of'] if ('get_utilities_of' in utility_params) else 'train_unlabeled'
			utility_params['get_utilities_of'] = get_utilities_of

		else:
			# Otherwise, we grab what was in 'get_utilities_of', and select a subset :num_utility_points
			if 'get_utilities_of' in utility_params:
				get_utilities_of =  utility_params['get_utilities_of']

				if get_utilities_of == 'train_unlabeled':
					indices_to_draw = copy.deepcopy( dataset.train_unlabeled_indices )
				elif get_utilities_of == 'test':
					indices_to_draw = copy.deepcopy( dataset.test_indices )
				elif (type(get_utilities_of) == type({})) and ('custom' in get_utilities_of):
					indices_to_draw = copy.deepcopy( get_utilities_of['custom'] )
			else:
				indices_to_draw = copy.deepcopy( dataset.train_unlabeled_indices )

			# Select a subset
			np.random.seed(seed=0)
			np.random.shuffle( indices_to_draw )
			indices_to_draw = indices_to_draw[:num_utility_points]							

			utility_params['get_utilities_of'] = {'custom' : indices_to_draw }


		# Determine data space to draw
		if self.draw_in_utility_space:
			# Obtain utility metrics
			(dict_utility, active_features_params, proactive_features_params) = \
				utility_functions.get_utility_metrics( utility_functions.get_indices_num_samples(dataset, utility_params['get_utilities_of'])[0], 
					dataset, model, annotators, 
					utility_params['active_features'], utility_params['active_features_params'], 
					utility_params['proactive_features'], utility_params['proactive_features_params'],
					{'log_weight' : utility_params['b_log_weight'], 'proactive_all' : True, 'proactive_best' : True} )
			
			if len(utility_params['proactive_features']) > 0:
				X = dict_utility['proactive_best']
			else:
				X = dict_utility['active']

			# Obtain utility values
			utility_tuples, utility_values, utility_indices, utility_annotators, utility_params = utility_functions.get_utility( dataset, model, annotators, utility_params )
			Z = utility_values

		else:
			# Obtain utility values
			utility_tuples, utility_values, utility_indices, utility_annotators, utility_params = utility_functions.get_utility( dataset, model, annotators, utility_params )

			X = dataset.X[utility_indices]
			Z = utility_values
		
		# Plot
		data_shape = X.shape
		if len(data_shape) == 1:
			data_dimension = 1
		elif len(data_shape) == 2:
			data_dimension = data_shape[1]
		else:
			print "That's weird, what is it?", data_shape
			data_dimension = 1

		if data_dimension == 1:
			plt.scatter(X[:,0], Z, alpha=0.85, s=1200 )

		elif data_dimension == 2:
			sc = plt.scatter(X[:,0], X[:,1], c=Z, alpha=0.85, s=1200 )
			plt.colorbar(sc)

		elif data_dimension > 2:
			X = PCA(n_components=2).fit_transform( X )
			sc = plt.scatter(X[:,0], X[:,1], c=Z, alpha=0.85, s=1200 )
			plt.colorbar(sc)

		# Show the plot
		if hold:
			plt.hold(True)
		else:
			plt.title( title )
			if output_filename == '':
				plt.show()
			else:
				plt.gcf().set_size_inches(20, 13.3)
				plt.savefig( os.path.join(self.output_folder, output_filename) )
			plt.clf()


	def draw_active_improvement(self, num_measure_points=20, type_measure_points=0, improvement_measure=0, utility_params={}, output_filename='', title='', hold=False):
		"""
			num_measure_points   : Number of points to measure improvement of
			type_measure_points  : 0 - Test on dataset.test_indices
			                       1 - Test on dataset.train_unlabeled_indices
			improvement_measure  : 0 - Measure improvement based on test accuracy
			                       1 - Measure improvement based on average entropy of test samples
		"""
		dataset = self.worker.dataset
		model = self.worker.model
		annotators = self.worker.annotators['available']

		# Cluster the rest of the unlabeled dataset, and get 'M' centroids
		if type_measure_points == 0:
			test_indices = dataset.test_indices
		elif type_measure_points == 1:
			test_indices = dataset.train_unlabeled_indices
		M = min( num_measure_points, len(test_indices) )

		# Determine data space to draw
		if self.draw_in_utility_space:
			# Obtain utility metrics
			(dict_utility, active_features_params, proactive_features_params) = \
				utility_functions.get_utility_metrics( dataset.train_unlabeled_indices, dataset, model, annotators, 
					utility_params['active_features'], utility_params['active_features_params'], 
					utility_params['proactive_features'], utility_params['proactive_features_params'],
					{'log_weight' : utility_params['b_log_weight'], 'proactive_all' : True, 'proactive_best' : True} )

			if len(utility_params['proactive_features']) > 0:
				X = dict_utility['proactive_best']
			else:
				X = dict_utility['active']

		else:
			X = dataset.X[ dataset.train_unlabeled_indices ]

		clusterer = MiniBatchKMeans(n_clusters=M, init='k-means++', n_init=1, init_size=1000, batch_size=1000)
		clusterer.fit( X )
		labels = clusterer.labels_
		centroids = clusterer.cluster_centers_

		# Sanity Check; make sure # of clutser == M
		try:
			assert( len(np.unique( labels )) == M )
		except:
			print "Please check here ............."
			print "%d doesn't equal %d" % ( len(np.unique( labels )), M )
			print "Centroid shape:", centroids.shape
			M = len(np.unique( labels ))
			centroids = centroids[ np.unique( labels ) ]



		# Calculate nearest neighbors for X
		G = min(5, X.shape[0])		
		nbrs = NearestNeighbors(n_neighbors=G).fit( X )

		# For each cluster centroid, get 'G' neighbors, annotate them
		# and make an updated model for each centroid
		list_extra_models = []
		list_extra_indices = []

		for cluster_index in range( M ):
			# Get centroid of each cluster
			centroid = centroids[cluster_index, :]

			# Get |G|-closest indices of each cluster.
			distances, neighbor_indices = nbrs.kneighbors( centroid )
			extra_indices = dataset.train_unlabeled_indices[ neighbor_indices[0] ]

			# Append it to the main indices
			indices = np.append( dataset.train_labeled_indices, extra_indices )

			######################################################################
			### The parent might not be a <ScikitModel>. For now it is assumed ###
			######################################################################	
			extra_model = ScikitModel( params=model.params )
			extra_model.train( dataset.X[indices], np.append(dataset.Ysingle[dataset.train_labeled_indices], dataset.Z[extra_indices] ) )
			
			list_extra_models.append( extra_model )
			list_extra_indices.append( extra_indices )

		# Measure initial performance
		if improvement_measure == 0:
			initial_performance  = model.test(  dataset.X[test_indices], dataset.Z[test_indices] ) * 100
		elif improvement_measure == 1:
			initial_performance = -model.measure_avg_entropy( dataset.X[test_indices] )

		# Record accuracy improvements when G neighbors around each of M centroids were obtained.
		delta_performances = np.empty( M )

		for m in range(len(list_extra_models)):
			# Calculate the difference in overall accuracy over test_indices between two models.
			extra_model   = list_extra_models[m]
			extra_indices = list_extra_indices[m]
			
			if improvement_measure == 0:
				performance_extra = extra_model.test( dataset.X[test_indices], dataset.Z[test_indices] ) * 100
			elif improvement_measure == 1:
				performance_extra = -extra_model.measure_avg_entropy( dataset.X[test_indices] )
			delta_performance = (performance_extra - initial_performance)

			# Append the results
			delta_performances[m] = delta_performance 

		# Plot
		data_shape = centroids.shape
		if len(data_shape) == 1:
			data_dimension = 1
		elif len(data_shape) == 2:
			data_dimension = data_shape[1]
		else:
			print "That's weird, what is it?", data_shape
			data_dimension = 1

		if data_dimension == 1:
			plt.scatter(centroids[:,0], delta_performances, alpha=0.85, s=1200 )

		elif data_dimension == 2:
			sc = plt.scatter(centroids[:,0], centroids[:,1], c=delta_performances, alpha=0.85, s=1200 )
			plt.colorbar(sc)

		elif data_dimension > 2:
			centroids = PCA(n_components=2).fit_transform( centroids )
			sc = plt.scatter(centroids[:,0], centroids[:,1], c=delta_performances, alpha=0.85, s=1200 )
			plt.colorbar(sc)



		# Show the plot
		if hold:
			plt.hold(True)
		else:
			plt.title( title )
			if output_filename == '':
				plt.show()
			else:
				plt.gcf().set_size_inches(20, 13.3)				
				plt.savefig( os.path.join(self.output_folder, output_filename) )
			plt.clf()

def load_visualizer(input_name):
	# Load an input visulizer and return a <Visualizer> instance
	return


if __name__ == '__main__':
	"""
		< Parameters > 
		'learner_type'    : type of <Learner> that defines the AL algorithm
		                    0 - <RandomLearner>
		                    1 - <MaxUtilityLearner>
		'learner_params'  : `params` passed to a <Learner> instance.
		                    'utility_params' : {
		                        'unlabeld_only' : (Boolean) to indicate if we are only considering utilities of unlabeled examples
		                        'active_features' : <List> of handles to feature methods (defined below)
		                        'active_features_params' : <List> of feature params in the same order as in 'active_features' {
		                            'density_est_type' : for utility functions that require density estimation
		                              0 - Kernel Density Estimation
		                              1 - K-means Reverse Count
		                        }
		                        'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
		                        'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
		                        'dynamic_params' : {
		                            # Params related to dynamic proactive learning (DPAL)
		                            'weight_update_method' : e.g. utility_functions.dpal_update
		                            'main_percentage' : percentage of # samples for the "main' model
		                            'M' : # of clusters for "extra" models
		                            'extra_model_group_method'
		                              0 - K-means clustering
		                              1 - K-means clustering + 10-NN of centroids 
		                            'extra_model_group_space'
		                              0 - Original concept space
		                              1 - Utility metrics space
		                        }
		                        'utility_sample' (for <MaxUtilityLearner>)
		                          0 - Sort the utility tuples by their utility value, and select the best `n_queries' candidates
		                          1 - Weighted sample of tuples by their normalized utility value
		                    }
		'model_type'      : type of <Model> that defines the classifier
		                    0 - <ScikitModel>
		'model_params'    : `params` passed to a <Model> instance.
		                    'clf_type' : (e.g.) 'LR', 'SVM', 'NB' etc.
		'd_annotators'    : a dictionary for ('name' : [ cost, type, params ] )
		'dataset_name'    : name of the dataset
		'budget'          : total budget assigned for a single fold task
		'percent_labeled' : percentage of initially labeled instances
		'n_queries'       : # of queries that will be made at each iteration
	    'n_folds'         : # of folds that will be performed for cross validation
	"""

	# Starting experiment setup
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'unlabeld_only' : True,
		'b_log_weight' : False,
		'active_features' : [ utility_functions.random_value ],
		'active_features_params' : [ {} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
	model_type       = 0
	model_params     = { 'clf_type' : 'SVC' }
	d_annotators     = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.0}) }
	dataset_name     = 'synth3'
	downsize         = -1
	budget           = 30
	percent_labeled  = 0.005
	n_queries        = 10
	n_folds          = 5
	return_after_exp = { 'workers' }

	exp_params = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds, return_after_exp )

	exp_results = Simulator().run( exp_params )
	worker = exp_results['workers'][0]
	
	# Output folder
	output_folder = '../results/visualizations'

	# Initialize a Visualizer
	visualizer = Visualizer( worker, output_folder )


	# Run experiments
	# (1). Decision Boundaries
	visualizer.draw_decision_boundaries( output_filename='', title='Decision Boundary', show_labeled_data=False, show_unlabeled_data=False, hold=True )


	# (2). Plot heatmap of utilities
	# (2) - 1: Entropy
	utility_params_1  = { 
		'unlabeld_only' : True,
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy ],
		'active_features_params' : [ {} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	#visualizer.draw_utility_values( utility_params_1, output_filename='', title='Entropy Based', hold=False )

	# (2) - 2: Density
	utility_params_2  = { 
		'unlabeld_only' : True,
		'b_log_weight' : False,
		'active_features' : [ utility_functions.density ],
		'active_features_params' : [ {'density_est_type' : 0} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	#visualizer.draw_utility_values( utility_params_2, output_filename='', title='Density Based', hold=False )

	# (2) - 3: Entropy + Density
	utility_params_3  = { 
		'unlabeld_only' : True,
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density ],
		'active_features_params' : [ {'normalize' : True}, { 'normalize' : True, 'density_est_type' : 0} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : {},
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	#visualizer.draw_utility_values( utility_params_3, output_filename='', title='Entropy + Density Based', hold=False )

	# (2) - 4: DPAL (Entropy + Density)
	utility_params_4  = { 
		'unlabeld_only' : True,
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density ],
		'active_features_params' : [ {'normalize' : True}, { 'normalize' : True, 'density_est_type' : 0} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
	                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
	                     'extra_model_group_space' : 0, 'regression_method' : 'svr_kernel',
	                     'b_update_weight' : utility_functions.dpal_update_always },
	    'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	#visualizer.draw_utility_values( utility_params_4, output_filename='', title='DPAL (Entropy + Density)', hold=False )


	# (3). Plot heatmap of actual improvements
	visualizer.draw_active_improvement( num_measure_points=320, type_measure_points=1, improvement_measure=0, output_filename='', title='Active Learning Improvement', hold=False )