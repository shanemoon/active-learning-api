__author__ = "Bronwyn Woods"

"""
	Author: Bronwyn Woods
	Description:
		A child class for <BaseModel>, which can handle the annotator parameters as
		well as the classifier parameters
"""

from BaseModel import BaseModel

class AnnotatorModel(BaseModel):

	def __init__(self, model_name='Annotator Joint Learner', params={}):
		BaseModel.__init__(self, model_name, params)
		self.clf_type = params['clf_type'] if 'clf_type' in params else 'LR'

		self.initialize_classifier()

	def initialize_classifier(self):
		pass

	def predict_proba(self, X):
		pass

	def predict(self, X):
		pass

	def train(self, X, Y, annotators=[]):
		pass

	def train_incremental(self, X, Y):
		pass
