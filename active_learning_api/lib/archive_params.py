"""
	Experiment Params Archive

	Here I record the params that would work well with each dataset.
	It is not meant to be loaded as a module, etc.
"""

	# Malware Dataset / Active Learning
	learner_type    = 1
	learner_params  = {'utility_function' : utility_functions.entropy_based}
	model_type      = 0
	model_params    = {'clf_type' : 'LR'}
	d_annotators    = {}	
	dataset_name    = 'mal'
	budget          = 50
	percent_labeled = 0.0005
	n_queries     = 5
	n_folds         = 5


	# Malware Dataset / Active Learning
	learner_type    = 1
	learner_params  = {'utility_function' : utility_functions.entropy_based}
	model_type      = 0
	model_params    = {'clf_type' : 'NB'}
	d_annotators    = {}	
	dataset_name    = '20newsgroups'
	budget          = 2500
	percent_labeled = 0.01
	n_queries     = 5
	n_folds         = 5






##########################################################################################
# List of parameters
list_exp_params  = []
names_exp_params = []


# Exp params 1
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.density ],
	'active_features_params' : [ {}, {'density_est_type' : 1} ],
	'proactive_features' : [ utility_functions.pro_p_answer ],
	'proactive_features_params' : [ {'p_ans_est_type' : 0} ],
	'dynamic' : {}
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.4}) }
dataset_name    = '20newsgroups'
budget          = 2000
percent_labeled = 0.01
n_queries     = 50
n_folds         = 5

exp_params_1 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_1 )
names_exp_params.append( 'Proactive' )


# Exp params 2
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.density ],
	'active_features_params' : [ {}, {'density_est_type' : 1} ],
	'proactive_features' : [],
	'proactive_features_params' : [],
	'dynamic_params' : {}
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.4}) }
dataset_name    = '20newsgroups'
budget          = 2000
percent_labeled = 0.01
n_queries     = 50
n_folds         = 5

exp_params_2 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_2 )
names_exp_params.append( 'Active' )

# Make sure that we have all the necessary folders
make_folders()



























####################################################################################################
from Simulator import *
import time

__author__ = "Shane Moon"

"""
	Description : Batch run the experiments.
	              Right now it is a bunch of copies and pastes.
	              Ideally, it needs some automation mechanism, which I won't worry about now.

	< Parameters > 
	'learner_type'    : type of <Learner> that defines the AL algorithm
	                    0 - <RandomLearner>
	                    1 - <MaxUtilityLearner>
	'learner_params'  : `params` passed to a <Learner> instance.
	                    'utility_params' : {
	                        'unlabeld_only' : (Boolean) to indicate if we are only considering utilities of unlabeled examples
	                        'active_features' : <List> of handles to feature methods (defined below)
	                        'active_features_params' : <List> of feature params in the same order as in 'active_features' {
	                            'density_est_type' : for utility functions that require density estimation
	                              0 - Kernel Density Estimation
	                              1 - K-means Reverse Count
	                        }
	                        'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
	                        'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
	                        'dynamic' : {
	                            # Params related to dynamic proactive learning (DPAL)
	                        }
	                    }
	'model_type'      : type of <Model> that defines the classifier
	                    0 - <ScikitModel>
	'model_params'    : `params` passed to a <Model> instance.
	                    'clf_type' : (e.g.) 'LR', 'SVM', 'NB' etc.
	'd_annotators'    : a dictionary for ('name' : [ cost, type, params ] )
	'dataset_name'    : name of the dataset
	'budget'          : total budget assigned for a single fold task
	'percent_labeled' : percentage of initially labeled instances
	'n_queries'       : # of queries that will be made at each iteration
    'n_folds'         : # of folds that will be performed for cross validation
"""

# List of parameters
list_exp_params  = []
names_exp_params = []


# Exp params 1
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.density ],
	'active_features_params' : [ {'normalize' : True}, {'density_est_type': 2, 'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update_3 }
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_1 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_1 )
names_exp_params.append( 'DPAL (US+DENS)' )





# Exp params 2
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.density ],
	'active_features_params' : [ {'normalize' : True}, {'density_est_type': 2, 'normalize' : False} ],
	'proactive_features' : [ utility_functions.pro_p_answer ],
	'proactive_features_params' : [ {'p_ans_est_type' : 0} ],
	'dynamic' : {}
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_2 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_2 )
names_exp_params.append( 'PAL (US+DENS+P_ANS)' )




# Exp params 3
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.density ],
	'active_features_params' : [ {'normalize' : True}, {'density_est_type': 2, 'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {}
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_3 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_3 )
names_exp_params.append( 'US+DENS' )



# Exp params 4
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy ],
	'active_features_params' : [ {'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {}
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_4 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_4 )
names_exp_params.append( 'US' )




# Exp params 5
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.density ],
	'active_features_params' : [ {'density_est_type': 2, 'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {}
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_4 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_4 )
names_exp_params.append( 'DENS' )




# Exp params 6
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.random_value ],
	'active_features_params' : [ {} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {}
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_6 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_6 )
names_exp_params.append( 'RAND' )


# Make sure that we have all the necessary folders
make_folders()





# Batch run the simulations
result_dictionary = {}
output_filename = './../results/graphs/20news.eps'

for i in range(len(list_exp_params)):
	exp_params = list_exp_params[i]
	legend = names_exp_params[i]
	print "==============================  Experiment %d, Code Name: %s  ==============================" % (i, legend)
	start_time = time.time()

	# Run the simulator
	fold_error_rates, fold_costs = Simulator().run( exp_params )

	# Add the result to result_dictionary
	result_dictionary[legend] = {'X':fold_costs, 'Y':fold_error_rates}
	elapsed_time = time.time() - start_time
	print "==============================  Experiment %d Done, Elapsed: %.2f  ==============================" % (i, elapsed_time)


# Draw the graph
Drawer().draw(output_filename, result_dictionary, 'Cost', 'Error Rate', '', False)











#####################################################################

from Simulator import *
import time

__author__ = "Shane Moon"

"""
	Description : Batch run the experiments.
	              Right now it is a bunch of copies and pastes.
	              Ideally, it needs some automation mechanism, which I won't worry about now.

	< Parameters > 
	'learner_type'    : type of <Learner> that defines the AL algorithm
	                    0 - <RandomLearner>
	                    1 - <MaxUtilityLearner>
	'learner_params'  : `params` passed to a <Learner> instance.
	                    'utility_params' : {
	                        'unlabeld_only' : (Boolean) to indicate if we are only considering utilities of unlabeled examples
	                        'active_features' : <List> of handles to feature methods (defined below)
	                        'active_features_params' : <List> of feature params in the same order as in 'active_features' {
	                            'density_est_type' : for utility functions that require density estimation
	                              0 - Kernel Density Estimation
	                              1 - K-means Reverse Count
	                        }
	                        'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
	                        'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
	                        'dynamic_params' : {
	                            # Params related to dynamic proactive learning (DPAL)
	                            'weight_update_method' : e.g. utility_functions.dpal_update
	                            'main_percentage' : percentage of # samples for the "main' model
	                            'M' : # of clusters for "extra" models
	                            'extra_model_group_method'
	                              0 - K-means clustering
	                              1 - K-means clustering + 10-NN of centroids 
	                            'extra_model_group_space'
	                              0 - Original concept space
	                              1 - Utility metrics space
	                        }
	                        'utility_sample' (for <MaxUtilityLearner>)
	                          0 - Sort the utility tuples by their utility value, and select the best `n_queries' candidates
	                          1 - Weighted sample of tuples by their normalized utility value
	                    }
	'model_type'      : type of <Model> that defines the classifier
	                    0 - <ScikitModel>
	'model_params'    : `params` passed to a <Model> instance.
	                    'clf_type' : (e.g.) 'LR', 'SVM', 'NB' etc.
	'd_annotators'    : a dictionary for ('name' : [ cost, type, params ] )
	'dataset_name'    : name of the dataset
	'budget'          : total budget assigned for a single fold task
	'percent_labeled' : percentage of initially labeled instances
	'n_queries'       : # of queries that will be made at each iteration
    'n_folds'         : # of folds that will be performed for cross validation
"""

# List of parameters
list_exp_params  = []
names_exp_params = []


# Exp params 1
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.entropy ],
	'active_features_params' : [ {'normalize' : True}, {'density_est_type': 2, 'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
	                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
	                     'extra_model_group_space' : 1  },
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_1 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_1 )
names_exp_params.append( 'DPAL (US+DENS)' )





"""
# Exp params 2
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.density ],
	'active_features_params' : [ {'normalize' : True}, {'density_est_type': 2, 'normalize' : False} ],
	'proactive_features' : [ utility_functions.pro_p_answer ],
	'proactive_features_params' : [ {'p_ans_est_type' : 0} ],
	'dynamic_params' : {},
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_2 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_2 )
names_exp_params.append( 'PAL (US+DENS+P_ANS)' )
"""



"""
# Exp params 3
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy, utility_functions.density ],
	'active_features_params' : [ {'normalize' : True}, {'density_est_type': 2, 'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {},
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_3 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_3 )
names_exp_params.append( 'US+DENS' )



# Exp params 4
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.entropy ],
	'active_features_params' : [ {'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {},
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_4 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_4 )
names_exp_params.append( 'US' )
"""



"""
# Exp params 5
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.density ],
	'active_features_params' : [ {'density_est_type': 2, 'normalize' : True} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {},
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_4 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_4 )
names_exp_params.append( 'DENS' )
"""



"""
# Exp params 6
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.random_value ],
	'active_features_params' : [ {} ],
	'proactive_features' : [  ],
	'proactive_features_params' : [  ],
	'dynamic_params' : {},
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 2000
percent_labeled = 0.01
n_queries     = 25
n_folds         = 5

exp_params_6 = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

list_exp_params.append( exp_params_6 )
names_exp_params.append( 'RAND' )
"""

# Make sure that we have all the necessary folders
#make_folders()





# Batch run the simulations
result_dictionary = {}
output_filename = './../results/graphs/20news.eps'

for i in range(len(list_exp_params)):
	exp_params = list_exp_params[i]
	legend = names_exp_params[i]
	print "==============================  Experiment %d, Code Name: %s  ==============================" % (i, legend)
	start_time = time.time()

	# Run the simulator
	fold_error_rates, fold_costs = Simulator().run( exp_params )

	# Add the result to result_dictionary
	result_dictionary[legend] = {'X':fold_costs, 'Y':fold_error_rates}
	elapsed_time = time.time() - start_time
	print "==============================  Experiment %d Done, Elapsed: %.2f  ==============================" % (i, elapsed_time)


# Draw the graph
Drawer().draw(output_filename, result_dictionary, 'Cost', 'Error Rate', '', False)