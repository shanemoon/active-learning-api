__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Class for an active learning worker that
		executes the following tasks:
		- Trains a Model
		- Performs Active Learning algorithm and chooses queries
		- Delegates the tasks to annotators
		- Updates and reports to the persistent database

		It can be called from a Simulator or an Experimentor.
		Each Worker is assigned with a dataset, a model,
		and a set of annotators.
"""

import numpy as np
from utils import print_errors

class ALWorker:

	def __init__(self, AL_learner, dataset, model, budget, annotators=[] ):
		"""
			Initialization method

			AL_learner : An <ActiveLearner> instance that defines the selection algorithms, etc.
			dataset    : A <Dataset> instance that wraps the actual dataset (e.g. filename, loaded data, etc.)
			model      : A <Model> instance which can predict, be trained, etc.
			annotators : A <List> of <Annotator>s for proactive learning scenario.
			             If [], we assume a single perfect oracle.

			query_history : <List> of <Tuple>s, consisting of ( annotator_name, indices_queried, Y )
		"""
		self.AL_learner = AL_learner
		self.dataset = dataset
		self.model = model
		self.budget = budget
		self.annotators = {'available': annotators, 'unavailable': [] }

		# Add the list of annotators to the dataset
		self.dataset.add_annotators(annotators)

		self.query_history = [ ]


	def __str__(self):
		return "<Active Learning Worker>" + \
			   "\n Active Learning Algorithm: %s" % self.AL_learner + \
			   "\n Dataset: %s" % self.dataset + \
			   "\n Model: %s" % self.model + \
			   "\n Annotators: %s" % str(self.annotators)


	def choose_queries(self, n_queries=1, info={}):
		"""
			Choose the best queries according to the AL_learner algorithm set for this Worker.
			Return a dictionary of annotator_index and indices_to_query that will answer this query.
		"""
		
		# Return : a dictionary { annotator_index : [index_to_query, ...], ... }
		available_annotators = self.get_available_annotators()
		annotator_index_to_indices_to_query = self.AL_learner.choose_queries(self.dataset, self.model, available_annotators, n_queries, info)
		return annotator_index_to_indices_to_query


	def initialize_rand_ground_truth(self, percent_labeled=0.1, rand_seed=0):
		"""
			Initialize randomly the small set of ground truth samples available
		"""
		# Shuffle the train indices
		np.random.seed( rand_seed )

		# Sudo-annotate the `percent_labeled` amount of train indices
		n_ground_truth_samples = int(self.dataset.train_indices.shape[0] * percent_labeled)

		# And this sudo-annotation is technically done by a GroundTruthAnnotator
		from GroundTruthAnnotator import GroundTruthAnnotator
		gta = GroundTruthAnnotator( name="Ground Truth Initializer", annotator_id=-1, cost=1, params={ 'Z' : self.dataset.Z} )
		self.dataset.add_annotators([gta])

		new_Y = np.zeros( n_ground_truth_samples )
		while len(np.unique(new_Y)) <= 1:
			# In case labels of all the initial samples are of the same class, we repeat sampling
			# Does this really happen? just checking ...
			indices_to_ground_truthfy = np.random.choice( self.dataset.train_indices,
			                                              n_ground_truth_samples, replace=False )
			(new_Y, response_type) = self.ask( gta, indices_to_ground_truthfy, for_free=True )


		# Update the model and the dataset record
		gta.availability = False
		self.add_annotator( gta )
		self.update( indices_to_ground_truthfy, new_Y, gta )


	def ask(self, annotator, indices_to_query, for_free=False, info={}):
		"""
			Ask the given annotator for instances at `indices_to_query`
			Return None if worker (self) cannot afford the annotator
		"""

		NORMAL_QUERY_RETURN_TYPE = 1
		INSUFFICIENT_BUDGET_RETURN_TYPE = 2

		try:
			# <type 'numpy.ndarray'>
			len_indices_to_query = indices_to_query.size
		except:
			# <type 'List'>
			len_indices_to_query = len(indices_to_query)

		if not for_free:
			budget_need = annotator.cost * len_indices_to_query
			if budget_need > self.budget:
				return (None, INSUFFICIENT_BUDGET_RETURN_TYPE)

			# Subtract the necessary cost for hiring an annotator to label `indices_to_query`
			self.budget -= budget_need

		# Get the answers from the annotator
		return (annotator.answer_queries( self.dataset, indices_to_query, info ), NORMAL_QUERY_RETURN_TYPE)

	def batch_update(self, list_indices_to_query, list_new_Y, list_annotators):
		"""
			Given new queries that came in batch, update the following:
			- Model
			- Query history of <ALWorker>
			- Query history for individual <Annotator>

			The difference from self.update(-) is that it won't spend time 
			training a new model for each annotator.
			This can reduce the total elapsed time roughly to roughly 1 / |K| annotators.
		"""

		pass


	def update(self, indices_to_query, new_Y, annotator ):
		"""
			Given new queries that came in, update the following:
			- Model
			- Query history of <ALWorker>
			- Query history for individual <Annotator>
		"""
		# Update the query history of <ALWorker>
		self.update_query_history( annotator, indices_to_query, new_Y )

		# Update the query history of individual <Annotator>
		annotator.update_query_history( indices_to_query, new_Y )

		# Update the model
		if self.model.can_learn_incrementally:
			# Update-train with only the new_Y
			self.train_incremental( indices_to_query, new_Y )
		else:
			# Train the model intirely
			self.train()


	def update_query_history(self, annotator, indices_to_query, new_Y ):
		# Update the query history list
		self.query_history.append( ( annotator.name, indices_to_query, new_Y ) )

		# Update the new_Y values to Y
		self.dataset.add_annotations(annotator, indices_to_query, new_Y)


	def print_query_history(self):
		"""
			Print the query history in a readable format
			query_history : 
			  [ ( annotator_name, indices_queried, Y ), ... ] 
		"""
		i = 0
		for e in self.query_history:
			print 'Iteration %d > \n- Annotator: %s\n- query_indices: %s\n- Y: %s\n' % (i, e[0], e[1], e[2])
			i += 1

		

	def train(self):
		X_train = self.dataset.X[ self.dataset.train_labeled_indices ]
		Y_train = self.dataset.Ysingle[ self.dataset.train_labeled_indices ]
		self.model.train( X_train, Y_train )


	def train_incremental(self, indices_to_query, new_Y):
		# Obtain the newly labeled X
		new_X = self.dataset.X[ indices_to_query ]
		self.model.train_incremental( new_X, new_Y )


	def test(self):
		"""
			Test the task that this worker is assigned with.
			Report the accuracy
		"""
		return self.model.test( self.dataset.X[ self.dataset.test_indices ], self.dataset.Z[ self.dataset.test_indices ] )


	def compare_prediction_results(self):
		"""
			(For debugging purpose), compare the prediction results and ground truth
		"""
		self.model.compare_prediction_results( self.dataset.X[ self.dataset.test_indices ], self.dataset.Z[ self.dataset.test_indices ] )


	def add_annotator(self, annotator):
		if annotator.availability == True:
			self.annotators['available'].append( annotator )
		else:
			self.annotators['unavailable'].append( annotator )

	def remove_annotator(self, annotator):
		annotator_id = annotator.annotator_id
		###########################################
		############ To be implemented ############
		###########################################
		pass

	def get_annotator_by_id(self, annotator_id):
		"""
			Find an <Annotator> with an annotator_id, and return it.
			If it does not exist, create one, add to self.annotators, and return it.
		"""
		for annotator in self.annotators['available']:
			if annotator.id == annotator_id:
				return annotator

		for annotator in self.annotators['unavailable']:
			if annotator.id == annotator_id:
				return annotator

		# It does not exist, so create one.
		from BaseAnnotator import BaseAnnotator
		annotator = BaseAnnotator(annotator_id=annotator_id)
		self.add_annotator( annotator )
		return annotator

	def replace_annotator_by_id(self, annotator_id_to_replace, new_annotator):
		"""
			Find an <Annotator> with an annotator_id, and replace it with a new annotator.
			If it does not exist, do nothing.
		"""
		for annotator_index in range(len(self.annotators['available'])):
			annotator = self.annotators['available'][annotator_index]
			if annotator.id == annotator_id_to_replace:
				self.annotators['available'][annotator_index] = new_annotator
				return

		for annotator_index in range(len(self.annotators['unavailable'])):
			annotator = self.annotators['available'][annotator_index]
			if annotator.id == annotator_id_to_replace:
				self.annotators['available'][annotator_index] = new_annotator
				return

		# It does not exist, do nothing but print warning
		print "Warning: could not find an annotator with ID %d" % annotator_id_to_replace

	def get_available_annotators(self):
		return self.annotators['available']


	def set_annotator_availability(self, annotator_id, new_availability):
		annotator = get_annotator_by_id( annotator_id )

		if annotator.availability == True:
			if new_availability == True:
				# We don't change anything then.
				pass
			else:
				###########################################
				############ To be implemented ############
				###########################################
				pass
		else:
			if new_availability == True:
				###########################################
				############ To be implemented ############
				###########################################
				pass
			else:
				# We don't change anything then.
				pass				


	def estimate_annotators_accuracy(self, estimation_method, estimation_budget, params={}):
		"""
			Estimate annotators' labeling accuracies with the given budget for estimation.
			Should take a (1) estimation method, (2) estimation_budget as input.
		"""
		pass