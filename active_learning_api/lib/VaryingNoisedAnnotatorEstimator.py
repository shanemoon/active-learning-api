__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		Parent class of <AnnotatorEstimator>, which make gaussian noised estimation of
		annotators if possible, e.g. for <GroundTruthAnnotator> or <NoisedGroundTruthAnnotator>
"""

import numpy as np
from NoisedGroundTruthAnnotator import NoisedGroundTruthAnnotator
from GroundTruthAnnotator import GroundTruthAnnotator
from AnnotatorEstimator import AnnotatorEstimator
from NoisedAnnotatorEstimator import NoisedAnnotatorEstimator

class VaryingNoisedAnnotatorEstimator(AnnotatorEstimator):

	def __init__(self):
		AnnotatorEstimator.__init__(self)
		pass

	def estimate(self, dataset, annotators, params={}):
		"""
			Return the 
			Return type: [ # Samples X # Annotators] np.array where each element \in [0, 1]
		"""
		p_ans_sigma_start = params['p_ans_sigma_start']
		p_ans_sigma_goal  = params['p_ans_sigma_goal']
		n_goal_reach_samples = params['n_goal_reach_samples']

		if 'n_first_seen_train_labeled_indices' in params:
			n_first_seen_train_labeled_indices = params['n_first_seen_train_labeled_indices']
		else:
			print
			print "This should be printed once every fold"
			print "**************************************"
			n_first_seen_train_labeled_indices = len(dataset.train_labeled_indices)
			params['n_first_seen_train_labeled_indices'] = n_first_seen_train_labeled_indices

		p_ans_sigma = p_ans_sigma_start - \
					  (p_ans_sigma_start - p_ans_sigma_goal) * \
					  min( ( len(dataset.train_labeled_indices) - n_first_seen_train_labeled_indices ) / \
					  float( n_goal_reach_samples ), 1.0)
		
		constructed_params = {'p_ans_sigma' : p_ans_sigma }

		# First get a perfect estimate of annotators
		return NoisedAnnotatorEstimator().estimate( dataset, annotators, constructed_params )