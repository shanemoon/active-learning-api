from utility_functions import even_cluster
from Simulator import load_dataset
from utils import vector_to_csv, matrix_to_csv
import numpy as np

dataset_name = '3class_from_alldata_nz_pca'
path_dataset = '../datasets/%s/data.pkl' % dataset_name
n_folds = 5
i_fold = 1
downsize = -1
utility_csv_filename = './3class_from_alldata_nz_pca_centroids_sampling_utility.csv'
order_csv_filename = './3class_from_alldata_nz_pca_centroids_sampling_order.csv' 
three_col_csv_filename = './3class_from_alldata_nz_pca_centroids_sampling.csv' 

dataset = load_dataset( path_dataset, n_folds, i_fold, dataset_name, downsize )
model = None
annotators = None
utility_params = {'get_utilities_of' : 'all', 'num_clusters' : 6}

(feature_value_vector, params) = even_cluster( dataset, model, annotators, params=utility_params )

print feature_value_vector.shape

aggregated_matrix = []
num2labelname = {}
num2labelname[0] = "DirectDownloader"
num2labelname[1] = "Scraze"
num2labelname[2] = "allaple"

for i in range(len(feature_value_vector)):
	feature_value = float(feature_value_vector[i])
	aggregated_matrix.append( [i, feature_value, num2labelname[ dataset.Z[i] ] ] )

# Save it into .csv
vector_to_csv(feature_value_vector, utility_csv_filename)
vector_to_csv( np.argsort( -feature_value_vector), order_csv_filename)
matrix_to_csv( aggregated_matrix, three_col_csv_filename )