import sys, traceback
import csv
import getpass
import numpy as np
import operator as op

csv.field_size_limit(sys.maxsize)

"""
	Author : Shane Moon
	Description :
		Some of the useful functions for pre-processing are defined here.
		Examples:
			- filename_maker: for consistent filename assignment
			- load_dataset  : for universal dataset loading
"""

def filename_maker( prefix, extension, params={} ):
	"""
		e.g. 
		prefix = 'output/segmented_blogs'
		extension = 'csv'
		params = {'s': 1, 'f': 0}

		return: 'output/segmented_blogs_s_1_f_0.csv'
	"""
	str_params = '_'.join( ['%s_%d' % (key, val) for key, val in params.items()] )
	if str_params == '':
		# If no params were given:
		return "%s.%s" % (prefix, extension )
	return "%s_%s.%s" % (prefix, str_params, extension )

def vector_to_csv(vector, filename, header=[], delimiter=' '):
	"""
		Convert a vector into a csv object,
		and save it as a .csv file with the provided filename.

		Vector is a 2d list, where each element of the vector
		corresponds to a row
	"""	
	with open(filename, 'wb') as csvfile:
		writer = csv.writer(csvfile, delimiter=delimiter)

		if header != []:
			writer.writerow( header )

		for element in vector:
			writer.writerow( [element] )
	return


def matrix_to_csv(matrix, filename, header=[], delimiter=' '):
	"""
		Convert a matrix into a csv object,
		and save it as a .csv file with the provided filename.

		Matrix is a 2d list, where each element of the matrix
		corresponds to a row
	"""	
	with open(filename, 'wb') as csvfile:
		writer = csv.writer(csvfile, delimiter=delimiter)

		if header != []:
			writer.writerow( header )

		for row in matrix:
			try:
				writer.writerow( row )
			except:
				try:
					for i in range(len(row)):
						element = row[i]
						if type(element) == type(u''):
							element = element.encode('ascii', 'ignore')
							row[i] = element
					writer.writerow( row )
				except:					
					print 'Something went wrong with this row: %s' % repr(row)
					print '-'*60
					traceback.print_exc(file=sys.stdout)
					print '-'*60			
	return

def csv_to_matrix(csv_filename, skip_header=False, delimiter=' '):
	with open( csv_filename, 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=delimiter)
		if skip_header:
			next(reader, None)  # skip the headers

		matrix = []

		for row in reader:
			try:
				matrix.append( row )			
			except:
				print "Something went wrong."

		return matrix

def txt_to_np_2darray(csv_filename, skip_header=False, delimiter=' '):
	with open( csv_filename, 'rb') as csvfile:
		reader = csv.reader(csvfile, delimiter=delimiter)
		if skip_header:
			next(reader, None)  # skip the headers

		data = np.array([])

		first_time = True
		for row in reader:
			try:
				row = map(float, row)
				if first_time:
					data = np.append( data, row )
					first_time = False
				else:
					data = np.vstack( (data, row )) 
			except:
				print_errors()

		return data

def combine_csvs_to_one(csv_filenames, output_csv_filename, header=[], delimiter=' '):
	"""
		Combine the input csv's and output the combined version
		Input csv's are assumed to be have the same format.
	"""
	combined_matrix = []
	for csv_filename in csv_filenames:
		# Read each csv
		if header == []:
			matrix = csv_to_matrix(csv_filename, skip_header=False, delimiter=delimiter)
		else:
			matrix = csv_to_matrix(csv_filename, skip_header=True, delimiter=delimiter)
		combined_matrix.extend( matrix )

	# Now output the combined version to a .csv
	matrix_to_csv(combined_matrix, output_csv_filename, header=header)
	return combined_matrix

def ndarray_to_libsvm(array, output_filename):
	# libsvm / svmlight format
	# : better for a sparse matrix
	from sklearn.datasets import dump_svmlight_file
	import numpy as np
	dump_svmlight_file(array, np.zeros((array.shape[0],)), output_filename)


def ndarray_to_txt(array, output_filename):
	# comma separated text file
	import numpy as np
	np_2darray = array + np.zeros( array.shape ) # convert sparse matrix to np array
	np.savetxt(output_filename, np_2darray, newline="\n", fmt="%s")

def ndarray_to_mat(array, output_filename):
	# comma separated text file
	import numpy as np
	import scipy.io as sio

	np_2darray = array + np.zeros( array.shape ) # convert sparse matrix to np array
	sio.savemat(output_filename, {'data' : np_2darray})

def save_pickle(object_to_save, output_filename):
	import dill as pickle

	with open(output_filename, 'wb') as output:
		pickle.dump(object_to_save, output, pickle.HIGHEST_PROTOCOL)	

def load_pickle(input_filename):
	import dill as pickle
	
	with open(input_filename, 'rb') as input_file:
		loaded_object = pickle.load(input_file)
		return loaded_object

def nCr(n, r):
	r = min(r, n-r)
	if r == 0: return 1
	numer = reduce(op.mul, xrange(n, n-r, -1))
	denom = reduce(op.mul, xrange(1, r+1))
	return numer//denom

def print_errors(position_index='Not Given'):
	print 'Something went wrong at: %s' % position_index
	print '-'*60
	traceback.print_exc(file=sys.stdout)
	print '-'*60		