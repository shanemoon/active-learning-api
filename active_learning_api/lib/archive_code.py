def dpal_update_1( dataset, model, annotators, active_features, active_features_params, proactive_features, proactive_features_params, dynamic_params ):
	
	# 1. Choose |M|=10 sets of random permutations using 60% of test_indices
	
	M = 10
	percentage = 0.4
	models = []
	test_indices = dataset.test_indices
	labeled_indices = dataset.train_labeled_indices
	################################################################
	### More rigorous n choose 2 checking might be required here ###
	################################################################
	n_samples = int(len(labeled_indices) * percentage)
	parent_model_params = model.params 

	for m in range(M):
		indices = np.random.choice( labeled_indices, n_samples, replace=False )

		######################################################################
		### The parent might not be a <ScikitModel>. For now it is assumed ###
		######################################################################	
		child_model = ScikitModel( params=parent_model_params )
		child_model.train( dataset.X[indices], dataset.Ysingle[indices] )
		models.append( child_model )


	# 2. For M Choose 2 (=45) models
	num_rows = nCr(M,2)
	num_cols = len(active_features) + len(proactive_features)

	delta_feature_matrix = np.empty( (num_rows, num_cols) )
	delta_accuracies = np.empty( num_rows )

	k = 0
	for model_index_1, model_index_2 in itertools.combinations( range(M), 2 ):
		# Obtain Models
		model_1 = models[model_index_1]
		model_2 = models[model_index_2]

		# 2-1. Calculate the difference in sum of utilities over test_indices between two models. These are 'X' (\psi(L+) - \psi(L-))

		# Active Features
		delta_sum_active_feature_vector = np.zeros( len(active_features) )
		for i in range(len(active_features)):
			# For each active feature
			active_feature = active_features[i]
			active_feature_params = active_features_params[i]
			active_feature_params['get_utilities_of'] = 'test'

			# Obtain feature value vector for each feature
			each_active_feature_value_vector_1, new_feature_params = active_feature( dataset, model_1, annotators, active_feature_params )	
			each_active_feature_value_vector_2, new_feature_params = active_feature( dataset, model_2, annotators, active_feature_params )	

			# Obtain the delta sum, and append it to delta_sum_active_feature_vector
			delta_sum_active_feature_vector[i] = sum(each_active_feature_value_vector_1) - sum(each_active_feature_value_vector_2)

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################	
			# Update active_feature_params in case we have an updated params
			active_features_params[i] = new_feature_params	


		# Proactive Features
		delta_sum_proactive_feature_vector = np.zeros( len(proactive_features) )
		for j in range(len(proactive_features)):
			# For each active feature
			proactive_feature = proactive_features[j]
			proactive_feature_params = proactive_features_params[j]
			proactive_feature_params['get_utilities_of'] = 'test'

			# Obtain feature value matrix for each feature
			each_proactive_feature_value_matrix_1, new_feature_params = proactive_feature( dataset, model_1, annotators, proactive_feature_params )	
			each_proactive_feature_value_matrix_2, new_feature_params = proactive_feature( dataset, model_2, annotators, proactive_feature_params )	

			# Obtain the delta sum, and append it to delta_sum_proactive_feature_vector
			delta_sum_proactive_feature_vector[j] = sum([sum(a) for a in each_proactive_feature_value_matrix_1]) - sum([sum(a) for a in each_proactive_feature_value_matrix_2])

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################	
			# Update proactive_feature_params in case we have an updated params
			proactive_features_params[j] = new_feature_params	


		# 2-2. Calculate the difference in overall accuracy over test_indices between two models. These are 'Y' (\delta accuracy)
		accuracy_1 = model_1.test( dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )
		accuracy_2 = model_2.test( dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )
		delta_accuracy = (accuracy_1 - accuracy_2) * 100

		# Append the results
		delta_feature_matrix[k, 0:len(active_features)] = delta_sum_active_feature_vector
		delta_feature_matrix[k, len(active_features):len(proactive_features)] = delta_sum_proactive_feature_vector
		delta_accuracies[k] = delta_accuracy

		k += 1

	# 3. Perform Linear Regression on 'X' and 'Y'. Divide it into `active_feature_weights' and `proactive_feature_weights'
	clf = linear_model.LinearRegression(normalize=True, copy_X=False)
	clf.fit( delta_feature_matrix, delta_accuracies )
	weights = clf.coef_

	active_feature_weights = weights[0:len(active_features)]
	proactive_feature_weights = weights[len(active_features):]

	return active_feature_weights, proactive_feature_weights, clf



def dpal_update_2( dataset, model, annotators, active_features, active_features_params, proactive_features, proactive_features_params, dynamic_params ):
	
	# 1. Choose main_percentage := 50%, and |M|=50 sets of random permutations using extra_percentage :=20% of labeled_indices,
	#    and build models for each
	M = 30
	main_percentage  = 0.5
	extra_percentage = 0.2

	test_indices = dataset.test_indices
	labeled_indices = dataset.train_labeled_indices

	# Build the main model
	n_main_samples = int(len(labeled_indices) * main_percentage)
	main_indices = labeled_indices[0:n_main_samples]      # Takes the first main_percentage indices

	parent_model_params = model.params
	main_model = ScikitModel( params=parent_model_params )
	main_model.train( dataset.X[main_indices], dataset.Ysingle[main_indices] )

	# Build extra tuned models
	n_extra_samples = int(len(labeled_indices) * extra_percentage)
	extra_indices_pool = labeled_indices[n_main_samples:] # Takes the rest after main_percentage indices as pool
	
	list_extra_models = []
	list_extra_indices = []

	for m in range(M):
		extra_indices = np.random.choice( extra_indices_pool, n_extra_samples, replace=False )
		indices = np.append( main_indices, extra_indices )

		######################################################################
		### The parent might not be a <ScikitModel>. For now it is assumed ###
		######################################################################	
		extra_model = ScikitModel( params=parent_model_params )
		extra_model.train( dataset.X[indices], dataset.Ysingle[indices] )
		list_extra_models.append( extra_model )
		list_extra_indices.append( extra_indices )


	# 2. For each extra model
	num_rows = M
	num_cols = len(active_features) + len(proactive_features)

	delta_feature_matrix = np.empty( (num_rows, num_cols) )
	delta_accuracies = np.empty( num_rows )

	for k in range(len(list_extra_models)):
		# 2.1 Calculate the sum of utilities over extra_indices to the main model. These are 'X' (\psi(X, K, L))
		extra_model   = list_extra_models[k]
		extra_indices = list_extra_indices[k]
		len_extra_indices = len(extra_indices)

		# Active Features
		avg_active_feature_vector = np.zeros( len(active_features) )  # This is a (p x 1) vector where p is # of active_feature attributes
		for i in range(len(active_features)):
			# For each active feature
			active_feature = active_features[i]
			active_feature_params = active_features_params[i]
			active_feature_params['get_utilities_of'] = { 'custom' : extra_indices }

			# Obtain feature value vector for each feature
			each_active_feature_value_vector_main, new_feature_params = active_feature( dataset, main_model, annotators, active_feature_params )	

			# Obtain the delta sum, and append it to avg_active_feature_vector
			avg_active_feature_vector[i] = sum(each_active_feature_value_vector_main) / len_extra_indices

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################	
			# Update active_feature_params in case we have an updated params
			active_features_params[i] = new_feature_params	


		# Proactive Features
		avg_proactive_feature_vector = np.zeros( len(proactive_features) )
		num_annotators = len(annotators)
		for j in range(len(proactive_features)):
			# For each active feature
			proactive_feature = proactive_features[j]
			proactive_feature_params = proactive_features_params[j]
			proactive_feature_params['get_utilities_of'] = { 'custom' : extra_indices }

			# Obtain feature value matrix for each feature
			each_proactive_feature_value_matrix_main, new_feature_params = proactive_feature( dataset, main_model, annotators, proactive_feature_params )	

			# Obtain the delta sum, and append it to avg_proactive_feature_vector
			avg_proactive_feature_vector[j] = sum([sum(a) for a in each_proactive_feature_value_matrix_main]) / len_extra_indices / num_annotators

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################	
			# Update proactive_feature_params in case we have an updated params
			proactive_features_params[j] = new_feature_params	


		# 2-2. Calculate the difference in overall accuracy over dataset.test_indices between two models. These are 'Y' (\delta accuracy)
		accuracy_extra = extra_model.test( dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )
		accuracy_main  = main_model.test(  dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )
		delta_accuracy = (accuracy_extra - accuracy_main)

		# Append the results
		delta_feature_matrix[k, 0:len(active_features)] = avg_active_feature_vector
		delta_feature_matrix[k, len(active_features):len(proactive_features)] = avg_proactive_feature_vector
		delta_accuracies[k] = delta_accuracy


	# 3. Perform Linear Regression on 'X' and 'Y'. Divide it into `active_feature_weights' and `proactive_feature_weights'
	clf = linear_model.LinearRegression()
	clf.fit( delta_feature_matrix, delta_accuracies )
	weights = clf.coef_

	active_feature_weights = weights[0:len(active_features)]
	proactive_feature_weights = weights[len(active_features):]

	print ''
	print delta_feature_matrix[0:5,:]
	print delta_accuracies[0:5]
	print 'Weights: %s ' % weights
	print 'R score: %.3f' % clf.score(delta_feature_matrix, delta_accuracies)
	return active_feature_weights, proactive_feature_weights, clf



def dpal_update_3( dataset, model, annotators, active_features, active_features_params, proactive_features, proactive_features_params, dynamic_params ):
	
	# 1. Choose main_percentage := 50%, and |M|=15 clusters for the remaining indices,
	#    and build models for each
	test_indices = dataset.test_indices
	labeled_indices = dataset.train_labeled_indices

	main_percentage  = 0.8
	M = min( 20, int( len(labeled_indices) * (1-main_percentage) ) )

	# 2. Build the main model
	"""
		To do: in building the "main" model:
		- Option 1. Randomly select 60~70 percent of labeled indices (as it is now)
		- Option 2. Select the first 60~70 percent of labeled indices by the order they were annotated
		- Option 3. Select the "center-most" 60~70 percent of labeled indices
	"""
	n_main_samples = int(len(labeled_indices) * main_percentage)
	main_indices = labeled_indices[0:n_main_samples]      # Takes the first main_percentage indices

	parent_model_params = model.params
	main_model = ScikitModel( params=parent_model_params )
	main_model.train( dataset.X[main_indices], dataset.Ysingle[main_indices] )


	# 3. Build extra tuned models
	extra_indices_pool = labeled_indices[n_main_samples:] # Takes the rest after main_percentage indices as pool
	
	list_extra_models = []
	list_extra_indices = []

	"""
		To do: in building the "extra" models:
		- Option 1: Apply conventional clustering methods (uneven) on the rest of the labeled indices
		- Option 2: Obtain centroids via conventional methods, make new groupings by taking |G|-closest neighbors of each centroid.
		- Option 3: Equal size clustering method.
		- Option 4: Do any of the above, but in the utility score space instead of concept space

		Output:
			- list_extra_models
			- list_extra_indices
	"""	
	extra_model_group_method = 1

	if extra_model_group_method == 0:
		# Perform K-means clustering
		# labels    : (n x 1)
		clusterer = MiniBatchKMeans(n_clusters=M, init='k-means++', n_init=1, init_size=1000, batch_size=1000)
		#clusterer = AffinityPropagation()
		#clusterer = AgglomerativeClustering(n_clusters=M)
		#min_samples = max(1, int( len(labeled_indices) * (1-main_percentage) ) / M)
		#clusterer = DBSCAN(min_samples=min_samples)

		clusterer.fit( dataset.X[extra_indices_pool] )
		#clusterer.fit( dataset.X[extra_indices_pool].toarray() )
		labels = clusterer.labels_
		M = len(np.unique( labels ))
		"""
			Sanity check: np.unique( labels ) sum to (M - 1) * M / 2 
		"""
		print "========================================================================="
		print "This should be True: %s" % str( M * (M-1) / 2 == sum(np.unique(labels)) )
		print "========================================================================="

		sizes_of_clusters = [ len(np.where( labels == label )[0] ) for label in np.unique( labels ) ]
		min_cluster_size = min(sizes_of_clusters)

		for label in np.unique( labels ):
			# Get indices of each cluster
			extra_indices = extra_indices_pool[ np.where( labels == label ) ]

			# Make sure that every cluster has the same size
			extra_indices = extra_indices[:min_cluster_size]

			# Sanity check
			if len(extra_indices) < 1:
				print
				print "=================================================================================="
				print "Something is very weird .................. please check @ utility_functions dpal_3"
				print "=================================================================================="

				"""
					Another sanity check; extra_indices are not in main_indices
				"""
			else:
				# Append it to the main indices
				indices = np.append( main_indices, extra_indices )

				######################################################################
				### The parent might not be a <ScikitModel>. For now it is assumed ###
				######################################################################	
				extra_model = ScikitModel( params=parent_model_params )
				extra_model.train( dataset.X[indices], dataset.Ysingle[indices] )
				list_extra_models.append( extra_model )
				list_extra_indices.append( extra_indices )


	elif extra_model_group_method == 1:
		# Perform K-means clustering, get centroids, and get |G| closest neighbors for each cluster.
		# labels    : (n x 1)
		# centroids : (n x p)
		clusterer = MiniBatchKMeans(n_clusters=M, init='k-means++', n_init=1, init_size=1000, batch_size=1000)
		clusterer.fit( dataset.X[extra_indices_pool] )
		labels = clusterer.labels_
		centroids = clusterer.cluster_centers_

		sizes_of_clusters = []

		for cluster_index in range(len(np.unique( labels ))):
			# Get centroid of each cluster
			centroid = centroids[cluster_index, :]

			# Get |G|-closest indices of each cluster.
			#extra_indices = extra_indices_pool[ np.where( labels == label ) ]
			"""
				TO DO: Get |G| nearest neighbors, get their indices
			"""
			G = 10
			nbrs = NearestNeighbors(n_neighbors=G).fit( dataset.X[extra_indices_pool] )
			distances, neighbor_indices = nbrs.kneighbors( centroid )
			extra_indices = extra_indices_pool[ neighbor_indices[0] ]

			sizes_of_clusters.append( len(extra_indices) )

			# Append it to the main indices
			indices = np.append( main_indices, extra_indices )

			# Sanity check: there is no overalp between main_indies and extra_indices
			# In other words, len(indices) == len(np.unique(indices))
			if len(indices) == len(np.unique(indices)):
				pass
			else:
				print "=================================================================================="
				print "Duplicate main_indies and extra_indices ...... something's wrong in dpal_3, check."
				print "=================================================================================="				

			######################################################################
			### The parent might not be a <ScikitModel>. For now it is assumed ###
			######################################################################	
			extra_model = ScikitModel( params=parent_model_params )
			extra_model.train( dataset.X[indices], dataset.Ysingle[indices] )
			list_extra_models.append( extra_model )
			list_extra_indices.append( extra_indices )

		min_cluster_size = min(sizes_of_clusters)


	# 4. For each extra model
	num_rows = len(list_extra_models)
	num_cols = len(active_features) + len(proactive_features)

	delta_feature_matrix = np.empty( (num_rows, num_cols) )
	delta_accuracies = np.empty( num_rows )

	for k in range(len(list_extra_models)):
		# 4.1 Calculate the sum of utilities over extra_indices to the main model. These are 'X' (\psi(X, K, L))
		extra_model   = list_extra_models[k]
		extra_indices = list_extra_indices[k]
		len_extra_indices = len(extra_indices)

		# Active Features
		avg_active_feature_vector = np.zeros( len(active_features) )  # This is a (p x 1) vector where p is # of active_feature attributes
		for i in range(len(active_features)):
			# For each active feature
			active_feature = active_features[i]
			active_feature_params = active_features_params[i]
			active_feature_params['get_utilities_of'] = { 'custom' : extra_indices }

			# Obtain feature value vector for each feature
			each_active_feature_value_vector_main, new_feature_params = active_feature( dataset, main_model, annotators, active_feature_params )

			# Obtain the delta sum, and append it to avg_active_feature_vector
			avg_active_feature_vector[i] = sum(each_active_feature_value_vector_main) #/ len_extra_indices

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################	
			# Update active_feature_params in case we have an updated params
			active_features_params[i] = new_feature_params	


		# Proactive Features
		avg_proactive_feature_vector = np.zeros( len(proactive_features) )
		num_annotators = len(annotators)
		for j in range(len(proactive_features)):
			# For each active feature
			proactive_feature = proactive_features[j]
			proactive_feature_params = proactive_features_params[j]
			proactive_feature_params['get_utilities_of'] = { 'custom' : extra_indices }

			# Obtain feature value matrix for each feature
			each_proactive_feature_value_matrix_main, new_feature_params = proactive_feature( dataset, main_model, annotators, proactive_feature_params )	

			# Obtain the delta sum, and append it to avg_proactive_feature_vector
			avg_proactive_feature_vector[j] = sum([sum(a) for a in each_proactive_feature_value_matrix_main])  / num_annotators #/ len_extra_indices

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################	
			# Update proactive_feature_params in case we have an updated params
			proactive_features_params[j] = new_feature_params	


		# 4-2. Calculate the difference in overall accuracy over dataset.test_indices between two models. These are 'Y' (\delta accuracy)
		accuracy_extra = extra_model.test( dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )
		accuracy_main  = main_model.test(  dataset.X[dataset.test_indices], dataset.Z[dataset.test_indices] )
		delta_accuracy = (accuracy_extra - accuracy_main)

		# Append the results
		delta_feature_matrix[k, 0:len(active_features)] = avg_active_feature_vector
		delta_feature_matrix[k, len(active_features):len(proactive_features)] = avg_proactive_feature_vector
		delta_accuracies[k] = delta_accuracy * 100


	# 5. Perform Linear Regression on 'X' and 'Y'. Divide it into `active_feature_weights' and `proactive_feature_weights'
	"""
		(2) To Do:
		Instead of linear regression, kernel regression?

		sklearn.svm.LinearSVR
		sklearn.svm.SVR(kernel='linear')
	"""	
	#clf = linear_model.LinearRegression()
	clf = svm.SVR(kernel='linear')
	clf.fit( delta_feature_matrix, delta_accuracies )
	#weights = clf.coef_   # for LinearRegression()
	weights = clf.coef_[0] # for SVR

	active_feature_weights = weights[0:len(active_features)]
	proactive_feature_weights = weights[len(active_features):]

	# For debugging purpose, save the delta feature matrix to see if we can actually fit it
	save_pickle(delta_feature_matrix, './temp_out/feature_matrix.pkl')
	save_pickle(delta_accuracies, './temp_out/accuracies.pkl')

	print ''
	print delta_feature_matrix[0:5,:]
	print delta_accuracies[0:5]
	print 'Lenth Extra Indices Pool: %d' % len(extra_indices_pool)
	print 'Sizes of cluster: %s, # clusters: %d, minimum cluster size: %d' % (str(sizes_of_clusters), len(sizes_of_clusters), min_cluster_size)
	print 'Weights: %s ' % weights
	print 'R score: %.3f' % clf.score(delta_feature_matrix, delta_accuracies)
	return active_feature_weights, proactive_feature_weights, clf


def dpal_update_bw1( dataset, model, annotators, active_features, active_features_params, proactive_features, proactive_features_params, dynamic_params ):

	# 1. Choose main_percentage := 50%, and |M|=15 clusters for the remaining indices,
	#    and build models for each

	test_indices = dataset.test_indices
	labeled_indices = dataset.train_labeled_indices


	num_extra = 150
	# at least 10 in the main samples group
	M = min( int( len(labeled_indices) - 10), num_extra)

	# Build the main model
	n_main_samples = len(labeled_indices) - M
	main_indices = labeled_indices[0:n_main_samples]      # Takes the first main_percentage indices

	parent_model_params = model.params
	main_model = ScikitModel( params=parent_model_params )
	main_model.train( dataset.X[main_indices], dataset.Ysingle[main_indices] )

	# Build extra tuned models
	extra_indices_pool = labeled_indices[n_main_samples:] # Takes the rest after main_percentage indices as pool

	# matrix of utility values for each potential point in the extra_indices_pool
	extra_indices_utility = np.zeros(shape=[len(extra_indices_pool), len(active_features)+len(proactive_features)])

	# Compute the "active features" utility for each of the extra indices pool
	for i in range(len(active_features)):
			# For each active feature
			active_feature = active_features[i]
			active_feature_params = active_features_params[i]
			active_feature_params['get_utilities_of'] = { 'custom' : extra_indices_pool }

			# Obtain feature value vector for each feature
			each_active_feature_value_vector_main, new_feature_params = active_feature( dataset, main_model, annotators, active_feature_params )

			# Obtain the delta sum, and append it to avg_active_feature_vector
			extra_indices_utility[:,i] =each_active_feature_value_vector_main

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################
			# Update active_feature_params in case we have an updated params
			active_features_params[i] = new_feature_params


	# Compute the "proactive features" utilities
	for i in range(len(proactive_features)):
			# For each active feature
			active_feature = active_features[i]
			active_feature_params = active_features_params[i]
			active_feature_params['get_utilities_of'] = { 'custom' : extra_indices_pool }

			# Obtain feature value vector for each feature
			each_proactive_feature_value_vector_main, new_feature_params = active_feature( dataset, main_model, annotators, active_feature_params )

			# Obtain the delta sum, and append it to avg_active_feature_vector
			extra_indices_utility[:,i+len(active_features)] =each_proactive_feature_value_vector_main

			#######################################################################
			### Debug if there's any insanity i.r.t updating new_feature_params ###
			#######################################################################
			# Update active_feature_params in case we have an updated params
			proactive_features_params[i] = new_feature_params


	list_extra_models = []
	list_extra_indices = []
	list_extra_indices_poolindex = []

	# Perform K-means clustering
	# labels    : (n x 1)
	clusterer = MiniBatchKMeans(n_clusters=M/2, init='k-means++', n_init=1, init_size=1000, batch_size=1000)
	#clusterer = AffinityPropagation()
	#clusterer = AgglomerativeClustering(n_clusters=M)
	#min_samples = max(1, int( len(labeled_indices) * (1-main_percentage) ) / M)
	#clusterer = DBSCAN(min_samples=min_samples)

	clusterer.fit( extra_indices_utility )
	#clusterer.fit( dataset.X[extra_indices_pool].toarray() )
	labels = clusterer.labels_
	M = len(np.unique( labels ))

	sizes_of_clusters = [ len(np.where( labels == label )[0] ) for label in np.unique( labels ) ]
	min_cluster_size = min(sizes_of_clusters)

	for label in np.unique( labels ):
		# Get indices of each cluster
		extra_indices = extra_indices_pool[ np.where( labels == label ) ]

		# Append it to the main indices
		indices = np.append( main_indices, extra_indices )

		######################################################################
		### The parent might not be a <ScikitModel>. For now it is assumed ###
		######################################################################
		extra_model = ScikitModel( params=parent_model_params )
		extra_model.train( dataset.X[indices], dataset.Ysingle[indices] )
		list_extra_models.append( extra_model )
		list_extra_indices.append( extra_indices )
		list_extra_indices_poolindex.append( np.where(labels==label)[0] )


	# For each extra model, the predictors are the mean of each utility value, and the response is the change in
	# accuracy of the model
	num_rows = len(list_extra_models)
	num_cols = len(active_features) + len(proactive_features)

	delta_feature_matrix = np.empty( (num_rows, num_cols) )
	delta_accuracies = np.empty( num_rows )

	for k in range(len(list_extra_models)):
		# 2.1 Calculate the sum of utilities over extra_indices to the main model. These are 'X' (\psi(X, K, L))
		extra_model   = list_extra_models[k]
		extra_indices = list_extra_indices[k]
		extra_indices_poolindex = list_extra_indices_poolindex[k]

		# 2-2. Calculate the difference in overall accuracy over dataset.test_indices between two models. These are 'Y' (\delta accuracy)
		accuracy_extra = extra_model.test( dataset.X[test_indices], dataset.Z[test_indices] )
		accuracy_main  = main_model.test(  dataset.X[test_indices], dataset.Z[test_indices] )
		delta_accuracy = (accuracy_extra - accuracy_main)

		# Append the results
		delta_feature_matrix[k, :] = extra_indices_utility[extra_indices_poolindex, :].mean(axis=0, keepdims=True)
		delta_accuracies[k] = delta_accuracy * 100

	clf = linear_model.LinearRegression()
	clf.fit( delta_feature_matrix, delta_accuracies )
	weights = clf.coef_   # for LinearRegression()

	weights = weights/sum(weights)
	active_feature_weights = weights[0:len(active_features)]
	proactive_feature_weights = weights[len(active_features):]

	# For debugging purpose, save the delta feature matrix to see if we can actually fit it
	save_pickle(delta_feature_matrix, './temp_out/feature_matrix.pkl')
	save_pickle(delta_accuracies, './temp_out/accuracies.pkl')

	print 'Sizes of cluster: %s, # clusters: %d, minimum cluster size: %d' % (str(sizes_of_clusters), len(sizes_of_clusters), min_cluster_size)
	print 'Weights: %s ' % weights
	print 'R score: %.3f' % clf.score(delta_feature_matrix, delta_accuracies)
	return active_feature_weights, proactive_feature_weights, clf

