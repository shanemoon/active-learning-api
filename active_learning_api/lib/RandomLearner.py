__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		A child class for <BaseLearner>.

		The <RandomLearner> randomly chooses the queries, without
		leveraging any prior knowledge of the current 
		class posterior distribution, etc.
"""

from BaseLearner import BaseLearner
import numpy as np

class RandomLearner(BaseLearner):

	def __init__(self, name="Random Learner", params={}):
		BaseLearner.__init__(self, name, params)


	def __str__(self):
		return "%s with params: %s" % (self.name, str(self.params))


	def choose_queries(self, dataset, model, annotators, n_queries=1, info={}):
		"""
			Return indices of the dataset that need to be 
			queried to annotators.

			We randomly choose queries.
		"""
		indices_to_query = np.random.choice(dataset.train_unlabeled_indices,
		                                    n_queries, replace=False)

		# We also randomly choose an annotator.
		###### This can be changed later ###### 
		annotator_index = np.random.choice( range(len(annotators)) )

		# Return : a dictionary { annotator_index : [index_to_query, ...], ... }		
		return { annotator_index : indices_to_query }