__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description:
		A wrapper class for a dataset.
		It stores the filename from which the actual data can be loaded, and
		its dataset (nick)name for convinience.

		It also tracks the instances that are labeled so far as well as their
		annotators. 

		Note that Dataset itself doesn't have ability to be annotated by itself.
		It always needs to be annotated by an annotator, usually ordered by a AL Worker.
"""

import numpy as np
import pandas as pd
from sklearn import cross_validation
import utils
from scipy.stats.mstats import mode


class Dataset:

	def __init__(self, filename, n_folds, dataset_name="", downsize=-1):
		self.filename = filename
		self.downsize = downsize

		if dataset_name == "":
			self.dataset_name = filename
		else:
			self.dataset_name = dataset_name

		# Load the dataset, set Xs and Ys
		self.load_data()
		self.initialize_fold(n_folds, i_fold=0, rand_seed=0)

	def __str__(self):
		return "<%s>" % self.dataset_name


	def load_data(self):
		"""
			Load the dataset accordingly.
			At the end of this operation, we get:

			X (data)                : A (n x p) matrix
			Y (annotated target)    : DataFrame holding annotations
			categories              : A (1 x |C|) matrix 
			                          where C is a set of categories.
			Z (ground-truth target) : A (n x 1) matrix (if available)
		"""
		data = utils.load_pickle( self.filename )

		if self.downsize != -1:
			# Downsize the dataset
			downsize_indices = np.arange(data.Z.shape[0])
			np.random.shuffle( downsize_indices )
			downsize_indices = downsize_indices[:self.downsize]

			# Initialize each array
			self.X = data.X[downsize_indices]
			self.Z = data.Z[downsize_indices]
			self.categories = data.categories

		else:
			# Initialize each array
			self.X = data.X
			self.Z = data.Z
			self.categories = data.categories

		# Y starts as an empty dictionary. Add elements keyed by annotator later.
		self.Y = pd.DataFrame(index=range(0,self.X.shape[0]))

		# Keeps the simplified version of the annotations around.
		self.Ysingle = np.empty(self.Z.shape)
		self.Ysingle.fill(np.nan)

		self.kfs = {}

	def add_annotations(self, annotator, indices_to_query, new_Y):
		self.Y[annotator.name].iloc[ indices_to_query ] = new_Y

		# Update the labeled indices list first
		self.train_labeled_indices = np.append( self.train_labeled_indices, indices_to_query )

		# Update the single label version
		self.update_Y_single(indices_to_query)

		# Update the unlabeled indices list.
		# This is basically:  train_unlabeled_indices = set(X) - set(test_indices) - set(labeled)
		self.train_unlabeled_indices = np.delete( np.arange( len(self.Ysingle) ),
		                                                  np.append( self.test_indices, self.train_labeled_indices ) )
		


	def update_Y_single(self, newindices):

		def getmode(s):
			s = s.values
			s = s[~np.isnan(s)]
			if len(s)<1:
				return np.nan
			return mode(s)[0][0]


		self.Ysingle[newindices] =  self.Y.iloc[newindices].apply(getmode, axis=1)


	def add_annotators(self, annotators):
		for ann in annotators:
			self.Y[ann.name] = np.nan

	def unload_data(self):
		"""
			Unload the data (X & Z).
			This is mainly to reduce the size of the <Dataset> object
			when it (the annotations info) needs to be stored.
		"""
		#####################################
		######    To be implemented    ######
		#####################################		
		pass


	def initialize_fold(self, n_folds, i_fold=0, rand_seed=0):
		"""
			Divide the dataset into train / test folds.
			If we already have initialized the folds, 
			we simply obtain the fold at `i_fold` index.
			KFold module from scikit-learn: More info at goo.gl/77Kf4e
		"""

		if rand_seed not in self.kfs:
			# If we have not seen this rand_seed with this dataset, we create a KFold object.
			kf = cross_validation.KFold(self.X.shape[0], n_folds=n_folds, shuffle=True, random_state=rand_seed)
			kf = [ (train_indices, test_indices) for train_indices, test_indices in kf ]

			# Save this K_Fold at this rand_seed to a dictionary
			self.kfs[rand_seed] = kf

		# Set the test and train indices of this fold
		self.train_indices, self.test_indices = self.kfs[rand_seed][i_fold]

		# Initially, there are no labeled instances
		# NOTE this keeps track of whether ANY annotator has labeled a point.
		self.train_labeled_indices = np.array([], dtype=int)
		self.train_unlabeled_indices = np.array(self.train_indices, copy=True)  


	def get_labeled_instances(self):
		return {'X': self.X[ self.train_labeled_indices ],
		        'Y': self.Ysingle[ self.train_labeled_indices ]}


	def get_unlabeled_instances(self):
		#TODO why is Y returned here at all? It is necessarily empty.
		return {'X': self.X[ self.train_unlabeled_indices ],
		        'Y': self.Ysingle[ self.train_unlabeled_indices ]}


	def get_labeled_instances_by(self, annotators=[]):
		#####################################
		######    To be implemented    ######
		#####################################
		return


	def get_unlabeled_instances_by(self, annotators=[]):
		#####################################
		######    To be implemented    ######
		#####################################		
		return


	def get_all_instances_info(self):
		#####################################
		######    To be implemented    ######
		#####################################		
		return


	def clear_annotations(self):
		#####################################
		######    To be implemented    ######
		#####################################		
		pass

