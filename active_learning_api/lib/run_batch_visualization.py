from Simulator import *
from Visualizer import Visualizer
import utility_functions 
import os

# Batch Experiment Configuration
dataset_name = 'satimage'
output_folder = '../results/visualizations/%s' % dataset_name
init_exp_config = 'entropy' # 'random', 'unknownness', 'entropy' or 'dpal'
do_PCA = True
draw_in_utility_space = False # if True, skip decision drawing boundary

# ['Entropy', 'Density', 'Unknownness', 'Even_Cluster', 'Entropy+Density', 'Pro_Answer', 'DPAL (US+DENS+UKN+PRO_P)']
experiment_types_to_run = [0,1,2,3,4,5,6] # [0, 1, 2, 3, 4, 5, 6]

os.system('mkdir %s' % output_folder)


if dataset_name == 'synth3':
	total_budget = 300
	time_snap_budgets = [240, 120, 80, 40, 20, 10]
	num_measure_points = 300
	num_utility_points = -1
	init_percent_labeled = 0.005
	n_queries_per_batch = 5
	exp_model_params = { 'clf_type' : 'SVC' }
	n_update_every_recurring = 20
	density_est_type = 0
	unknownness_est_type = 0
	num_clusters_for_even_cluster_sampling = 6
	n_domain_experts = 3
	annotator_noise_rate = 0.4
	p_ans_sigma = 2.0

elif dataset_name == 'iris':
	total_budget = 100
	time_snap_budgets = [80, 60, 50, 40, 20]
	num_measure_points = 25
	num_utility_points = -1
	init_percent_labeled = 0.02
	n_queries_per_batch = 5
	exp_model_params = { 'clf_type' : 'SVC' }
	n_update_every_recurring = 20
	density_est_type = 0
	unknownness_est_type = 0
	num_clusters_for_even_cluster_sampling = 6
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.5

elif dataset_name == '20newsgroups_norm':
	total_budget = 2500
	time_snap_budgets = [2000, 1000, 500, 250]
	num_measure_points = 300
	num_utility_points = 300
	init_percent_labeled = 0.01
	n_queries_per_batch = 25
	exp_model_params = { 'clf_type' : 'NB' }
	n_update_every_recurring = 250
	density_est_type = 1
	unknownness_est_type = 1
	num_clusters_for_even_cluster_sampling = 20
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.5

elif dataset_name == '4newsgroups_mini':
	total_budget = 1500
	time_snap_budgets = [1000, 500, 250]
	num_measure_points = 300
	num_utility_points = 300
	init_percent_labeled = 0.01
	n_queries_per_batch = 25
	exp_model_params = { 'clf_type' : 'NB' }
	n_update_every_recurring = 250
	density_est_type = 1
	unknownness_est_type = 1
	num_clusters_for_even_cluster_sampling = 8
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.5

elif dataset_name == 'new_mal_4_norm':
	total_budget = 500
	time_snap_budgets = [300, 50]
	num_measure_points = 50
	num_utility_points = 50	
	init_percent_labeled = 0.01
	n_queries_per_batch = 500
	exp_model_params = { 'clf_type' : 'LR' }
	n_update_every_recurring = 200
	density_est_type = 0
	unknownness_est_type = 0
	num_clusters_for_even_cluster_sampling = 4
	n_domain_experts = 4
	annotator_noise_rate = 0.4
	p_ans_sigma = 0.5

elif dataset_name == 'satimage':
	total_budget = 1100
	time_snap_budgets = [1000, 800, 600, 400, 200]
	num_measure_points = 250
	num_utility_points = 250	
	init_percent_labeled = 0.005
	#n_queries_per_batch = 20
	n_queries_per_batch = 1100
	exp_model_params = { 'clf_type' : 'LR' }
	n_update_every_recurring = 200
	density_est_type = 1
	unknownness_est_type = 1
	num_clusters_for_even_cluster_sampling = 4	
	n_domain_experts = 3
	annotator_noise_rate = 0.2
	p_ans_sigma = 0.2



list_active_features = [ [utility_functions.entropy],
						 [utility_functions.density],
						 [utility_functions.unknownness], 
						 [utility_functions.even_cluster], 
						 [utility_functions.entropy, utility_functions.density],
						 [],
						 [utility_functions.entropy, utility_functions.density, utility_functions.unknownness] ]	
list_active_features_params = [ [{}],
								[{'density_est_type' : density_est_type}],
								[{'unknownness_est_type' : unknownness_est_type}], 
								[{'num_clusters' : num_clusters_for_even_cluster_sampling }],
								[{'normalize' : True}, { 'normalize' : True, 'density_est_type' : density_est_type}],
								[],
								[{'normalize' : True}, { 'normalize' : True, 'density_est_type' : density_est_type}, {'normalize' : True, 'unknownness_est_type' : 0}] ]
list_proactive_features = [ [],
							[],
							[],
							[],
							[],
							[utility_functions.pro_p_answer],
							[utility_functions.pro_p_answer] ]
list_proactive_features_params = [ [],
								   [],
								   [],
								   [],
								   [],
								   [{'p_ans_est_type' : 1, 'p_ans_sigma' : p_ans_sigma}],
								   [{'p_ans_est_type' : 1, 'p_ans_sigma' : p_ans_sigma}] ]
list_dynamic_params = [{},
					   {},
					   {},
					   {},
					   {},
					   {},
					   { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
	                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
	                     'extra_model_group_space' : 0, 'regression_method' : 'svr_kernel',
                         'start_conditions' : [utility_functions.dpal_update_test_accuracy],
	                     'start_conditions_params' : [ {'threshold_accuracy' : 0.5} ], 
	                     'update_conditions' : [utility_functions.dpal_update_always ],
	                     'update_conditions_params' : [ {} ] }]

output_filenames = ['us.eps', 'dens.eps', 'ukn.eps', 'even.eps', 'us+dens.eps', 'pro_p.eps', 'dpal_us+dens+ukn+prop_p.eps']
titles = ['Entropy', 'Density', 'Unknownness', 'Even_Cluster', 'Entropy+Density', 'Pro_Answer', 'DPAL (US+DENS+UKN+PRO_P)']


# Starting experiment setup
learner_type    = 1
if init_exp_config == 'random':
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.random_value ],
		'active_features_params' : [ {} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : { },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}

elif init_exp_config == 'dpal':
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy, utility_functions.density, utility_functions.unknownness ],
		'active_features_params' : [ {'normalize' : True}, {'density_est_type': density_est_type, 'normalize' : True}, {'unknownness_est_type': unknownness_est_type, 'normalize' : True}  ],
		'proactive_features' : [ utility_functions.pro_p_answer ],
		'proactive_features_params' : [ {'p_ans_est_type' : 1, 'p_ans_sigma' : p_ans_sigma} ],
		'dynamic_params' : { 'weight_update_method' : utility_functions.dpal_update, 'main_percentage' : 0.8,
		                     'M' : 20, 'choose_M_method' : 0, 'extra_model_group_method' : 1,
		                     'extra_model_group_space' : 0, 'regression_method' : 'svr_kernel',
		                     'start_conditions' : [utility_functions.dpal_update_test_accuracy],
		                     'start_conditions_params' : [ {'threshold_accuracy' : 0.55} ], 
		                     'update_conditions' : [utility_functions.dpal_update_every_recurring],
		                     'update_conditions_params' : [ {'n_update_every_recurring' : n_update_every_recurring } ] },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
elif init_exp_config == 'unknownness':
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.unknownness ],
		'active_features_params' : [ {'unknownness_est_type': unknownness_est_type} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : { },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}
elif init_exp_config == 'entropy':
	learner_params  = { 'utility_params' : {
		'b_log_weight' : False,
		'active_features' : [ utility_functions.entropy ],
		'active_features_params' : [ {} ],
		'proactive_features' : [  ],
		'proactive_features_params' : [  ],
		'dynamic_params' : { },
		'utility_sample' : 0,
		'annotator_selection' : 0
	 }
	}	
else:
	print "Initial experiment configuration is not recognized."
	pass

model_type       = 0
model_params     = exp_model_params
d_annotators     = { 'auto' : ('build_k_domain_experts', n_domain_experts, {'noise_rate' : annotator_noise_rate}) }
dataset_name     = dataset_name
downsize         = -1
budget           = total_budget
percent_labeled  = init_percent_labeled
n_queries        = n_queries_per_batch
n_folds          = 5
n_repeat_iterations = 1
return_after_exp = { 'workers', 'fold_costs', 'fold_error_rates' }

exp_params = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
                                 downsize, learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds, n_repeat_iterations, return_after_exp )

# Initialize a Visualizer and Simulator
exp_results = Simulator().run( exp_params )
worker = exp_results['workers'][0]
visualizer = Visualizer( worker, output_folder, do_PCA, draw_in_utility_space )

# Draw the overall graph
Drawer().draw( os.path.join(output_folder, 'curve.eps') , {
	'test' : {'X': [exp_results['fold_costs'][0]], 'Y': [exp_results['fold_error_rates'][0]]}
	}, 'Cost', 'Error Rate', '', False)

# Run experiment for each budget
for budget in time_snap_budgets:
	# Load a snapshot at each budget point
	visualizer.time_snap( budget )
	test_accuracy = visualizer.worker.test()

	# Run experiments
	for i in experiment_types_to_run:
		# (1). Decision Boundaries
		visualizer.draw_decision_boundaries( output_filename='', title='', show_labeled_data=False, show_unlabeled_data=False, hold=True )

		# Info about this experiment
		active_features = list_active_features[i]
		active_features_params = list_active_features_params[i]
		proactive_features = list_proactive_features[i]
		proactive_features_params = list_proactive_features_params[i]
		dynamic_params = list_dynamic_params[i]
		title = 'Budget: %d, Accuracy: %.2f, AL Criteria: %s' % (budget, test_accuracy, titles[i])
		output_filename = 'b_%d_p_%d_u_%d_%s' % (budget, do_PCA, draw_in_utility_space, output_filenames[i])

		utility_params  = { 
			'unlabeld_only' : True,
			'b_log_weight' : False,
			'active_features' : active_features,
			'active_features_params' : active_features_params,
			'proactive_features' : proactive_features,
			'proactive_features_params' : proactive_features_params,
			'dynamic_params' : dynamic_params,
			'utility_sample' : 0,
			'annotator_selection' : 0
		}
		visualizer.draw_utility_values( utility_params, num_utility_points=num_utility_points, output_filename=output_filename, title=title, hold=False )

	# (3) - 1. Plot heatmap of actual improvements (in accuracy)
	title = 'Budget: %d, Accuracy: %.2f, Actual +Accuracy' % (budget, test_accuracy)
	output_filename = 'b_%d_p_%d_u_%d_actual_improvement_acc.eps' % (budget, do_PCA, draw_in_utility_space)
	visualizer.draw_decision_boundaries( output_filename='', title='', show_labeled_data=False, show_unlabeled_data=False, hold=True )	
	visualizer.draw_active_improvement( num_measure_points=num_measure_points, type_measure_points=1, improvement_measure=0, utility_params=learner_params['utility_params'], output_filename=output_filename, title=title, hold=False )

	# (3) - 2. Plot heatmap of actual improvements (in accuracy)
	title = 'Budget: %d, Accuracy: %.2f, Actual -Avg Entropy' % (budget, test_accuracy)
	output_filename = 'b_%d_p_%d_u_%d_actual_improvement_ent.eps' % (budget, do_PCA, draw_in_utility_space)
	visualizer.draw_decision_boundaries( output_filename='', title='', show_labeled_data=False, show_unlabeled_data=False, hold=True )	
	visualizer.draw_active_improvement( num_measure_points=num_measure_points, type_measure_points=1, improvement_measure=1, utility_params=learner_params['utility_params'], output_filename=output_filename, title=title, hold=False )

