execfile("sim/sim_core.py")

#  A basic simulation


if __name__ == '__main__':
	#DEFINE YOUR SIMULATION
	learner_type    = 1
	learner_params  = { 'utility_params' : {
		'unlabeld_only' : True,
		'active_features' : [ utility_functions.density, utility_functions.entropy],
		'active_features_params' : [ {'density_est_type' : 1} , {}],
		'proactive_features' : [],
		'proactive_features_params' : [],
		'dynamic_params' : {},
		'utility_sample' : 0
	 }
	}
	model_type      = 0
	model_params    = { 'clf_type' : 'NB' }
	d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
	dataset_name    = '20newsgroups_norm'
	budget          = 1500
	percent_labeled = 0.01
	n_queries     = 25
	n_folds         = 5

	exp_params = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
	                                 learner_type, learner_params, model_type, model_params, d_annotators,
	                                 dataset_name, budget, percent_labeled, n_queries, n_folds )


	# Make sure that we have all the necessary folders
	make_folders()
    # Run simulation
	fold_error_rates, fold_costs = Simulator().run( exp_params )
	# Draw the graph
	output_filename = './../results/graphs/sim_0.eps'
	Drawer().draw(output_filename, {
		'test' : {'X':fold_costs, 'Y':fold_error_rates}
		}, 'Cost', 'Error Rate', '', False)
