execfile("sim/sim_core.py")
make_folders()

# A comparison of several methods inspired by Bronwyn

# WARNING: this is currently set up to be run in the interactive terminal.  To
#   make it batch-ready, substitute the following code section as needed:
#        exp_params = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
#                                  learner_type, learner_params, model_type, model_params, d_annotators,
#                                  dataset_name, budget, percent_labeled, n_queries, n_folds )

############################################################################
#BW
#update_bw1, sample=1
    #BW model
learner_type    = 1
learner_params  = { 'utility_params' : {
	'unlabeld_only' : True,
	'active_features' : [ utility_functions.density, utility_functions.entropy],
	'active_features_params' : [ {'density_est_type' : 1} , {}],
	'proactive_features' : [],
	'proactive_features_params' : [],
	'dynamic_params' : {'weight_update_method' : utility_functions.dpal_update_bw1},
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 1500
percent_labeled = 0.01
n_queries       = 25
n_folds         = 5

__file__ = os.getcwd()
exp_params = generate_exp_params(os.path.join(__file__, 'datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

fold_error_rates_bw, fold_costs_bw = Simulator().run( exp_params )
save_pickle(fold_error_rates_bw, "./../results/fold_error_rates_bw")
save_pickle(fold_costs_bw, "./../results/fold_costs_bw")


###########################################################################
# BWnonorm:  Ignoring this method ... running nonorm seems to require tweaking the code

###########################################################################
# Baseline: no dynamic weight update, sample=0
learner_type    = 1
learner_params  = { 'utility_params' : {
    'unlabeld_only' : True,
	'active_features' : [ utility_functions.density, utility_functions.entropy],
	'active_features_params' : [ {'density_est_type' : 1} , {}],
	'proactive_features' : [],
	'proactive_features_params' : [],
	'dynamic_params' : {},
	'utility_sample' : 0
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 1500
percent_labeled = 0.01
n_queries       = 25
n_folds         = 5

__file__ = os.getcwd()
exp_params = generate_exp_params(os.path.join(__file__, 'datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )
                                 
# exp_params = generate_exp_params(os.path.join(os.path.dirname(__file__), '../datasets/%s/data.pkl' % dataset_name),
#                          learner_type, learner_params, model_type, model_params, d_annotators,
#                          dataset_name, budget, percent_labeled, n_queries, n_folds )

fold_error_rates_baseline, fold_costs_baseline = Simulator().run( exp_params )
save_pickle(fold_error_rates_baseline, "./../results/fold_error_rates_baseline")
save_pickle(fold_costs_baseline, "./../results/fold_costs_baseline")


############################################################################
#Shane 3
#sample=0
learner_type    = 1
learner_params  = { 'utility_params' : {
    'unlabeld_only' : True,
	'active_features' : [ utility_functions.density, utility_functions.entropy],
	'active_features_params' : [ {'density_est_type' : 1} , {}],
	'proactive_features' : [],
	'proactive_features_params' : [],
	'dynamic_params' : {'weight_update_method' : utility_functions.dpal_update_3},
	'utility_sample' : 0
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 1500
percent_labeled = 0.01
n_queries       = 25
n_folds         = 5

__file__ = os.getcwd()
exp_params = generate_exp_params(os.path.join(__file__, 'datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

fold_error_rates_shane3, fold_costs_shane3 = Simulator().run( exp_params )
save_pickle(fold_error_rates_shane3, "./../results/fold_error_rates_shane3")
save_pickle(fold_costs_shane3, "./../results/fold_costs_shane3")


############################################################################
#Shane 3 sample
#sample=1
learner_type    = 1
learner_params  = { 'utility_params' : {
    'unlabeld_only' : True,
	'active_features' : [ utility_functions.density, utility_functions.entropy],
	'active_features_params' : [ {'density_est_type' : 1} , {}],
	'proactive_features' : [],
	'proactive_features_params' : [],
	'dynamic_params' : {'weight_update_method' : utility_functions.dpal_update_3},
	'utility_sample' : 1
 }
}
model_type      = 0
model_params    = { 'clf_type' : 'NB' }
#d_annotators    = {}
d_annotators    = { 'auto' : ('build_k_domain_experts', 4, {'noise_rate' : 0.2}) }
dataset_name    = '20newsgroups_norm'
budget          = 1500
percent_labeled = 0.01
n_queries       = 25
n_folds         = 5

__file__ = os.getcwd()
exp_params = generate_exp_params(os.path.join(__file__, 'datasets/%s/data.pkl' % dataset_name),
                                 learner_type, learner_params, model_type, model_params, d_annotators,
                                 dataset_name, budget, percent_labeled, n_queries, n_folds )

fold_error_rates_shane3sample, fold_costs_shane3sample = Simulator().run( exp_params )
save_pickle(fold_error_rates_shane3sample, "./../results/fold_error_rates_shane3sample")
save_pickle(fold_costs_shane3sample, "./../results/fold_costs_shane3sample")





############################################################################
##### LOAD ALL RESULTS AND DRAW THE GRAPH
############################################################################
fold_error_rates_bw = load_pickle( "./../results/fold_error_rates_bw")
fold_costs_bw = load_pickle("./../results/fold_costs_bw")

fold_error_rates_baseline = load_pickle( "./../results/fold_error_rates_baseline")
fold_costs_baseline = load_pickle("./../results/fold_costs_baseline")

fold_error_rates_shane3 = load_pickle( "./../results/fold_error_rates_shane3")
fold_costs_shane3 = load_pickle("./../results/fold_costs_shane3")

fold_error_rates_shane3sample = load_pickle( "./../results/fold_error_rates_shane3sample")
fold_costs_shane3sample = load_pickle("./../results/fold_costs_shane3sample")

#Draw the graph
output_filename = './../results/graphs/sim_1.eps'
Drawer().draw(output_filename, {
	'BW' : {'X':fold_costs_bw, 'Y':fold_error_rates_bw},
	'Baseline' : {'X':fold_costs_baseline, 'Y':fold_error_rates_baseline},
	'Shane3' : {'X':fold_costs_shane3, 'Y':fold_error_rates_shane3},
	'Shane3sample' : {'X':fold_costs_shane3sample, 'Y':fold_error_rates_shane3sample}
	}, 'Cost', 'Error Rate', '', False)


#     """
# 		< Parameters > 
# 		'learner_type'    : type of <Learner> that defines the AL algorithm
# 		                    0 - <RandomLearner>
# 		                    1 - <MaxUtilityLearner>
# 		'learner_params'  : `params` passed to a <Learner> instance.
# 		                    'utility_params' : {
# 		                        'unlabeld_only' : (Boolean) to indicate if we are only considering utilities of unlabeled examples
# 		                        'active_features' : <List> of handles to feature methods (defined below)
# 		                        'active_features_params' : <List> of feature params in the same order as in 'active_features' {
# 		                            'density_est_type' : for utility functions that require density estimation
# 		                              0 - Kernel Density Estimation
# 		                              1 - K-means Reverse Count
# 		                        }
# 		                        'proactive_features' : <List> of handles to proactive feature methods (defined below) that requires per annotator value matrices
# 		                        'proactive_features_params' : <List> of feature params in the same order as in 'proactive_features'
# 		                        'dynamic' : {
# 		                            # Params related to dynamic proactive learning (DPAL)
# 		                        }
# 		                        'utility_sample' 0 = take max utility, 1 = sample from utility distribution
# 		                    }
# 		'model_type'      : type of <Model> that defines the classifier
# 		                    0 - <ScikitModel>
# 		'model_params'    : `params` passed to a <Model> instance.
# 		                    'clf_type' : (e.g.) 'LR', 'SVM', 'NB' etc.
# 		'd_annotators'    : a dictionary for ( 'auto' : { type, num_annotators }, 'manual' : { 'name' : [ annotator_id, cost, type, params ] } )
# 		'dataset_name'    : name of the dataset
# 		'budget'          : total budget assigned for a single fold task
# 		'percent_labeled' : percentage of initially labeled instances
# 		'n_queries'       : # of queries that will be made at each iteration
# 	    'n_folds'         : # of folds that will be performed for cross validation
# 	"""

