import sys
import os
sys.path.insert(0, os.path.join(os.getcwd(), 'lib'))

from ALWorker import ALWorker
from ProgressBar import AnimatedProgressBar
from Drawer import Drawer
from AnnotatorBuilder import AnnotatorBuilder
import os
import utility_functions
from utils import save_pickle, load_pickle
import numpy as np

class Simulator:
	
	def __init__(self):	
		pass


	def run(self, exp_params):
		"""
			Run an active learning simulation given an experimental setup.
			Ultimately, this function returns a prediction report for this experiment

			- exp_params: a dictionary that contains the setup, keys of which are as follows:

				'learner_type'    : type of <Learner> that defines the AL algorithm
				                    0 - <RandomLearner>
				                    1 - <MaxUtilityLearner>
				'model_type'      : type of <Model> that defines the classifier
				                    0 - <ScikitModel>
				'model_params'    : `params` passed to a <Model> instance.
				                    'clf_type' : (e.g.) 'LR', 'SVM', etc.				                    
				'd_annotators'    : a dictionary for ( 'auto'   : { type, num_annotators }, 
					                                   'manual' : { 'name' : [ annotator_id, cost, type, params ] } )
				'dataset_name'    : name of the dataset
				'budget'          : total budget assigned for a single fold task
				'percent_labeled' : percentage of initially labeled instances
				'n_queries'       : # of queries that will be made at each iteration
			    'n_folds'         : # of folds that will be performed for cross validation
		"""
		# 0. Load the input parameters
		path_dataset    = exp_params['path_dataset']
		learner_type    = exp_params['learner_type']
		learner_params  = exp_params['learner_params']
		model_type      = exp_params['model_type']
		model_params    = exp_params['model_params']
		d_annotators    = exp_params['d_annotators']
		dataset_name    = exp_params['dataset_name']
		budget          = exp_params['budget']
		percent_labeled = exp_params['percent_labeled']
		n_queries       = exp_params['n_queries']
		n_folds         = exp_params['n_folds']

		# Initialize a learner. A learner decides how to perform active learning
		# (e.g.) what to query, which annotators to choose, etc.
		learner = load_learner( learner_type, learner_params )

		fold_error_rates = []
		fold_costs = []

		for i_fold in range(n_folds):
			if i_fold >= 1:
				break
			print 'Starting Fold %d (out of %d) ...' % (i_fold, n_folds)

			# Create a new Dataset initialization for this fold
			dataset = load_dataset( path_dataset, n_folds, i_fold, dataset_name )

			# Initialize a model for this fold
			model = load_model( model_type, model_params )

			# Get annotators
			annotators = load_annotators( d_annotators, dataset )

			# Create an Active Learning Worker
			worker = ALWorker( learner, dataset, model, budget, annotators=annotators )

			# Randomly assign ground truth samples to start active learning with
			worker.initialize_rand_ground_truth( percent_labeled=percent_labeled, rand_seed=0 )

			# Check the initial performance.
			# Initialize `error_rates` and `cumulative_costs` which track the performance
			# at each iteration.
			accuracy =  worker.test()
			error_rates = [ 1 - accuracy ]
			cumulative_costs = [ 0 ]

			# Repeat quering until the budget runs out.
			# Start an animated progress bar.
			p = AnimatedProgressBar(end=budget, width=100)
			i = 0

			TERMINATE = False
			while (worker.budget > 0) and (TERMINATE == False):

				# Choose the best queries to make
				annotator_index_to_indices_to_query = worker.choose_queries( n_queries=n_queries )

				# For each annotator, make queries
				for annotator_index, indices_to_query in annotator_index_to_indices_to_query.items():
					annotator = annotators[annotator_index]
				
					# Get annotations for these queries by the chosen annotator
					(new_Y, response_type) = worker.ask( annotator, indices_to_query )

					# response_type 1: normal query, received an answer from the annotator
					if response_type == 1:
						# Update the model and the dataset record
						worker.update( indices_to_query, new_Y, annotator )

						# Get the accuracy at this iteration on the held-out test dataset.
						accuracy =  worker.test()
						error_rates.append( 1 - accuracy )

						# Update the cumulative cost so far
						cumulative_costs.append( budget - worker.budget )

						# Update the progress bar
						p.set_absolute_progress_and_iteration(budget - worker.budget, i)
						p.show_progress()

					elif response_type == 2:
						# We ran out of budget, thus terminate the process
						TERMINATE = True
					else:
						# Optionally, it could be because the annotator
						# simply refused to answer, due to insufficient confidence level, etc.
						# To accommodate this, we can have different return-types for worker.ask()
						#####################################
						######    To be implemented    ######
						#####################################
						pass
				i += 1

			# We are done with the loop
			print "End of the active learning process: Final error rate: %.3f" % (1 - accuracy)

			# Optionally printout the query history
			#worker.print_query_history()

			# Append the error_rates & costs result to fold_error_rates & fold_costs
			fold_error_rates.append( error_rates )
			fold_costs.append( cumulative_costs )

			# Clear annotation at every fold.
			dataset.clear_annotations()

		return fold_error_rates, fold_costs


def load_learner( learner_type, learner_params ):
	"""
		'learner_type' : type of <Learner> that defines the AL algorithm
		               0 - <RandomLearner>
		               1 - <MaxUtilityLearner>	
	"""
	if learner_type == 0:
		from RandomLearner import RandomLearner
		return RandomLearner( params=learner_params )

	elif learner_type == 1:
		from MaxUtilityLearner import MaxUtilityLearner
		return MaxUtilityLearner( params=learner_params )


def load_model( model_type, model_params ):
	"""
		'model_type' : type of <Model> that defines the classifier
		               0 - <ScikitModel>	
	"""
	if model_type == 0:
		from ScikitModel import ScikitModel
		return ScikitModel( params=model_params )


def load_dataset( path_dataset, n_folds, i_fold, dataset_name ):
	from Dataset import Dataset
	dataset = Dataset(filename=path_dataset, n_folds=n_folds, dataset_name=dataset_name)
	dataset.initialize_fold( n_folds, i_fold )
	return dataset


def load_annotators( d_annotators, dataset ):
	"""
		'd_annotators' : a dictionary for ( 'auto'   : { type, num_annotators }, 
			                                'manual' : { 'name' : [ annotator_id,, cost, type, params ] } )
		where 'params' : a dictionary that stores specific information
	"""
	return AnnotatorBuilder().build_experts_from_dictionary( d_annotators, dataset )


def generate_exp_params(path_dataset,
	                    learner_type, learner_params, model_type, model_params, d_annotators,
	                    dataset_name, budget, percent_labeled, n_queries, n_folds ):	
	exp_params = {}

	# Set Parameters
	exp_params['path_dataset']    = path_dataset
	exp_params['learner_type']    = learner_type
	exp_params['learner_params']  = learner_params
	exp_params['model_type']      = model_type
	exp_params['model_params']    = model_params
	exp_params['d_annotators']    = d_annotators
	exp_params['dataset_name']    = dataset_name
	exp_params['budget']          = budget
	exp_params['percent_labeled'] = percent_labeled
	exp_params['n_queries']       = n_queries
	exp_params['n_folds']         = n_folds

	return exp_params


def make_folders():
    path = os.getcwd()
    path_split = os.path.split(path)
    bottom_two_levels = os.path.join(os.path.split(path_split[0])[1],
                                     path_split[1])
    # the current working directory better be here or else the results folder
    #   goes somewhere strange
    assert(bottom_two_levels == 'active-learning-api/active_learning_api')
    os.system('mkdir ./temp_out/')
    os.system('mkdir ./../results/')
    os.system('mkdir ./../results/graphs')

