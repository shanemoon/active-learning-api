from django.shortcuts import render

from active_learning_api.models import Query, Answer, ALWorkerInterface
from active_learning_api.models import myServerManager

# REST Framework Modules
from active_learning_api.serializers import QuerySerializer, AnswerSerializer, ALWorkerInterfaceSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Library Modules
from active_learning_api.lib.Simulator import load_dataset, load_learner, load_model, load_annotators
from active_learning_api.lib.ALWorker import ALWorker
from active_learning_api.lib.utils import save_pickle, load_pickle, print_errors

# Miscellaneous
import numpy as np
import random
import os.path

# Index
class ALWorkerInterfaceList(APIView):
    """
    	List all <ALWorkerInterface>s, or create a new <ALWorkerInterface>.
    """
    def get(self, request, format=None):
        worker_interfaces = ALWorkerInterface.objects.all()
        serializer = ALWorkerInterfaceSerializer(worker_interfaces, many=True)

        # For each worker_interfaces, load or create a <Worker> instance,
        # and load them to myServerManager.workers (dictionary),
        # if their id's are not in the keys yet.
        for worker_interface in worker_interfaces:
            worker_id = worker_interface.id

            if worker_id not in myServerManager.workers:
                print 'Loading worker %d' % worker_id
                worker = load_pickle( './active_learning_api/cache/al_worker_id_%d.pkl' % worker_id  )
                myServerManager.workers[ worker_id ] = worker

        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = ALWorkerInterfaceSerializer(data=request.data)
        
        if serializer.is_valid():
            serializer.save()

            # A new <WorkerInterface> is created.
            # Create a new <Worker> instance as well, and save it to
            # the local WorkerStorage as a pickle.
            # Also add it to myServerManager.workers[ new_id ]
            n_folds         = serializer.data['n_folds']
            i_fold          = 0
            learner_type    = serializer.data['learner_type']
            learner_params  = eval( serializer.data['learner_params'] )
            dataset_name    = serializer.data['dataset_name']
            path_dataset    = get_path_dataset( dataset_name )
            model_type      = serializer.data['model_type']
            model_params    = {}
            n_annotators    = serializer.data['n_annotators']
            d_annotators    = { 'auto' : ('build_k_human_annotators', n_annotators, {}) }
            budget          = serializer.data['budget']
            percent_labeled = serializer.data['percent_labeled']
            worker_id       = serializer.data['id']

            # Initialize a learner. A learner decides how to perform active learning
            # (e.g.) what to query, which annotators to choose, etc.
            learner = load_learner( learner_type, learner_params )

            # Create a new Dataset initialization for this fold
            dataset = load_dataset( path_dataset, n_folds, i_fold, dataset_name )

            # Initialize a model for this fold
            model = load_model( model_type, model_params )

            # Get annotators
            annotators = load_annotators( d_annotators, dataset )

            # Create an Active Learning Worker
            worker = ALWorker( learner, dataset, model, budget, annotators=annotators )

            # Randomly assign ground truth samples to start active learning with
            worker.initialize_rand_ground_truth( percent_labeled=percent_labeled, rand_seed=0 )

            # Pickle it to a cache storage
            save_pickle( worker, './active_learning_api/cache/al_worker_id_%d.pkl' % worker_id )

            # Add a new worker to `myServerManager.workers'
            myServerManager.workers[ worker_id ] = worker
            print "=============================="
            print myServerManager.workers[ worker_id ]
            print "=============================="

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnswerList(APIView):
    """
    	List all <Answer>s, or create a new <Answer>.
    """
    def get(self, request, format=None):
        answers = Answer.objects.all()
        serializer = AnswerSerializer(answers, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = AnswerSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()            
            # Obtain worker_id, user_id, and query_id
            # Load the <Worker> instance using the worker_id.
            # Provide the <Worker> with user_id, query_id, and query_answer.
            # The <Worker> then updates the system properly.
            # Save the <Worker> into a pickle as well.
            
            # **** There might be a better way than pickling the <Worker> at every iteration
            # **** Maybe when we pickle a <Worker>, we unload all the expensive objects,
            # **** but only store the light-weighted variables such as indices, etc.
            # **** When we load a <Worker>, we run a method that populates other objects from
            # **** the basic information.
            worker_id = serializer.data['worker_id']
            worker = load_worker( worker_id )
            query_id = serializer.data['query_id']
            query_obj = Query.objects.get(pk=query_id)
            annotator_id = serializer.data['annotator_id']
            annotator = worker.get_annotator_by_id( annotator_id )
            #next_query = eval(query_obj.next_query)
            indices_to_query = np.array(eval(query_obj.indices_to_query))
            #data_to_label = np.array(eval(query_obj.data_to_label))
            new_Y = np.array(eval(serializer.data['query_answer']))
            worker.update( indices_to_query, new_Y, annotator )

            #################### Debug ####################
            print '======================================='
            print 'POST: <Answer>'
            print 'indices_to_query: %s' % indices_to_query
            print 'new_Y: %s' % new_Y
            print 'Test Accuracy: %.2f' % worker.test()
            print 'Number of labels so far: %d' % len(worker.dataset.train_labeled_indices)
            print '======================================='
            ###############################################

            # Pickle the updated worker to a cache storage
            save_pickle( worker, './active_learning_api/cache/al_worker_id_%d.pkl' % worker_id )

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class QueryList(APIView):
    """
    	List all <Query>s, or create a new <Query>.
    """
    def get(self, request, format=None):
        queries = Query.objects.all()
        serializer = QuerySerializer(queries, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        try:
            serializer = QuerySerializer(data=request.data)

            if serializer.is_valid():
                serializer.save()                
            	# Obtain worker_id, annotator_id, and n_queries
                # Load the <Worker> instance using the worker_id.
                # Provide the <Worker> with annotator_id, and query_id.
                # The <Worker> then runs the active learning algorithm,
                # and gives the annotator "next_query".
                # Update "next_query" to the <request.data> as well.
                # Return it.

                worker = load_worker( serializer.data['worker_id'] )
                n_queries = serializer.data['n_queries']
                annotator_id = serializer.data['annotator_id']

                info = {}
                if annotator_id != -1:
                    # Querier specified who will be annotating this time.
                    # Pass in this information to `worker.choose_queries() function`
                    annotator = worker.get_annotator_by_id( annotator_id )
                    info['annotator'] = annotator

                # Formulate the next query
                annotator_index_to_indices_to_query = worker.choose_queries( n_queries=n_queries, info=info )            
                
                if annotator_id in annotator_index_to_indices_to_query:
                    indices_to_query = annotator_index_to_indices_to_query[ annotator_id ]
                else:
                    ##############################
                    ##### NEED TO MODIFY HERE ####
                    ##############################
                    
                    # The best way is probably that worker.choose_queries takes an annotator id as an input.
                    # For now, we randomly choose someone else's assignment.
                    print 'Randomly choosing ...............'
                    indices_to_query = random.choice( annotator_index_to_indices_to_query.values() )

                # Include the following info in the response
                data_to_label = worker.dataset.X[ indices_to_query ]
                #next_query = { 'indices_to_query' : str(indices_to_query.tolist()), 'data_to_label' : str(data_to_label.tolist()) }
                test_accuracy = worker.test()
                labels_clf_estimation = worker.model.predict( data_to_label )
                labels_clf_confidence = worker.model.predict_proba( data_to_label )

                # Save `next_query` to DB, and return it
                data = dict( request.data.items() )
                data['indices_to_query'] = str(indices_to_query.tolist())
                data['data_to_label'] = str(data_to_label.tolist())
                data['labels_clf_estimation'] = str(labels_clf_estimation.tolist())
                data['labels_clf_confidence'] = str(labels_clf_confidence.tolist())

                data['annotator_id'] = annotator.id
                data['test_accuracy'] = test_accuracy
                serializer = QuerySerializer(data=data)

                #################### Debug ####################
                print '======================================='
                print 'POST: <Query>'
                print 'indices_to_query: %s' % indices_to_query
                print 'labels_clf_estimation: %s' % labels_clf_estimation
                print 'labels_clf_confidence: %s' % labels_clf_confidence
                print 'Test Accuracy: %.2f' % test_accuracy
                print 'Number of labels so far: %d' % len(worker.dataset.train_labeled_indices)
                print '======================================='
                ###############################################

                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
        except:
            print_errors()
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# Detail
class AnswerDetail(APIView):
    """
    	Retrieve, update or delete an <Answer> instance.
    """
    def get_object(self, pk):
        try:
            return Answer.objects.get(pk=pk)
        except Answer.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        answer = self.get_object(pk)
        serializer = AnswerSerializer(answer)

        return Response(serializer.data)

    def put(self, request, pk, format=None):
        answer = self.get_object(pk)
        serializer = AnswerSerializer(answer, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        answer = self.get_object(pk)
        answer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class QueryDetail(APIView):
    """
   		Retrieve, update or delete a <Query> instance.
    """
    def get_object(self, pk):
        try:
            return Query.objects.get(pk=pk)
        except Query.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        query = self.get_object(pk)
        serializer = QuerySerializer(query)

        return Response(serializer.data)

    def put(self, request, pk, format=None):
        query = self.get_object(pk)
        serializer = QuerySerializer(query, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        query = self.get_object(pk)
        query.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



class ALWorkerInterfaceDetail(APIView):
    """
    	Retrieve, update or delete a <ALWorkerInterface> instance.
    """
    def get_object(self, pk):
        try:
            return ALWorkerInterface.objects.get(pk=pk)
        except ALWorkerInterface.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        worker_interface = self.get_object(pk)
        serializer = ALWorkerInterfaceSerializer(worker_interface)

        return Response(serializer.data)

    def put(self, request, pk, format=None):
        worker_interface = self.get_object(pk)
        serializer = ALWorkerInterfaceSerializer(worker_interface, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        worker_interface = self.get_object(pk)
        worker_interface.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


def load_worker( worker_id ):
    # Return a worker instances.
    # If it already exists in myServerManager, return it.
    # Otherwise, return it from the storage.

    if worker_id not in myServerManager.workers:
        worker = load_pickle( './active_learning_api/cache/al_worker_id_%d.pkl' % worker_id  )
        myServerManager.workers[ worker_id ] = worker
    return myServerManager.workers[ worker_id ]


def get_path_dataset(dataset_name):
    path_dataset = './active_learning_api/datasets/%s/data.pkl' % dataset_name
    
    if os.path.isfile(path_dataset):
        return path_dataset
    else:
        # Exception handling here maybe
        return
    


