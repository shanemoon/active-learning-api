from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from active_learning_api import views

urlpatterns = [
    url(r'^workers/$', views.ALWorkerInterfaceList.as_view()),
    url(r'^workers/(?P<pk>[0-9]+)/$', views.ALWorkerInterfaceDetail.as_view()),

    url(r'^answers/$', views.AnswerList.as_view()),
    url(r'^answers/(?P<pk>[0-9]+)/$', views.AnswerDetail.as_view()),

    url(r'^queries/$', views.QueryList.as_view()),
    url(r'^queries/(?P<pk>[0-9]+)/$', views.QueryDetail.as_view()),   
]

urlpatterns = format_suffix_patterns(urlpatterns)