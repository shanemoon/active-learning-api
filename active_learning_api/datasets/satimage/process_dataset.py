import numpy as np
import dill as pickle

class DataObject():
	def __init__(self, X, Z, categories):
		self.X = X
		self.Z = Z
		self.categories = categories

do_normalize = False
col_index_label = -1
input_filename = 'data.txt'
output_filename = 'data.pkl'

data = np.loadtxt(input_filename, delimiter=" ")

X = data[:, 0:col_index_label]
Z = data[:, col_index_label]
categories = np.unique( Z )
categories.sort()
print X.shape
print categories, type(categories)

do = DataObject(X, Z, categories)

with open(output_filename, 'wb') as output:
	pickle.dump(do, output, pickle.HIGHEST_PROTOCOL)