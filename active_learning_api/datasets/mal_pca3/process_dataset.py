import numpy as np
import dill as pickle

class DataObject():
	def __init__(self, X, Z, categories):
		self.X = X
		self.Z = Z
		self.categories = categories

n_top_pca_components = 3
do_normalize = False
col_index_label = -1
input_filename = 'nonzerodecor.csv'
pca_input_filename = 'pcaproj.csv'
output_filename = 'data.pkl'

labelname2num = {}
labelname2num['"benign"'] = 0
labelname2num['"zeus"'] = 1
labelname2num['"m9002"'] = 2

converter = {-1: lambda labelname: labelname2num[labelname]}

data = np.loadtxt(input_filename, delimiter=",", skiprows=1, converters=converter)
pca_data = np.loadtxt(pca_input_filename, delimiter=",", skiprows=1)

X = pca_data[:, 0:n_top_pca_components]
Z = data[:, col_index_label]
categories = np.unique( Z )
categories.sort()
print categories, type(categories)

do = DataObject(X, Z, categories)

with open(output_filename, 'wb') as output:
	pickle.dump(do, output, pickle.HIGHEST_PROTOCOL)