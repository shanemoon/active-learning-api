import numpy as np
import dill as pickle
import sys
from sklearn.preprocessing import normalize

class DataObject():
	def __init__(self, X, Z, categories):
		self.X = X
		self.Z = Z
		self.categories = categories

do_normalize = False
pca_input_filename = 'alldata_nz.csv'
output_filename = 'data.pkl'


labelname2num = {}
labelname2num['DirectDownloader'] = 0
labelname2num['FlyAgent'] = 1
labelname2num['Gampass'] = 2
labelname2num['Gampass/ramnit/Storark'] = 3
labelname2num['Scraze'] = 4
labelname2num['Storark'] = 5
labelname2num['allaple'] = 6
labelname2num['ramnit'] = 7

include_categories = set(['DirectDownloader', 'Scraze', 'allaple'])


converter = {-2: lambda labelname: labelname2num.get( labelname[1:-1], -999 ), 
             -1: lambda labelname: labelname2num.get( labelname[1:-1], -999 ) }


#data = np.genfromtxt(pca_input_filename, delimiter=',', dtype=None, skiprows=1, converters=converter)
data_full = np.loadtxt(pca_input_filename, delimiter=",", skiprows=1, converters=converter)
mask = (data_full[:,-2] == False)
for cat in include_categories:
    mask |= (data_full[:,-2] == labelname2num[cat])
data = data_full[mask]

X = data[:,0:-2]
Z = data[:, -2]
print X.shape, X
print Z.shape, Z

# normalize
X = normalize( X )
print X.shape, X
print Z.shape, Z

categories = np.unique( Z )
categories.sort()
for category in categories:
	print category, len(np.where( Z == category )[0])

do = DataObject(X, Z, categories)

with open(output_filename, 'wb') as output:
	pickle.dump(do, output, pickle.HIGHEST_PROTOCOL)
