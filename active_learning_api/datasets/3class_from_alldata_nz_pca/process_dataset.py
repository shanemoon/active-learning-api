import numpy as np
import dill as pickle
import sys
from sklearn.preprocessing import normalize
from sklearn.decomposition import PCA


class DataObject():
	def __init__(self, X, Z, categories):
		self.X = X
		self.Z = Z
		self.categories = categories

do_normalize = False
pca_input_filename = './../3class_from_alldata_nz/3class_from_alldata_nz.csv'
output_filename = 'data.pkl'
num_PCA_components = 200

labelname2num = {}
labelname2num['DirectDownloader'] = 0
labelname2num['Scraze'] = 1
labelname2num['allaple'] = 2


converter = {-2: lambda labelname: labelname2num.get( labelname[1:-1], -999 ), 
             -1: lambda labelname: labelname2num.get( labelname[1:-1], -999 ) }


#data = np.genfromtxt(pca_input_filename, delimiter=',', dtype=None, skiprows=1, converters=converter)
data = np.loadtxt(pca_input_filename, delimiter=",", skiprows=1, converters=converter)

X = data[:,0:-2]
Z = data[:, -2]
print X.shape, X
print Z.shape, Z

# normalize
X = normalize( X )
print 'X Shape Before PCA:', X.shape, X
print 'Z Shape:', Z.shape, Z

# PCA
X = PCA(n_components=num_PCA_components).fit_transform( X )
print 'X Shape After PCA:', X.shape, X

categories = np.unique( Z )
categories.sort()
for category in categories:
	print category, len(np.where( Z == category )[0])

do = DataObject(X, Z, categories)

with open(output_filename, 'wb') as output:
	pickle.dump(do, output, pickle.HIGHEST_PROTOCOL)
