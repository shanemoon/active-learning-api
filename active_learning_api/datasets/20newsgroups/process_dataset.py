import numpy as np
import dill as pickle


class DataObject():
	def __init__(self, X, Z, categories):
		self.X = X
		self.Z = Z
		self.categories = categories

from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer

newsgroups = fetch_20newsgroups()
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(newsgroups.data)
Z = newsgroups.target
categories = np.unique( Z )
categories.sort()
print X.shape
print categories, type(categories)

output_filename = 'data.pkl'

do = DataObject(X, Z, categories)

#with open(output_filename, 'wb') as output:
#	pickle.dump(do, output, pickle.HIGHEST_PROTOCOL)