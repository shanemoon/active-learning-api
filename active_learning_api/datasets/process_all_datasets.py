# author = Zach
# purpose: Loop over the various dataset folders and execute the main data creation routine in each folder.

import os
mydir = os.getcwd()
for dataset in os.listdir(mydir):
	if os.path.isdir(dataset):
		path = os.path.join(mydir, dataset)
		if 'process_dataset.py' in os.listdir(path):
			print 'processing the dataset in ' + path
			os.chdir(path)
			execfile(os.path.join(mydir, dataset, 'process_dataset.py'))
			os.chdir(mydir)


