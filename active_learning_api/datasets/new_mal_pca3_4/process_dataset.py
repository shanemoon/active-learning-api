import numpy as np
import dill as pickle

class DataObject():
	def __init__(self, X, Z, categories):
		self.X = X
		self.Z = Z
		self.categories = categories

do_normalize = False
pca_input_filename = 'pca4_4class.csv'
output_filename = 'data.pkl'

pca_data = np.loadtxt(pca_input_filename, delimiter=",", skiprows=1)

X = pca_data
Z = np.empty( X.shape[0] )
num_first_class = 1016 - 2 + 1 # allaple
num_second_class = 1827 - 1017 + 1 # scraze
num_third_class = 2089 - 1828 + 1 # flyagent
num_fourth_class = X.shape[0] - num_first_class - num_second_class - num_third_class # direct

Z[0:num_first_class] = 0
Z[num_first_class:num_first_class+num_second_class] = 1
Z[num_first_class+num_second_class:num_first_class+num_second_class+num_third_class] = 2
Z[num_first_class+num_second_class+num_third_class:] = 3

categories = np.unique( Z )
categories.sort()
print categories, type(categories)

do = DataObject(X, Z, categories)

with open(output_filename, 'wb') as output:
	pickle.dump(do, output, pickle.HIGHEST_PROTOCOL)