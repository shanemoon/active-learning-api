import numpy as np
import dill as pickle
from sklearn.datasets import make_classification

class DataObject():
	def __init__(self, X, Z, categories):
		self.X = X
		self.Z = Z
		self.categories = categories

col_index_label = -1
output_filename = 'data.pkl'

X, Z = make_classification(n_samples=1500, n_features=5, n_informative=5, 
	n_redundant=0, n_repeated=0, n_classes=4, n_clusters_per_class=6, weights=None, 
	flip_y=0.05, class_sep=1.0, hypercube=True, shift=0.0, scale=1.0, shuffle=True, random_state=None)

categories = np.unique( Z )
categories.sort()

print categories, type(categories)

do = DataObject(X, Z, categories)

with open(output_filename, 'wb') as output:
	pickle.dump(do, output, pickle.HIGHEST_PROTOCOL)