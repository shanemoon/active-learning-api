from django.db import models


class ServerManager:
	def __init__(self):
		print "!!! My Server Manager is created !!!"
		self.workers = {}

# Initialize a Server Manager, that can be accessed across models, views, etc.
myServerManager = ServerManager()

class Query(models.Model):
	"""
		When a Query object is POSTed:
		It finds the right Worker, gives the Worker who (annotator_id) is asking to label.
		The Worker then runs the Active Learning algorithm, and computes the "next_indices_to_query"

		"next_query" is then returned to the annotator (who made this query POST request)
		in the Response.
	"""
	created = models.DateTimeField(auto_now_add=True)
	worker_id = models.PositiveIntegerField()
	annotator_id = models.PositiveIntegerField(default=-1)
	n_queries = models.PositiveIntegerField( default=1 )

	indices_to_query = models.TextField(blank=True, default='')
	data_to_label = models.TextField(blank=True, default='')

	test_accuracy = models.FloatField(blank=True, default=0.0 )
	labels_clf_estimation = models.TextField(blank=True, default='' )
	labels_clf_confidence = models.TextField(blank=True, default='' )
	#answer_candidates = models.TextField(choices=[], blank=True, default='')

	class Meta:
		ordering = ('created',)


class Answer(models.Model):
	"""
		When an Answer object is POSTed:
		It finds the right Worker, gives the Worker who (annotator_id) 
		just labeled "query_answer" in response to which Query (query_id).

		The worker then takes "query_answer", updates the Worker system.

		*** Optionally it may be able to return "next_query" in the response ***
	"""
	created = models.DateTimeField(auto_now_add=True)
	worker_id = models.PositiveIntegerField()
	annotator_id = models.PositiveIntegerField(default=-1)
	query_id = models.PositiveIntegerField()

	query_answer = models.TextField(blank=True, default='')

	class Meta:
		ordering = ('created',)


class ALWorkerInterface(models.Model):
	"""
		It is an interface to <ALWorker> class.
		When an <ALWorkerInterface> object is posted,
		it creates an <ALWorker> instance, 
		and saves (pickles) it to a Worker Storage (local storage) indexed by its worker_id.
	"""
	created = models.DateTimeField(auto_now_add=True)
	learner_type    = models.PositiveIntegerField( default=0 )
	learner_params  = models.TextField( blank=True, default='{}' ) 
	model_type      = models.PositiveIntegerField( default=0 )
	model_params    = { 'clf_type' : 'LR' }
	n_annotators    = models.PositiveIntegerField( default=1 )
	d_annotators    = { 'auto' : ('build_k_human_annotators', n_annotators, {}) }
	dataset_name    = models.TextField(blank=True, default='mal')
	budget          = models.PositiveIntegerField( default=300 )
	percent_labeled = models.FloatField( default=0.05 )
	n_folds         = models.PositiveIntegerField( default=5 )

	class Meta:
		ordering = ('created',)
