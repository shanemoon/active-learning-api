from django.forms import widgets
from rest_framework import serializers
from active_learning_api.models import Query, Answer, ALWorkerInterface


class QuerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Query
        fields = ('id', 'worker_id', 'annotator_id', 'indices_to_query', 'data_to_label', 'n_queries', 'test_accuracy', 'labels_clf_estimation', 'labels_clf_confidence')

class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'worker_id', 'annotator_id', 'query_id', 'query_answer')

class ALWorkerInterfaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ALWorkerInterface
        fields = ('id', 'learner_type', 'learner_params', 'model_type', 'dataset_name', 'budget', 'percent_labeled', 'n_folds', 'n_annotators')
