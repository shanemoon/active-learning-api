
function getParameterByName(name) {
    /* 
        Parse the URL into a set of parameters
        Usage:
            var prodId = getParameterByName('prodId');
    */    
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}