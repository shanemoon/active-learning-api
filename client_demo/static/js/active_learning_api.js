
HOST_ADDRESS = "http://localhost:8000/";
QUERIES_ADDRESS = "queries/";
ANSWERS_ADDRESS = "answers/";
WORKERS_ADDRESS = "workers/"
NUM_CLASSES = 3;
var num_instances = 0;
var query_id = 0;

// Visualization methods
// 0 : Plain texts
// 1 : Graph3 Visualization using vis.js
var visualization_method = 1;

function query_data_to_label_and_visualize( worker_id, annotator_id, container_id ) {
  $.post( HOST_ADDRESS + QUERIES_ADDRESS, { 'worker_id': worker_id, 'annotator_id': annotator_id }, function( data ){
    // Get the data to label
    data_to_label = eval(data['data_to_label']);
    query_id = eval(data['id'])
    
    // For each data point, visualize it.
    num_instances = data_to_label.length;
    for (var i = 0; i < num_instances; i++) {
    	// Obtain each instance, which is usually a list of featres (numbers)
    	instance_to_label = data_to_label[i];

    	// Create a sub-div, which holds a data visualization, and annotation options.
    	var sub_container_id = get_sub_container_id( container_id, i );
		$('<div/>', {
		    'id': sub_container_id,
		    'align' : 'center'
		}).appendTo( '#' + container_id );


    	// Visualize the data
    	var sub_container_data_id = get_sub_container_data_id( container_id, i );
		$('<div/>', { id: sub_container_data_id }).appendTo( '#' + sub_container_id );

    	if (visualization_method == 0) {
    		// Method 0 : Plain Text
			$('#' + sub_container_data_id).append(
				'<b>' + instance_to_label.slice(0, 5) + ' ... (skipped. better visualization coming)' + '</b>'
			)
			$('#explanation').text( 'Please annotate the following data.' );
			class_labels = ['Class 0', 'Class 1', 'Class 2'];
    	}


    	else if (visualization_method == 1) {
    		// Method 1 : Graph visualization with vis.js
    		var dataset_name = ''
			$.get( HOST_ADDRESS + WORKERS_ADDRESS + worker_id, function( worker_data ){
				// Obtain dataset name
				dataset_name = worker_data['dataset_name']
				$('#annotation_title').html( 'Dataset Name: ' + dataset_name )
				
				/*
					The code below seems to be hard-coded.
					Instead, it should:
					- grab data samples (e.g. pre-rendered, etc.) of the input dataset with `dataset_name'
					- grab class labels
				*/

				// Obtain dataset sample
				var data = sample_data;    // hard-sampled data (in /js/data_sample.js)
				var labels = sample_label; // hard-sampled data (in /js/data_sample.js)

				// Build a vis_data object
				var vis_data = new vis.DataSet();

				// Draw the data distribution of the data as a reference.
				for (var j = 0; j < data.length; j++) {
					data_point = data[j];
					label      = labels[j];
					vis_data.add( {x: data_point[0], y: data_point[1], z: data_point[2], style:label } );
				}

				// Add a new data point to label (assuming at least D:3, draw the first 3)
				vis_data.add( {x: instance_to_label[0], y: instance_to_label[1], z: instance_to_label[2], style: NUM_CLASSES } );

				// Options
				var options = {
				style: 'dot-color',
				width: '500px',
				legendLabel: 'Class',
				keepAspectRatio: false
				};

				// Visualize the data
				var dataset = new vis.DataSet(data);
				var graph2d = new vis.Graph3d(document.getElementById(sub_container_data_id), vis_data, options);

				$('#explanation').html( 'Please classify the <font color="red">red</font> dot.' );
				class_labels = ['Blue', 'Cyan', 'Green'];				

		    	// Visualize the annotation options
		    	var sub_container_options_id = get_sub_container_options_id( container_id, i )
		    	$('<select/>', {
				    'id': sub_container_options_id,
				    'class': 'form-control',
				    'style': 'width:200px',
				}).appendTo( '#' + sub_container_id );
				for (var j = 0; j < NUM_CLASSES; j++) {
					$('#' + sub_container_options_id).append( 
						'<option value="' + j + '"> ' + class_labels[j] + '</option>'
					);
				}

			}, "json");
    	}
    }
  }, "json");
}

function send_answers( worker_id, annotator_id, container_id ) {
	var answers = [];
	// For each data point, obtain selected answers and append to `answers`
    for (var i = 0; i < num_instances; i++) {
    	answer = $('#' + get_sub_container_options_id( container_id, i )).find(":selected").val();
    	answers.push( answer );
    }

	if (answers.length == num_instances) {
		// Annotations must have been made completely.
		// Send a POST request
		query_answer = '[' + answers.toString() + ']';
		$.post( HOST_ADDRESS + ANSWERS_ADDRESS, { 'worker_id': worker_id, 'annotator_id': annotator_id, 'query_id': query_id, 'query_answer' : query_answer }, function( data ){
		    // Maybe check if it is valid.
		    return 1;
		});
	}
	else {
		// Annotations are not done completely.
		return 0;
	}
}

function get_sub_container_id( container_id, sub_container_index ) {
	return container_id + '_' + sub_container_index;
}

function get_sub_container_data_id( container_id, sub_container_index ) {
	return get_sub_container_id( container_id, sub_container_index ) + '_data';
}

function get_sub_container_options_id( container_id, sub_container_index ) {
	return get_sub_container_id( container_id, sub_container_index ) + '_options';
}

function get_sub_container_option_item_id( container_id, sub_container_index, item_index ) {
	return get_sub_container_options_id( container_id, sub_container_index ) + '_' + item_index
}