from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from client_demo import views

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^annotation/', TemplateView.as_view(template_name='annotation.html')),
)