# Active Learning API #

## Overview ##

The repository contains the active learning framework source code (in `./active_learning_api/lib`) as well as the Django Web Server backend code. You can simulate an active learning scenario via `./active_learning_api/lib/Simulator.py`, or deploy an actual learning task by hosting a Django server as explained in this document.

If you simply want to run a simulation, all you need is the code in `./active_learning_api/lib`.


## Installation ##

Note that you need to install all of the followings in a virtual environment if you want to run a server to deploy a learning task (see Section "Run Server"). If you simply want to run a simulation, you do not need a virtual environment.

### General Installations ###

* [Install Numpy](http://www.numpy.org/): `pip install -U numpy`
* [Install Pandas](): `pip install pandas`
* [Install PyLab](): `pip install matplotlib`
* [Install Scikit-Learn](http://scikit-learn.org/stable/install.html): `pip install -U scipy scikit-learn`
* [Miscellaneous](): `pip install dill`
* [Load datasets](): While disconnected from VPN, change directory to `active_learning_api/datasets` and with python 2.7 execute `process_all_datasets.py`.

### Python Django Installation (for running a server) ###

* [Install Django](https://docs.djangoproject.com/en/1.7/topics/install/)

For more info, check the following tutorials:

* [Writing your first Django app](https://docs.djangoproject.com/en/1.7/intro/tutorial01/)
* [Django REST Framework](http://www.django-rest-framework.org/tutorial/1-serialization/)

## Run Simulation ##

To run a simulation, you can simply run a main function at `python ./lib/Simulator.py`.


## Run Server ##

1. `cd` into `./active_learning`.
2. Set up a virtual environment by running `virtualenv env`, then `source env/bin/activate`
3. Now that we're inside a virtualenv environment, we can install our package requirements. `pip install django`, `pip install djangorestframework`, `pip install pygments`. To exit virtualenv,  just type `deactivate`. Also install all the dependency libraries as explained in General Installations, including `numpy`, `scikit-learn`, `pandas`, etc.
4. If the database is not set up yet, do `python manage.py makemigrations active_learning_api`, then `python manage.py migrate` .
5. Run server by running `python manager.py runserver`.
6. If everything's successful, the server should be running by default at `http://localhost:8000`.


## How to Use API (To be updated) ##
* To create a new task (<Worker>): (e.g.)
	- Random selection: `http --form POST http://127.0.0.1:8000/workers/ dataset_name=mal`
	- Active learning selection: `http --form POST http://127.0.0.1:8000/workers/ dataset_name=mal learner_type=1 learner_params='{"utility_params":{"active_features":["entropy"],"active_features_params":[{}]}}'`
	- DPAL: `http --form POST http://127.0.0.1:8000/workers/ dataset_name=mal learner_type=1 learner_params='{"utility_params":{"active_features":["entropy"],"active_features_params":[{}],"dynamic_params":{"main_percentage":0.8}}}'`
* To ask for next queries: (e.g.) `http --form POST http://127.0.0.1:8000/queries/ worker_id=56 annotator_id=0`
* To update with new answers: (e.g.) `http --form POST http://127.0.0.1:8000/answers/ worker_id=56 query_id=27 annotator_id=0 query_answer=[1]`


## Client Demo ##
A client demo that uses this API is also included in the repository. Run the server by `python manage.py runserver`, and:
1. Go to `http://localhost:8000/`, which lands you to an index page where you can select worker_id, and user_id.
2. Click 'Start Annotation'.
3. Annotate the data, and hit 'Done' button.

The client demo included in this source repository makes use of `vis.js`. For more info, here's their [documentation](http://visjs.org/docs/graph3d.html).