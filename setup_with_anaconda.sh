
## This contains similar information as in the README.md, but it is customized for 
##    the Anaconda user and also provides the commands in a slightly easier-to-run format

## The following assumes that you use Anaconda Python and you have pip installed.
##    Run each of the following commands, one at a time, while reading the comments.

## Virtualize ##
conda create -n py27 python=2.7
source activate py27 # `source deactivate` to close

## Prepare environment ##
pip install http
conda install numpy
conda install matplotlib
conda install scipy scikit-learn
conda install dill
conda install django
pip install djangorestframework #conda can't find it!
conda install pygments
pip install pandas

## Run Server ##
python manage.py makemigrations active_learning_api
python manage.py migrate
python manage.py runserver
#  If everything's successful, the server should be running by default at `http://localhost:8000`.

## Run Simulation in a new terminal:
source activate py27 
cd active_learning_api
python ./lib/Simulator.py
