from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = [
    url(r'^', include('active_learning_api.urls')),
    url(r'^', include('client_demo.urls')),
]